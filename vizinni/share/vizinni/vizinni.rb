#!/usr/bin/env ruby

require 'libglade2'
require 'mltpp'

class Gtk::Widget
	def cleanup
	end
end

class Geometry < Gtk::VBox
	def initialize( properties, name, player )
		super( false )

		@properties = properties
		@name = name
		@player = player

		@widgets = parse( properties.get( name ) )

		@pressed = 0
		@press = @player.glade.get_widget("eventbox1").signal_connect( "button_press_event" ) { |w,e|
			modify e, false
		}
		@move = @player.glade.get_widget("eventbox1").signal_connect( "motion_notify_event" ) { |w,e|
			if @pressed == 1
				modify e, false
			end
		}
		@release = @player.glade.get_widget("eventbox1").signal_connect( "button_release_event" ) { |w,e|
			if @pressed == 1
				modify e, true
				@pressed = 0
			end
		}
	end

	def modify e, final
		x = e.x - @player.sdl.get_int( "rect_x" )
		x = 0 if x < 0
		x = @player.sdl.get_int( "rect_w" ) if x > @player.sdl.get_int( "rect_w" )
		y = e.y - @player.sdl.get_int( "rect_y" )
		y = 0 if y < 0
		y = @player.sdl.get_int( "rect_y" ) if y > @player.sdl.get_int( "rect_h" )
		x = ( x * 100.0 ) / @player.sdl.get_int( "rect_w" )
		y = ( y * 100.0 ) / @player.sdl.get_int( "rect_h" )

		if @pressed == 0
			@start_x = x
			@start_y = y
			geometry = x.to_s + "%," + y.to_s + "%"
			@player.indicate.set "composite.geometry", geometry + ":0%x0%"
			@player.indicate.set "composite.refresh", "1"
			@player.sdl.attach @player.indicate
			@pressed = 1
		else
			x1 = @start_x
			y1 = @start_y

			if ( x < x1 )
				x1 = x
				x = @start_x
			end

			if ( y < y1 )
				y1 = y
				y = @start_y
			end

			if ( final )
				if ( x - x1 > 0 && y - y1 > 0 )
					@widgets[0].value = x1
					@widgets[1].value = y1
					@widgets[2].value = x - x1
					@widgets[3].value = y - y1
				end
				@player.sdl.detach @player.indicate
				@pressed = 0
			else
				geometry = x1.to_s + "%," + y1.to_s + "%:" + 
					   	   ( x - x1 ).to_s + "%x" + ( y - y1 ).to_s + "%"

				@player.indicate.set "composite.geometry", geometry
				@player.indicate.set "composite.refresh", "1"
			end
		end
		@player.refresh
	end

	def cleanup
		@player.glade.get_widget("eventbox1").signal_handler_disconnect( @press )
		@player.glade.get_widget("eventbox1").signal_handler_disconnect( @move )
		@player.glade.get_widget("eventbox1").signal_handler_disconnect( @release )
	end

	def parse( value )

		widgets = Array.new

		value.split( ":" ).each do |v| 
			case v
			when /x/
				v.split( "x" ).each do |v|
					widget = Gtk::HScale.new( 0, 200, 1 )
					widget.value = v.to_f
					widgets.push widget
				end
			when /,/
				v.split( "," ).each do |v|
					widget = Gtk::HScale.new( -100, 100, 1 )
					widget.value = v.to_f
					widgets.push widget
				end
			when //
				widget = Gtk::HScale.new( 0, 100, 1 )
				widget.value = v.to_f
				widgets.push widget
			end
		end

		widgets.each { |widget| widget.set_value_pos( 1 ) }
		widgets.each { |widget| widget.signal_connect( 'value_changed' ) { changed } }
		widgets.each { |widget| pack_start( widget, false ) }

		return widgets
	end

	def changed
		count = 0
		joins = ",:x:"
		value = ""

		@widgets.each do |widget|
			value = value.to_s + widget.value.to_f.to_s + "%"
			value = value.to_s + joins[ count .. count ].to_s
			count += 1
		end

		@properties.set @name, value
		@properties.set "refresh", "1"
		@properties.set "composite.refresh", "1"
		@player.refresh
	end
end

class FileSelector < Gtk::Table

	def initialize( properties, name )
		super( 1, 2, false )
		@properties = properties
		@name = name
		parse( properties.get( name ) )
	end

	def parse file
		@text = Gtk::Entry.new
		@text.editable = false
		@text.text = file
		@button = Gtk::Button.new( "Change" )
		@button.signal_connect("clicked") { change }
		attach( @text, 0, 1, 0, 1, Gtk::SHRINK | Gtk::EXPAND | Gtk::FILL, Gtk::FILL, 0, 0 )
		attach( @button, 1, 2, 0, 1, Gtk::SHRINK | Gtk::FILL, Gtk::FILL, 0, 0 )
	end

	def change
		dialog = Gtk::FileSelection.new( "Select media" )
		dialog.filename = @text.text if File.exist?( @text.text )
		dialog.ok_button.signal_connect("clicked") do
			dialog.selections.each { |x| 
				@properties.set @name, x 
				@text.text = x
			}
			dialog.close
		end
		dialog.cancel_button.signal_connect("clicked") do
			dialog.close
		end
		dialog.show_all
	end

end

class Control < Gtk::Table

	def initialize( properties, player )
		super( 1, 2, false )

		@properties = properties
		@player = player
		@widgets = Array.new( )

		count = 0

		for i in 0 .. properties.count - 1
			name = properties.get_name( i )
			if name.include?( "property." )
				name = name.sub( "property.", "" )
				value = properties.get( i )
				Array values = value.split( /:/ )

				case values[2]
				when 'int'
					Array range = values[3].split( ',' )
					label = Gtk::Label.new( values[ 1 ] )
					widget = Gtk::HScale.new( range[0].to_i, range[1].to_i, 1 )
					widget.value = @properties.get( name ).to_f
					widget.set_value_pos( 1 )
					widget.signal_connect( 'value_changed' ) { changed }
				when 'bool'
					label = Gtk::Label.new( values[ 1 ] )
					widget = Gtk::CheckButton.new( "On" )
					widget.active = @properties.get( name ).to_i == 1
					widget.signal_connect( 'toggled' ) { changed }
				when 'rbool'
					label = Gtk::Label.new( values[ 1 ] )
					widget = Gtk::CheckButton.new( "On" )
					widget.active = @properties.get( name ).to_i == 0
					widget.signal_connect( 'toggled' ) { changed }
				when 'float'
					Array range = values[3].split( ',' )
					label = Gtk::Label.new( values[ 1 ] )
					widget = Gtk::HScale.new( range[0].to_i, range[1].to_i, 0.1 )
					widget.value = @properties.get( name ).to_f
					widget.set_value_pos( 1 )
					widget.signal_connect( 'value_changed' ) { changed }
				when 'geometry'
					label = Gtk::Label.new( values[ 1 ] )
					widget = Geometry.new( @properties, name, @player )
				when 'file'
					label = Gtk::Label.new( values[ 1 ] )
					widget = FileSelector.new( @properties, name )
				when 'text'
					label = Gtk::Label.new( values[ 1 ] )
					widget = Gtk::Entry.new
					widget.text = @properties.get( name )
					widget.signal_connect( 'changed' ) { changed }
				end

				resize( count + 1, 2 )
				label.set_alignment( 0, 0 )
				attach( label, 0, 1, count, count + 1, Gtk::FILL, Gtk::FILL, 0, 0 )
				attach( widget, 1, 2, count, count + 1, Gtk::SHRINK | Gtk::EXPAND | Gtk::FILL, Gtk::FILL, 0, 0 )
				count = count + 1
				@widgets.push( widget ) if !widget.nil?
			end
		end
		show_all
	end

	def destroy
		super
		@widgets.each { |widget| widget.cleanup }
	end

	def changed
		widget_index = 0
		for i in 0 .. @properties.count - 1
			name = @properties.get_name( i )
			if name.include?( "property." )
				name = name.sub( "property.", "" )
				value = @properties.get( i )
				Array values = value.split( /:/ )
				widget = @widgets[ widget_index ]

				case values[2]
				when 'int'
					@properties.set name, widget.value.to_s
				when 'bool'
					@properties.set name, widget.active? ? "1" : "0"
				when 'rbool'
					@properties.set name, widget.active? ? "0" : "1"
				when 'float'
					@properties.set name, widget.value.to_s
				when 'text'
					@properties.set name, widget.text
				end

				widget_index = widget_index + 1
			end
		end
		@player.refresh
	end

end

class Player

	attr_reader :sdl, :drawingarea, :glade, :indicate, :producer

	def initialize( files )
		@profile = Mltpp::Profile.new

		# Get the glade object
		@glade = GladeXML.new( PATH + "vizinni.glade") {|handler| method(handler)}

		# Get the window
		@window = @glade.get_widget("vizinni")

		# Define the list model and treeview
		@model = Gtk::ListStore.new(String, String, String, String)
        column = Gtk::TreeViewColumn.new("File", Gtk::CellRendererText.new, { :text => 0, :foreground => 1, :background => 2 })
        column2 = Gtk::TreeViewColumn.new("Path", Gtk::CellRendererText.new, { :text => 3 })
		@treeview = @glade.get_widget( "treeview1" )
		@treeview.set_model( @model )
		@treeview.append_column( column )
		column2.set_visible( false )
		@treeview.append_column( column2 )
		@treeview.selection.mode = Gtk::SELECTION_MULTIPLE

		# Define the callback 
        @treeview.signal_connect("button_press_event") do |widget, ev|
            play_selected_clip if (ev.button == 1 and ev.event_type == 5)
        end

		@producer = Mltpp::Playlist.new

		# Populate the treeview
		add_files( files )

		# Define the fx palette
		@model_palette = Gtk::ListStore.new(TrueClass, String)
		column3 = Gtk::TreeViewColumn.new("Selected", Gtk::CellRendererToggle.new, { :active => 0 })
		column4 = Gtk::TreeViewColumn.new("File", Gtk::CellRendererText.new, { :text => 1 })
		@treeview_palette = @glade.get_widget( "treeview2" )
		@treeview_palette.set_model( @model_palette )
		@treeview_palette.append_column( column3 )
		@treeview_palette.append_column( column4 )

        @treeview_palette.signal_connect("button_press_event") do |widget, ev|
			apply_fx if (ev.button == 1 and ev.event_type == 5)
        end

        @treeview_palette.signal_connect("cursor_changed") { show_fx }

		@fx = Array.new
		@custom_count = 0
		properties = Mltpp::Properties.new( PATH + "fx.properties" )
		add_fx properties
		@custom_properties = Mltpp::Properties.new( ENV[ 'HOME' ] + "/.fx.properties" )
		add_fx @custom_properties

		# Get transport buttons and assign signals (I'm sure this could be cleaner...)
		@transport = [
			Button.new( @glade.get_widget("togglebutton1"), @glade.get_widget("togglebutton1").signal_connect( "clicked" ) { pressed_prev } ),
			Button.new( @glade.get_widget("togglebutton2"), @glade.get_widget("togglebutton2").signal_connect( "clicked" ) { pressed_rew } ),
			Button.new( @glade.get_widget("togglebutton3"), @glade.get_widget("togglebutton3").signal_connect( "clicked" ) { pressed_play } ),
			Button.new( @glade.get_widget("togglebutton4"), @glade.get_widget("togglebutton4").signal_connect( "clicked" ) { pressed_ffwd } ),
			Button.new( @glade.get_widget("togglebutton5"), @glade.get_widget("togglebutton5").signal_connect( "clicked" ) { pressed_next } ),
			Button.new( @glade.get_widget("togglebutton10"), @glade.get_widget("togglebutton10").signal_connect( "clicked" ) { pressed_stop } ),
			Button.new( @glade.get_widget("togglebutton11"), @glade.get_widget("togglebutton11").signal_connect( "clicked" ) { pressed_inject } ),
		]

		@fullscreen = false
		@glade.get_widget("togglebutton7").signal_connect( "clicked" ) { toggle_fullscreen }
		@glade.get_widget("togglebutton9").signal_connect( "clicked" ) { toggle_scaling }

		# Playlist buttons
		@glade.get_widget("button1").signal_connect( "clicked" ) { select_files }
		@glade.get_widget("button2").signal_connect( "clicked" ) { remove_files }
		@glade.get_widget("button3").signal_connect( "clicked" ) { @model.clear }

		@filter_stack = Array.new
		@glade.get_widget("button4").signal_connect( "clicked" ) { save_filters }

		# Get the scale and assign signals
		@scale = @glade.get_widget( "hscale1" )
		@scale_handler = @scale.signal_connect('value_changed') { seek }

		# Playlist button
		@show = @glade.get_widget("togglebutton6")
		@playlist_area = @glade.get_widget("vbox5")
		@show.signal_connect( "clicked" ) { show_playlist }

		# FX palette
		@show_fx = @glade.get_widget("togglebutton8")
		@palette_area = @glade.get_widget("vbox6")
		@show_fx.signal_connect("clicked") { show_palette }

		# Get the drawing area and assign the window id to the sdl env var
		@drawingarea = @glade.get_widget("drawingarea1")

		@indicate = Mltpp::Filter.new( @profile, "region" )
		@indicate.set "filter[0]", "invert"

		@drawingarea.parent.double_buffered = TRUE
		@drawingarea.can_focus = FALSE
		@drawingarea.has_focus = FALSE
		@drawingarea.signal_connect( "expose_event" ) { @sdl.set( "refresh", 1 ); false }
		@window.signal_connect( "unmap_event" ) { @producer.set_speed( 0 ); false }
		@window.signal_connect_after( "key_press_event" ) { |w,e| 
			if e.keyval == Gdk::Keyval::GDK_space
				pressed_play
			elsif e.keyval == Gdk::Keyval::GDK_Left
				pressed_rew
			elsif e.keyval == Gdk::Keyval::GDK_Right
				pressed_ffwd
			elsif e.keyval == Gdk::Keyval::GDK_Up
				pressed_prev
			elsif e.keyval == Gdk::Keyval::GDK_Down
				pressed_next
			elsif e.keyval == Gdk::Keyval::GDK_Escape || e.keyval == Gdk::Keyval::GDK_q
				destroy
			end
			false
		}
		ENV[ 'SDL_WINDOWID' ] = @drawingarea.window.xid.to_s

		# Set up drag and drop behaviour
		Gtk::Drag.dest_set( @window, Gtk::Drag::DEST_DEFAULT_ALL, [["text/uri-list", 0, 0]], 
							Gdk::DragContext::ACTION_COPY| Gdk::DragContext::ACTION_MOVE)
		@window.signal_connect( "drag_data_received" ) do |w, context, x, y, data, info, time|
        	data.data.each { |x| add_file( urldecode( x ) ) }
			Gtk::Drag.finish(context, true, false, 0)
			play_clip 0 if @producer.nil?
		end

		# Create the sdl consumer
		@sdl = Mltpp::Consumer.new( @profile, "gtk2_preview" )
		@sdl.set( "rescale", "none" )
		@sdl.set( "width", 352 )
		@sdl.set( "height", 288 )
		@sdl.set( "progressive", 1 )
		@sdl.set( "refresh", 1 )
		@sdl.connect @producer 
		@sdl.start

		@filter_volume = Mltpp::Filter.new( @profile, "volume" )
		@sdl.attach @filter_volume
		@volume = @glade.get_widget( "hscale2" )
		@volume.signal_connect('value_changed') { @filter_volume.set( "gain", @volume.value.to_s ) }

		@filter_brightness = Mltpp::Filter.new( @profile, "brightness" )
		@sdl.attach @filter_brightness
		@brightness = @glade.get_widget( "hscale3" )
		@brightness.signal_connect('value_changed') { @filter_brightness.set( "start", @brightness.value.to_s ) }

		@filter_gamma = Mltpp::Filter.new( @profile, "gamma" )
		@sdl.attach @filter_gamma
		@gamma = @glade.get_widget( "hscale4" )
		@gamma.signal_connect('value_changed') { @filter_gamma.set( "gamma", @gamma.value.to_s ) }

		# Set up destructor signal
		@window.signal_connect( "destroy" ) { destroy }

		# Play the first clip
		play_clip 0

		# Set up the timer callback
		@timer = Gtk.timeout_add(1000) { timer; true }
	end

	def destroy
		@sdl.stop
		Gtk.main_quit
		exit
	end

	def urldecode(str)
		str.to_s.gsub('file:/','/').gsub('\+',' ').gsub(/%([0-9A-Fa-f]{2})/) {'%c' % $1.hex}.chomp
	end
	
	def add_fx properties
		for i in 0 .. properties.count - 1
			name = properties.get_name( i )
			value = properties.get( i )
			if !name.include?( "." )
				description = properties.get( i + 1 )
				i = i + 1
				filter = append_fx( description, value.gsub( 'PATH+', PATH ) )
				filter = nil if filter.nil? || !filter.is_valid
			else
				filter.set( name.sub( /^[^\.]*\./, "" ), value.gsub( 'PATH+', PATH ) ) if ! filter.nil?
			end
		end
	end

	def append_fx( title, id, props = nil )
		filter = Mltpp::Filter.new( @profile, id )
		if filter.is_valid 
			if props != nil
				props.each { |x| filter.parse( x ) }
			end
			@fx.push filter
			iter = @model_palette.append
			iter.set_value( 0, false )
			iter.set_value( 1, title )
		end
		filter
	end

	def refresh
		@sdl.set "refresh", 1 
	end

	def apply_fx
        if iter = @treeview_palette.selection.selected 
			path = iter.path 
			if ! iter.get_value( 0 )
				@producer.attach( @fx[ path.to_s.to_i ] )
				iter.set_value( 0, true )
				show_fx
				@filter_stack.push( @fx[ path.to_s.to_i ] )
			else
				@producer.detach( @fx[ path.to_s.to_i ] ) != 0
				iter.set_value( 0, false )
				remove_fx
				@filter_stack.delete( @fx[ path.to_s.to_i ] )
			end
		end
		refresh
	end

	def show_fx
        if !@producer.nil? && iter = @treeview_palette.selection.selected 
			path = iter.path 
			if iter.get_value( 0 )
				vbox = @glade.get_widget( "vbox7" )
				vbox.remove( @control ) if ! @control.nil?
				@control.destroy if ! @control.nil?
				@control = Control.new( @fx[ path.to_s.to_i ], self )
				vbox.pack_end( @control, false )
			else
				remove_fx
			end
		end
	end

	def remove_fx
		vbox = @glade.get_widget( "vbox7" )
		vbox.remove( @control ) if ! @control.nil?
		@control.destroy if ! @control.nil?
		@control = nil
	end

	def drop_effect path
		iter = @treeview_palette.model.get_iter( path )
		if iter.get_value( 0 ) 
			iter.set_value( 0, false ) 
			@producer.detach( @fx[ path.to_s.to_i ] )
			@filter_stack.delete( @fx[ path.to_s.to_i ] )
		end
	end

	def apply_effect path
		iter = @treeview_palette.model.get_iter( path )
		iter.set_value( 0, true ) 
		@producer.attach( @fx[ path.to_s.to_i ] )
		@filter_stack.push( @fx[ path.to_s.to_i ] )
	end

	def add_files files
        files.each { |x| add_file(x) }
	end

	def add_file file
        if File.directory?(file)
			Dir.entries( file ).each do |x|
			next if x =~ /^\.{1,2}/   # ignore '.' and '..'
				x = file + "/" + x
            	add_file( x )
			end
		else
			iter = @model.append
			value = file
			iter.set_value(0, File::basename( value ))
			iter.set_value(3, file)
		end
	end

	def select_files
		dialog = Gtk::FileSelection.new( "Select media" )
		dialog.select_multiple = TRUE
		dialog.ok_button.signal_connect("clicked") do
			dialog.selections.each { |x| add_file x }
			dialog.close
		end
		dialog.cancel_button.signal_connect("clicked") do
			dialog.close
		end
		dialog.show_all
	end

	def remove_files
		selected = Array.new()
		@treeview.selection.selected_each { |model, path, iter| selected.push iter }
		selected.each { |x| @model.remove x }
	end

	def show_state
		@transport.each do |x|
			x.widget.signal_handler_block( x.signal )
			x.widget.active = FALSE
			x.widget.signal_handler_unblock( x.signal )
		end
		@sdl.set( "refresh", 1 )
	end

	def pressed_inject

		if ! ENV[ 'VIZINNI_CONSUMER' ].nil?
			values = ENV[ 'VIZINNI_CONSUMER' ].split( " " )
			id = values.shift
			alternative = Mltpp::Consumer.new( @profile, id )
			raise "Invalid consumer specified #{ENV['VIZINNI_CONSUMER']}" if !alternative.is_valid
			values.each { |v| alternative.parse v }
			@producer.set "title", File::basename( @producer.get_clip( 0 ).parent( ).get( "resource" ) ) if @producer.count( ) > 0
			alternative.connect @producer

			@producer.set_speed 0
			@sdl.stop
			alternative.start if !alternative.nil?
			alternative.stop if !alternative.nil?
			@producer.set_speed 0
			@sdl.start
		end
	end

	def pressed_prev
		play_clip @next - 2
		@sdl.start if @sdl.is_stopped
		show_state
	end

	def pressed_rew
		@producer.set_speed( -10 ) 
		@sdl.start if @sdl.is_stopped
		show_state
	end

	def pressed_play 
		@producer.set_speed( @producer.get_speed != 1 ? 1 : 0 ) 
		@sdl.start if @sdl.is_stopped
		show_state
	end

	def pressed_ffwd 
		@producer.set_speed( 10 ) 
		@sdl.start if @sdl.is_stopped
		show_state
	end

	def pressed_next 
		play_clip @next
		@sdl.start if @sdl.is_stopped
		show_state
	end

	def pressed_stop 
		if ! @producer.nil? && !@sdl.is_stopped
			@producer.set_speed( @producer.get_speed == 1 ? 0 : 1 ) 
		elsif !@sdl.is_stopped
			@sdl.stop 
		end	
		show_state
	end

	def show_playlist
		if @playlist_area.visible?
			@playlist_area.hide
		else
			@playlist_area.show
		end
	end

	def show_palette
		if @palette_area.visible?
			@palette_area.hide
		else
			@palette_area.show
		end
	end

	def play_selected_clip
		@treeview.selection.selected_each { |model, path, iter| play_clip iter.path.to_s.to_i }
	end

	def play_clip clip
		clip = 0 if clip < 0
		path = Gtk::TreePath.new clip.to_s 
		iter = @model.get_iter path
		if !iter.nil?
			file = iter.get_value( 3 )
			producer = Mltpp::Producer.new( @profile, file )
		end
		if !producer.nil? && producer.is_valid
			@producer.lock
			@producer.clear
			@scale.set_range( producer.get_in, producer.get_out )
			@scale.set_digits 0
			@producer.append producer
			@producer.set_speed 1
			@producer.seek 0
			@producer.unlock
			@treeview.set_cursor( path, nil, FALSE )
		else
			@producer.set_speed 0
		end
		@sdl.set( "refresh", 1 );
		@next = clip + 1
	end

	def timer
		if ! @producer.nil?
			out = @producer.get_out
			position = @producer.position
			if out == position
				play_clip @next
			end
			@scale.signal_handler_block(@scale_handler)
			@scale.value = position 
			@scale.signal_handler_unblock(@scale_handler)
		end
	end

	def seek
		if ! @producer.nil?
			position = @scale.value
			@producer.seek( position.to_i )
			@sdl.set( "refresh", 1 )
		end
	end

	class Button
		def initialize( in_widget, in_signal )
			@widget = in_widget
			@signal = in_signal
		end
	
		def widget
			return @widget
		end
	
		def signal
			return @signal
		end
	end

	def toggle_fullscreen
		@window.fullscreen if !@fullscreen
		@window.unfullscreen if @fullscreen
		@fullscreen = @fullscreen ? false : true
	end

	def toggle_scaling
		rescale = @sdl.get( "rescale" )
		@sdl.stop
		@sdl.set( "rescale", rescale == "none" ? "hyper" : "none" )
		@sdl.start
	end

	def save_filters
		properties = Mltpp::Properties.new
		custom_count = @custom_properties.get_int( "count" )
		@custom_properties.set( "count", ( custom_count + 1 ).to_s )
		id = "custom[#{custom_count}]"
		properties.parse "#{id}=region"
		properties.parse "#{id}.description=#{id}"
		properties.parse "#{id}.composite.geometry=0,0:100%x100%:100"
		properties.parse "#{id}.property.composite.geometry=:Start:geometry:0,100:100"
		properties.parse "#{id}.property.resource=:Shape:file"
		count = 0
		@filter_stack.each { |filter|
			description = filter.get( "description" )
			for i in 0 .. filter.count - 1
				begin
					name = filter.get_name( i )
					value = filter.get( name )

					if name.index( "_" ) != 0 && !name.include?( "property." ) && !value.nil?
						if name == "mlt_service"
							properties.parse "#{id}.filter[#{count}].off=0"
							properties.parse "#{id}.filter[#{count}].property.off=:#{description}:rbool"
							properties.parse "#{id}.filter[#{count}]=#{value}"
						else
							properties.parse "#{id}.filter[#{count}].#{name}=#{value}"
						end
					end
				rescue
				end
			end
			count += 1
		}
		add_fx properties
		@custom_properties.inherit properties
		@custom_properties.save ENV[ 'HOME' ] + "/.fx.properties"

		remove_fx
		last = nil
		@treeview_palette.model.each { |model,path,iter| 
			drop_effect path 
			last = path
		}

		apply_effect last if !last.nil?
	end
end

def get_path file
	path = File.dirname file
	path += "/" if path != ""
end

files = ARGV.collect { |x| x unless x =~ /^-/ }

PATH=get_path $0

ENV[ 'MLT_TEST_CARD' ] = PATH + "logo.png" if ENV[ 'MLT_TEST_CARD' ].nil?
ENV[ 'VIZINNI_CONSUMER' ] = "valerie" if ENV[ 'VIZINNI_CONSUMER' ].nil?

Gtk.init
Mltpp::Factory::init
Player.new( files )
Gtk.main

