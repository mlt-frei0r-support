MLT++
-----

	This mlt sub-project provides a C++ wrapping for the MLT library.

INSTALLATION
------------

	./configure [ --prefix=path ]
	make
	make install

USAGE
-----

	Use the following definitions in a Makefile to compile and link with mlt++:

		CXXFLAGS=`mlt-config -Wall`
		LDFLAGS=-lmlt++

	Include files for the classes can either be explicitly included, ie:

		#include <mlt++/MltProducer.h>
		etc

	Or you can include all using:

		#include <mlt++/Mlt.h>

	All definitions are placed in an Mlt namespace, and adhere closely to the C
	naming convention. Mappings always follow the pattern:

	Factory methods:

		mlt_factory_init        ==> Mlt::Factory::init
		mlt_factory_producer    ==> Mlt::Factory::producer
		mlt_factory_filter      ==> Mlt::Factory::filter
		mlt_factory_transition  ==> Mlt::Factory::transition
		mlt_factory_consumer    ==> Mlt::Factory::consumer
		mlt_factory_close       ==> Mlt::Factory::close

	NB: Factory usage for service construction is optional.

	Types:

		mlt_properties          ==> Mlt::Properties
		mlt_frame               ==> Mlt::Frame
		mlt_service             ==> Mlt::Service
		mlt_producer            ==> Mlt::Producer
		mlt_filter              ==> Mlt::Filter
		mlt_transition          ==> Mlt::Transition
		mlt_consumer            ==> Mlt::Consumer

	Methods:

		mlt_type_method         ==> Mlt::Type.method
	ie:	mlt_playlist_append     ==> Mlt::Playlist.append

	Parent methods are available directly on children.

	Additionally, you can specify:

		using namespace Mlt;

	To avoid the enforced use of the Mlt:: prefix.

	Enumerators and macros are reused directly from the C library.

CLASS HIERARCHY
---------------

	The currently mapped objects are shown in the following hierarchy:

		Factory
		Properties
			Frame
			Service
				Consumer
				Field
				Filter
				Multitrack
				Producer
					Playlist
				Tractor
				Transition

	An additional set of classes allow apps to behave as, and communicate with,
	client/server components - these components provide MLT with unique 
	possibilties for process to process or system to system communications.
	
		Miracle
		Response

SPECIAL CASES
-------------

	Care should be taken with wrapper objects. 

	Taking, as an example, the C function that returns the immediate consumer of
	a service:

		mlt_service mlt_service_consumer( mlt_service );

	This maps to:

		Mlt::Service *Mlt::Service.consumer( );

	Note that you get an object back - it is never the original c++ object, but 
	a wrapping object. This is done to keep consistency with the C api which may 
	instantiate C instances - therefore it cannot be assumed that a C++ object 
	exists for all mlt service instances.

	As such, it is mandatory that you delete these objects. The original will 
	not be affected. However, all other modifications (to properties or its
	state of connection) will be reflected in the original object.

	This approach excludes the use of RTTI to determine the real type of the 
	object - this can only be done by parsing the objects properties.

	Objects may be invalid - always use the is_valid method to check validity 
	before use.

LIMITATIONS
-----------

	The mechanisms for the definition of new services are deliberately 
	excluded from the C++ wrappings - this is done to ensure that service 
	networks constructed can be serialised and used by existing applications
	which are based on the C API (such as miracle).

SWIG
----

	Experimental swig bindings based on mlt++ are provided.

