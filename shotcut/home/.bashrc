# .bashrc

# User specific aliases and functions
alias vi=gvim

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi
