# .bash_profile
 
# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi
 
# User specific environment and startup programs
export MLT_CONSUMER=valerie
export MLT_WESTLEY_DEEP=true
 
PATH=$PATH:$HOME/bin
 
export PATH
unset USERNAME

