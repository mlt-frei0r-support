/**
 * NLE_Browser.cpp - Provides the NLE browser component
 * Copyright (C) 2004-2005 Charles Yates
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdlib.h>
#include <string.h>

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include "NLE_Browser.h"
#include "NLE_Project.h"
using namespace shotcut;

NLE_Browser::NLE_Browser( int x, int y, int w, int h ) :
	Fl_Browser_( x, y, w, h ),
	first( NULL ),
	last( NULL ),
	lines( 0 ),
	showing( PROJECTS | STORIES | AUDIO | STILLS ),
	active( false )
{
	type( FL_SELECT_BROWSER );
	resizable( this );
	set_visible_focus( );
	textsize( 12 );
}

NLE_Browser::~NLE_Browser( )
{
	clear( );
}

void NLE_Browser::selection_cb( )
{
	static int doing = 0;
	if ( doing == 0 )
	{
		doing = 1;
		if ( first != NULL && selection( ) != NULL )
			NLE::Change_Mode( browser_mode, true );
		doing = 0;
	}
}

int NLE_Browser::handle( int e )
{
	NLE_Producer *selected = ( NLE_Producer * )selection( );
	int ret = 0;

	active = true;

	switch( e )
	{
		case FL_PUSH:
			selection_color( NLE_HOT );
			Fl_Browser_::handle( e );
			take_focus( );
			selection_cb( );
			ret = 1;
			break;

		case FL_RELEASE:
			ret = 1;
			break;

		case FL_DRAG:
			if ( selected )
				NLE::Forward_Drop_Object( selected->producer );
			ret = 1;
			break;

		case FL_FOCUS:
			selection_color( NLE_HOT );
			redraw( );
			ret = 1;
			break;

		case FL_UNFOCUS:
			selection_color( NLE_COLD );
			redraw( );
			ret = 1;
			break;

		case FL_KEYDOWN:
			if ( Fl::event_key() == FL_Up )
			{
				if ( selected == NULL || selected == item_first( ) )
					select_only( item_last( ) );
				else
					select_only( item_prev( selected ) );
				selection_cb( );
				ret = 1;
			}
			else if ( Fl::event_key( ) == FL_Enter || Fl::event_key() == FL_Down )
			{
				if ( selected != NULL && Fl::event_key( ) == FL_Enter && Fl::event_state( FL_CTRL ) )
					NLE::Insert_Into_Project( selected->producer );
				if ( selected == NULL || selected == last )
					select_only( item_first( ) );
				else
					select_only( item_next( selected ) );
				selection_cb( );
				ret = 1;
			}
			break;

		case FL_KEYUP:
			break;

		default:
			ret = Fl_Browser_::handle( e );
			break;
	}

	active = false;
	return ret;
}

// Changes the selection without invoking a callback
void NLE_Browser::set_selection( Producer *producer )
{
	if ( producer != NULL )
	{
		NLE_Producer *p = ( NLE_Producer * )item_first( );
		while ( p != NULL )
		{
			if ( p->producer != NULL && p->producer->same_clip( *producer ) )
				break;
			p = ( NLE_Producer * )item_next( p );
		}
		if ( active == false && p != selection( ) )
			select_only( p );
	}
	else
	{
		select_only( NULL );
	}
}

void NLE_Browser::clear( )
{
	NLE_Producer *p = first;
	while ( p != NULL )
	{
		NLE_Producer *n = p->next;
		delete p->producer;
		free( p->document );
		free( p->title );
		free( p );
		p = n;
	}
	first = NULL;
	last = NULL;
	lines = 0;
	new_list( );
}

void NLE_Browser::clean( )
{
	int count;

	do 
	{
		count = 0;
		NLE_Producer *p = ( NLE_Producer * )item_first( );
		while ( p != NULL )
		{
			NLE_Producer *next = ( NLE_Producer * )item_next( p );
			NLE_Producer *n = p->next;
			if ( p->producer->ref_count( ) <= 1 )
			{
				if ( p->prev ) p->prev->next = n;
				if ( n ) n->prev = p->prev;
				if ( p == first ) first = n;
				if ( p == last ) last = p->prev;
				delete p->producer;
				free( p->document );
				free( p->title );
				free( p );
				lines --;
				count ++;
			}
			p = next;
		}
	}
	while( count != 0 );

	new_list( );
	redraw( );
}

void NLE_Browser::display( int flags )
{
	showing = flags;
	NLE_Producer *p = first;
	while ( p != NULL )
	{
		p->hidden = !( p->producer->get_int( "nle_type" ) & showing );
		p = p->next;
	}
	new_list( );
	redraw( );
}

void NLE_Browser::add( char *title, char *document )
{
	Fl::lock( );
	insert( lines + 1, title, document );
	Fl::check( );
	Fl::unlock( );
}

void NLE_Browser::insert( int position, char *title, char *document )
{
	char *real_title = ( char * )malloc( strlen( title ) + 10 );
	strcpy( real_title, title );
	int instances = 1;
	NLE_Producer *p = first;

	// Ensure that the title is unique
	while ( p != NULL )
	{
		if ( !strcmp( p->title, real_title ) )
			sprintf( real_title, "%s (%d)", title, ++ instances );
		p = p->next;
	}

	NLE_Producer *producer = ( NLE_Producer * )calloc( 1, sizeof( NLE_Producer ) );
	producer->title = real_title;
	producer->document = strdup( document );
	NLE::Load_Producer( producer );
	producer->hidden = !( producer->producer->get_int( "nle_type" ) & showing );
	if ( producer->producer != NULL && producer->producer->get_int( "_no_show" ) == 0 )
	{
		insert( position, producer );
	}
	else
	{
		delete producer->producer;
		free( producer );
	}
}

void NLE_Browser::refresh( Producer &producer )
{
	char *title = producer.get( "title" );
	if ( title == NULL )
		title = producer.get( "nle_title" );

	// If no title, then don't track it in the browser
	if ( title == NULL )
		return;

	NLE_Producer *p = first;
	while ( p != NULL )
	{
		if ( !strcmp( p->title, title ) )
		{
			if ( producer.get_parent( ) != p->producer->get_parent( ) )
			{
				int len;
				uint8_t *thumb = ( uint8_t * )p->producer->get_data( "thumb", len );
				p->producer->set( "thumb", thumb, len );
				producer.set( "nle_type", p->producer->get_int( "nle_type" ) );
				producer.set( "thumb", thumb, len, mlt_pool_release );
				delete p->producer;
				p->producer = new Producer( producer );
			}
			return;
		}
		p = p->next;
	}

	// Add a new NLE_Producer
	p = ( NLE_Producer * )calloc( 1, sizeof( NLE_Producer ) );
	p->producer = new Producer( producer );
	p->title = strdup( title );
	NLE::Load_Producer( p );
	p->hidden = !( p->producer->get_int( "nle_type" ) & showing );
	insert( lines + 1, p );
}

void NLE_Browser::refresh( Tractor *tractor )
{
	if ( tractor )
	{
		NLE_Project project( tractor );
		for ( int i = 0; i < project.tracks( ); i ++ )
		{
			for( int j = 0; j < project.count( i ); j ++ )
			{
				if ( !project.is_blank( i, j ) )
				{
					Producer *clip = project.get_cut( i, j );
					if ( clip != NULL )
						refresh( clip->parent( ) );
					delete clip;
				}
			}
		}
	}
}

void NLE_Browser::refresh( Playlist *playlist )
{
	for ( int i = 0; i < playlist->count( ); i ++ )
	{
		Producer *clip = playlist->get_clip( i );
		if ( clip != NULL )
		{
			if ( clip->parent( ).get( "title" ) == NULL )
			{
				char *title = clip->parent( ).get( "resource" );
				if ( strchr( title, '/' ) ) title = strrchr( title, '/' ) + 1;
					clip->parent( ).set( "title", title );
			}
			refresh( clip->parent( ) );
		}
		delete clip;
	}
}

void NLE_Browser::insert( int position, NLE_Producer *p )
{
	if ( first == NULL )
	{
		first = last = p;
		p->next = p->prev = NULL;
	}
	else if ( position >= lines )
	{
		last->next = p;
		p->prev = last;
		p->next = NULL;
		last = p;
	}
	else if ( position <= 0 )
	{
		if ( !p->hidden )
			inserting( first, p );
		first->prev = p;
		p->next = first;
		p->prev = NULL;
		first = p;
	}
	else
	{
		NLE_Producer *i = find( position - 1 );
		if ( !p->hidden )
			inserting( i, p );
		p->next = i->next;
		p->prev = i;
		i->next->prev = p;
		i->next = p;
	}
	lines ++;
	if ( !p->hidden )
		redraw_line( p );
}

NLE_Producer *NLE_Browser::find( int position )
{
	NLE_Producer *p = ( NLE_Producer * )item_first( );
	while ( p != NULL && position -- )
		p = ( NLE_Producer * )item_next( p );
	return p;
}

/** This method must be provided by the subclass to return the first item in the list. */
void *NLE_Browser::item_first() const
{
	NLE_Producer *p = first;
	while ( p != NULL && p->hidden )
		p = p->next;
	return p;
}

/** This method must be provided by the subclass to return the first item in the list. */
void *NLE_Browser::item_last() const
{
	NLE_Producer *p = last;
	while ( p != NULL && p->hidden )
		p = p->prev;
	return p;
}

/** This method must be provided by the subclass to return the item in the list after \c p. */
void *NLE_Browser::item_next(void *p) const
{
	p = ( ( NLE_Producer * ) p )->next;
	while ( p != NULL && ( ( NLE_Producer * )p )->hidden )
		p = ( ( NLE_Producer * ) p )->next;
	return p;
}

/** This method must be provided by the subclass to return the item in the list before \c p. */
void *NLE_Browser::item_prev(void *p) const
{
	p = ( ( NLE_Producer * ) p )->prev;
	while ( p != NULL && ( ( NLE_Producer * )p )->hidden )
		p = ( ( NLE_Producer * ) p )->prev;
	return p;
}

/** This method must be provided by the subclass to return the height of the item \c p
* in pixels. Allow for two additional pixels for the list selection box. */
int NLE_Browser::item_height(void *p) const
{
	return 36;
}

/** This method must be provided by the subclass to return the width of the item \c p in 
* pixels. Allow for two additional pixels for the list selection box. */
int NLE_Browser::item_width(void *p) const
{
	NLE_Producer *nle_producer = ( NLE_Producer * )p;
	const char *str = nle_producer->title;
	fl_color( textcolor( ) );
	return 32 + ( int )fl_width( str );
}

/** This method may be provided by the subclass to return the height of the item \c p 
* in pixels. Allow for two additional pixels for the list selection box. This method 
* differs from item_height in that it is only called for selection and scrolling 
* operations. The default implementation calls item_height. */
//int item_quick_height(void *p) const { return 32; }

/** This method must be provided by the subclass to draw the item p in the area 
* indicated by x, y, w, and h. */
void NLE_Browser::item_draw(void *p,int x,int y,int width,int height) const
{
	const char *str = ( ( NLE_Producer * )p )->title;
	if ( ( ( NLE_Producer * )p )->producer != NULL )
	{
		int size;
		unsigned char *thumb = ( unsigned char * )( ( NLE_Producer * )p )->producer->get_data( "thumb", size );
		fl_draw_image( thumb, x, y, 44, 36 );
	}

	fl_color( textcolor( ) );
	fl_font(textfont(), 10 );
	fl_draw( str, x+48, y, width - 6, height, FL_ALIGN_LEFT );
}

// you don't have to provide these but it may help speed it up:
/** This method may be provided by the subclass to indicate the full width of the item 
* list in pixels. The default implementation computes the full width from the
* item widths. */
//int NLE_Browser::full_width() const { }

/** This method may be provided by the subclass to indicate the full height of the item 
* list in pixels. The default implementation computes the full height from the 
* item heights. */
//int NLE_Browser::full_height() const { }

/** This method may be provided to return the average height of all items, to be used 
* for scrolling. The default implementation uses the height of the first item. */
//int NLE_Browser::incr_height() const { }

