/**
 * NLE.h - Provides access to the main NLE singleton
 * Copyright (C) 2004-2005 Charles Yates
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _NLE_H_
#define _NLE_H_

#include <FL/Enumerations.H>

#include <mlt++/Mlt.h>
using namespace Mlt;

#include "NLE_Types.h"
#include "NLE_Project.h"
using namespace shotcut;

namespace NLE
{
	// Get the global config properties
	Properties &Get_Config( );

	// Invoked to change between different modes
	void Change_Mode( nle_mode, bool force = false );

	// Obtain the current mode
	nle_mode Current_Mode( );

	// Turn on/off undo state handling on project modification
	// This is not related to mode - modifications can occur to the
	// project whilst in browser and disabled mode
	void Ignore_Changes( bool );
	bool Ignore_Changes( );

	// Handle undo state now
	void Check_Point( );

	// Obtain the current project
	NLE_Project &Project( );

	// Invoked by server component - these are sent to the browser for 
	// retitling and the browser then calls back to the Load_Producer 
	// method to physically load and apply rules for current current 
	// project
	void Forward_Browser( char *title, char *doc );

	// Invoked by browser on receipt of a new document
	void Load_Producer( NLE_Producer * );

	// The item that is currently selected (in browser mode, this is the 
	// selected item, in project and preview mode, it's the current cut, 
	// in disabled mode, it's NULL)
	Producer *Fetch_Selected( );

	// Playback speed request
	void Set_Preview_Speed( double speed );

	// Forward the item for drop handling
	void Forward_Drop_Object( Producer *producer, const char *info = "" );

	// Fetch the current drop item
	Producer *Fetch_Drop_Object( );

	// Highlight this producer in the browser
	void Highlight_In_Browser( Producer *producer );

	// Refresh the multitrack after a position change
	void Refresh_Multitrack( bool force = false );

	// Force a refresh on the preview
	void Refresh_Preview( );

	// Seek to the position specified
	void Change_Position( int position, PositionChanger changer, bool request );

	// Resize a cut 
	void Resize_Cut( Producer *producer, int &in, int &out, int position = -1 );

	// Insert this producer into the project (in the the current track
	// and at the current position according to the rules of the track)
	void Insert_Into_Project( Producer *producer );
}

// Global colours
#define NLE_HOT			( Fl_Color )( FL_FREE_COLOR + 0 )
#define NLE_WARM		( Fl_Color )( FL_FREE_COLOR + 1 )
#define NLE_LUKE		( Fl_Color )( FL_FREE_COLOR + 2 )
#define NLE_COLD		( Fl_Color )( FL_FREE_COLOR + 3 )

#endif
