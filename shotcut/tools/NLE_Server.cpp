/**
 * NLE_Server.cpp - Provides the NLE server component
 * Copyright (C) 2004-2005 Charles Yates
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdlib.h>
#include <string.h>
#include "NLE_Server.h"
using namespace shotcut;

// TODO: This should be wrapped properly in mlt++
#include <valerie/valerie_remote.h>

NLE_Server::NLE_Server( char *id, int port ) :
	Miracle( id, port )
{
	set( "push-parser-off", 1 );
	shutdown_old_server( port );
	start( );
}

// Shutdown the old instance to make way for the new
void NLE_Server::shutdown_old_server( int port )
{
	valerie_parser parser = valerie_parser_init_remote( "localhost", port );
	valerie_response response = valerie_parser_connect( parser );
	if ( response != NULL )
	{
		valerie_response_close( response );
		response = valerie_parser_execute( parser, "shutdown" );
		if ( response != NULL )
			valerie_response_close( response );
	}
	valerie_parser_close( parser );
}

// We only accept a shutdown command at the moment
Response *NLE_Server::execute( char *command )
{
	valerie_response response = valerie_response_init( );
	if ( !strcmp( command, "shutdown" ) )
		exit( 0 );
	return new Response( response );
}

// Push method
Response *NLE_Server::received( char *command, char *doc )
{
	valerie_response response = valerie_response_init( );
	if ( doc != NULL )
	{
		Tokeniser tokens( command );
		Properties properties;
		properties.set( "title", "Unknown submission" );
		for ( int i = 0; i < tokens.count( ); i ++ )
			if ( strchr( tokens.get( i ), '=' ) )
				properties.parse( tokens.get( i ) );
		valerie_response_set_error( response, 200, "OK" );
		NLE::Forward_Browser( properties.get( "title" ), doc );
	}
	return new Response( response );
}

