/*
 * Panel_Obscure.h -- Obscure panel
 * Copyright (C) 2004-2005 Ushodaya Enterprises Limited
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <string.h>

#include "NLE.h"
#include "Panel_Obscure.h"
#include "AvCutter.h"
using namespace shotcut;

#include <FL/Fl_Choice.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl_Positioner.H>
#include <FL/Fl_Value_Slider.H>
#include <FL/Fl_Scroll.H>
#include <FL/Fl_Value_Input.H>
#include <FL/Fl_Button.H>
#include <FL/fl_draw.H>

Fl_Menu_Item area_menu[] = 
{
  	{ "Primary", 	0, 0, ( void * )"0" },
  	{ "Secondary", 	0, 0, ( void * )"1" },
  	{ NULL }
};

Fl_Menu_Item choice_menu[] = 
{
  	{ "None",	 	0, 0, ( void * )"0" },
  	{ "Rectangle", 	0, 0, ( void * )"1" },
  	{ "Circle",	 	0, 0, ( void * )"2" },
  	{ NULL }
};

class NLE_Positioner : public Fl_Positioner
{
	private:
		int vw;
		int vh;
		Fl_Boxtype vb;

	public:
		NLE_Positioner( int x, int y, int w, int h ) :
			Fl_Positioner( x, y, w, h ),
			vw( 200 ),
			vh( 200 ),
			vb( FL_BORDER_FRAME )
		{
		}

		void set_size( int w, int h )
		{
			bool changed = false;
			if ( w >= 0 && vw != w )
			{
				vw = w;
				changed = true;
			}
			if ( h >= 0 && vh != h )
			{
				vh = h;
				changed = true;
			}
			if ( changed )
				damage( FL_DAMAGE_ALL );
		}

		int get_w( )
		{
			return vw;
		}

		int get_h( )
		{
			return vh;
		}

		void set_shape( Fl_Boxtype value )
		{
			if ( value != vb )
			{
				vb = value;
				damage( FL_DAMAGE_ALL );
			}
		}

		void draw( )
		{
			if ( w( ) > 0 && h( ) > 0 )
			{
				Fl_Positioner::draw( );
				double vx = Fl_Positioner::xvalue( );
				double vy = Fl_Positioner::yvalue( );
				double bx = vx - vw / 2;
				double by = vy - vh / 2;
				double sx = ( double )w( ) / 720.0;
				double sy = ( double )h( ) / 576.0;
				fl_push_clip( x( ), y( ), w( ), h( ) );
				fl_draw_box( vb, x( ) + ( int )( bx * sx ), y( ) + ( int )( by * sy ), ( int )( vw * sx ), ( int )( vh * sy ), FL_BLUE );
				fl_pop_clip( );
			}
		}
};

class NLE_Obscure : public NLE_Panel
{
	private:
		Geometry geometry;
		Producer *current;
		int cut_offset, cut_in, cut_out, cut_length, cut_flags;
		Fl_Choice *area;
		Fl_Choice *choice;
		NLE_Positioner *positioner;
		Fl_Slider *rollers[ 2 ];
		Fl_Group *inner;
		Fl_Button *buttons[ 5 ];
		AvCutter *cutter;

	public:
		NLE_Obscure( int x, int y, int w, int h, const char *label ) :
			NLE_Panel( x, y, w, h, label ),
			geometry( ),
			current( NULL ),
			cut_offset( 0 ),
			cut_in( 0 ),
			cut_out( 0 ),
			cut_length( 0 ),
			cut_flags( 0 )
		{
			int sx = x + 5;
			int sy = y + 20;
			int sw = w - 10;

			// Main group
			area = new Fl_Choice( sx, sy, sw, 20, "Area" );
			area->align( FL_ALIGN_TOP | FL_ALIGN_LEFT );
			area->menu( area_menu );
			area->callback( static_area_cb, this );
			area->labelsize( 10 );

			// Create the choice of mask
			sy += 30;
			choice = new Fl_Choice( sx, sy, sw, 20, "Mask" );
			choice->align( FL_ALIGN_TOP | FL_ALIGN_LEFT );
			choice->menu( choice_menu );
			choice->callback( static_choice_cb, this );
			choice->labelsize( 10 );

			// Inner group
			sy += 35;
			inner = new Fl_Group( sx, sy, sw, h - sy - 50 );

			// On the right of the scroll, we have the positioner and width/height rollers
			positioner = new NLE_Positioner( sx, sy, sw - 15, sw * 3 / 4 );
			positioner->callback( static_positioner_cb, this );
			rollers[ 0 ] = new Fl_Slider( sx, sy + positioner->h( ), sw - 20, 15 );
			rollers[ 0 ]->type( FL_HOR_NICE_SLIDER );
			rollers[ 0 ]->callback( static_roller_cb, this );
			rollers[ 1 ] = new Fl_Slider( sw - 15, sy, 15, positioner->h( ) );
			rollers[ 1 ]->type( FL_VERT_NICE_SLIDER );
			rollers[ 1 ]->callback( static_roller_cb, this );

			buttons[ 0 ] = new Fl_Button( sx, y + 100 + positioner->h( ), 60, 20, "&Prev" );
			buttons[ 1 ] = new Fl_Button( sx + sw / 3 - 20 , y + 100 + positioner->h( ), 60, 20, "&Remove" );
			buttons[ 2 ] = new Fl_Button( sx + 2 * sw / 3 - 40, y + 100 + positioner->h( ), 60, 20, "&End" );
			buttons[ 3 ] = new Fl_Button( sx + sw - 60, y + 100 + positioner->h( ), 60, 20, "&Next" );
			buttons[ 4 ] = new Fl_Button( sx, y + 100 + positioner->h( ) + 25, sw, 20, "&Clear All" );
			for ( int i = 0; i < 5; i ++ )
			{
				 buttons[ i ]->clear_visible_focus( );
				 buttons[ i ]->labelsize( 10 );
				 buttons[ i ]->callback( static_buttons_cb, this );
			}

			// All widgets created
			inner->resizable( positioner );
			inner->end( );
			cutter = new AvCutter( x, y + h - 50, w, 50 );
			cutter->set_values( 0, 0, 999, 1000 );
			cutter->fixate( IN | OUT | DND | INSERT );
			cutter->locked( true );
			cutter->callback( static_cutter_cb, this );
			resizable( inner );
			end( );

			positioner->xbounds( 0, 719 );
			positioner->ybounds( 0, 575 );
			positioner->xstep( 1 );
			positioner->ystep( 1 );
			positioner->value( 360, 288 );
			positioner->set_size( 200, 200 );
			rollers[ 0 ]->maximum( 719 );
			rollers[ 0 ]->step( 1 );
			rollers[ 0 ]->value( 200 );
			rollers[ 1 ]->minimum( 575 );
			rollers[ 1 ]->maximum( 0 );
			rollers[ 1 ]->step( 1 );
			rollers[ 1 ]->value( 200 );
		}

		~NLE_Obscure( )
		{
			delete current;
		}

		void update( )
		{
			delete current;
			current = NULL;

			switch( NLE::Current_Mode( ) )
			{
				case disable_mode:
				case preview_mode:
				case browser_mode:
					break;

				case project_mode:
					current = NLE::Project( ).cut_info( cut_offset, cut_in, cut_out, cut_length, cut_flags );
					break;
			}

			if ( current != NULL )
			{
				int region = area->value( );
				char temp[ 512 ];
				sprintf( temp, "meta.attr.obscure%d", region );
				if ( current->get_int( temp ) )
				{
					sprintf( temp, "meta.attr.obscure%d.resource", region );
					char *resource = current->get( temp );
					choice->value( resource == NULL || strcmp( resource, "circle" ) ? 1 : 2 );
					positioner->set_shape( choice->value( ) == 1 ? FL_BORDER_FRAME : FL_OVAL_FRAME );
					sprintf( temp, "meta.attr.obscure%d.geometry", region );
					char *data = current->get( temp );
					geometry.parse( data, current->parent( ).get_playtime( ) );
					// Update the display to show the geometry
					refresh_geometry( );
					// Activate the inner group
					inner->show( );
					cutter->set_values( NLE::Project( ).position( ) - cut_offset + cut_in, -1, -1, cut_length );
					cutter->show( );
				}
				else
				{
					choice->value( 0 );
					inner->hide( );
					cutter->hide( );
				}
				activate( );
			}
			else
			{
				choice->value( 0 );
				inner->hide( );
				cutter->hide( );
				deactivate( );
			}
		}

		void set_position( int value )
		{
			refresh_geometry( );
		}

		static void static_area_cb( Fl_Widget *widget, void *obj )
		{
			( ( NLE_Obscure * )obj )->update( );
		}

		static void static_choice_cb( Fl_Widget *widget, void *obj )
		{
			( ( NLE_Obscure * )obj )->choice_cb( );
		}

		void choice_cb( )
		{
			int region = area->value( );
			char temp[ 512 ];
			clear_dirty( );
			sprintf( temp, "meta.attr.obscure%d", region );
			NLE::Ignore_Changes( false );
			dirty( false );
			NLE::Project( ).block( );
			if ( choice->value( ) )
			{
				current->set( temp, 1 );
				sprintf( temp, "meta.attr.obscure%d.absolute", region );
				current->set( temp, 1 );
				sprintf( temp, "meta.attr.obscure%d.resource", region );
				current->set( temp, ( char * )( choice->value( ) == 1 ? "rectangle" : "circle" ) );
				sprintf( temp, "meta.attr.obscure%d.geometry", region );
				if ( current->get( temp ) == NULL )
				{
					char temp2[ 512 ];
					sprintf( temp2, "%d=260,188:200x200", NLE::Project( ).position( ) - cut_offset + current->get_in( ) );
					current->set( temp, temp2 );
				}
			}
			else
			{
				current->set( temp, 0 );
			}
			NLE::Project( ).unblock( );
			NLE::Refresh_Preview( );
			update( );
		}

		static void static_buttons_cb( Fl_Widget *widget, void *obj )
		{
			( ( NLE_Obscure * )obj )->buttons_cb( ( Fl_Button * )widget );
		}

		void buttons_cb( Fl_Button *button )
		{
			GeometryItem item;
			int position = NLE::Project( ).position( ) - cut_offset + current->get_in( );
			int region = area->value( );
			char temp[ 512 ];

			clear_dirty( );

			if ( button == buttons[ 0 ] )
			{
				if ( geometry.prev_key( item, position - 1 ) == 0 )
				{
					int frame = cut_offset + item.frame( ) - current->get_in( );
					if ( frame >= NLE::Project( ).position( ) || frame < cut_offset )
						frame = cut_offset;
					NLE::Change_Position( frame, panel_position, true );
				}
			}
			else if ( button == buttons[ 1 ] )
			{
				NLE::Project( ).block( );
				geometry.remove( position );
				char *data = geometry.serialise( );
				sprintf( temp, "meta.attr.obscure%d.geometry", region );
				current->set( temp, data );
				if ( data == NULL || !strcmp( data, "" ) )
				{
					choice->value( 0 );
					current->set( temp, ( char * )NULL );
					sprintf( temp, "meta.attr.obscure%d", region );
					current->set( temp, 0 );
				}
				NLE::Project( ).unblock( );
				NLE::Refresh_Preview( );
				update( );
			}
			else if ( button == buttons[ 2 ] )
			{
				update_geometry( );
				GeometryItem item;
				item.frame( position + 1 );
				item.x( 360 );
				item.y( 288 );
				item.w( 0 );
				item.h( 0 );
				item.mix( 100 );

				NLE::Project( ).block( );
				geometry.insert( item );
				char *data = geometry.serialise( );
				sprintf( temp, "meta.attr.obscure%d.geometry", region );
				current->set( temp, data );
				NLE::Project( ).unblock( );
				NLE::Refresh_Preview( );
				update( );
			}
			else if ( button == buttons[ 3 ] ) 
			{
				if ( geometry.next_key( item, position + 1 ) == 0 )
					NLE::Change_Position( cut_offset + item.frame( ) - current->get_in( ), panel_position, true );
			}
			else if ( button == buttons[ 4 ] ) 
			{
				NLE::Project( ).block( );
				sprintf( temp, "meta.attr.obscure%d.geometry", region );
				current->set( temp, "" );
				NLE::Project( ).unblock( );
				NLE::Refresh_Preview( );
				choice->value( 0 );
				choice_cb( );
			}

			refresh_geometry( );
		}

		static void static_positioner_cb( Fl_Widget *widget, void *obj )
		{
			( ( NLE_Obscure * )obj )->positioner_cb( );
		}

		void positioner_cb( )
		{
			NLE::Ignore_Changes( true );
			dirty( true );
			if ( positioner->get_w( ) == 0 || positioner->get_h( ) == 0 )
			{
				positioner->set_size( 200, 200 );
				rollers[ 0 ]->value( 200 );
				rollers[ 1 ]->value( 200 );
			}
			update_geometry( );
		}

		static void static_roller_cb( Fl_Widget *widget, void *obj )
		{
			( ( NLE_Obscure * )obj )->roller_cb( );
		}

		void roller_cb( )
		{
			NLE::Ignore_Changes( true );
			dirty( true );
			positioner->set_size( ( int )rollers[ 0 ]->value( ), ( int )rollers[ 1 ]->value( ) );
			update_geometry( );
		}

		static void static_cutter_cb( Fl_Widget *widget, void *obj )
		{
			( ( NLE_Obscure * )obj )->cutter_cb( );
		}

		void cutter_cb( )
		{
			NLE::Change_Position( cut_offset + cutter->position( ) - cut_in, panel_position, true );
			refresh_geometry( );
		}

		void refresh_geometry( )
		{
			if ( current != NULL )
			{
				GeometryItem item;
				float position = ( float )( NLE::Project( ).position( ) - cut_offset + current->get_in( ) );
				geometry.fetch( item, position );
				float x = item.x( ) + item.w( ) / 2;
				float y = item.y( ) + item.h( ) / 2;
				if ( item.key( ) )
				{
					buttons[ 1 ]->activate( );
					positioner->set_shape( choice->value( ) == 1 ? FL_BORDER_BOX : FL_OVAL_BOX );
				}
				else
				{
					buttons[ 1 ]->deactivate( );
					positioner->set_shape( choice->value( ) == 1 ? FL_BORDER_FRAME : FL_OVAL_FRAME );
				}
				positioner->xvalue( x );
				positioner->yvalue( y );
				positioner->set_size( ( int )item.w( ), ( int )item.h( ) );
				rollers[ 0 ]->value( item.w( ) );
				rollers[ 1 ]->value( item.h( ) );

				GeometryItem next, prev;
				if ( geometry.next_key( next, ( int )position + 1 ) )
					next.frame( current->get_out( ) );
				geometry.prev_key( prev, ( int )position - 1 );
				if ( prev.frame( ) > item.frame( ) )
					prev.frame( current->get_in( ) );
				int prev_key = prev.frame( );
				int next_key = next.frame( );
				if ( prev_key < current->get_in( ) ) prev_key = current->get_in( );
				if ( next_key > current->get_out( ) ) next_key = current->get_out( );
				if ( next_key > current->get_out( ) ) next_key = current->get_out( );
				if ( prev_key < item.frame( ) && item.key( ) ) prev_key = item.frame( );
				if ( next_key != current->get_out( ) ) next_key --;
				cutter->set_values( ( int )position, prev_key, next_key, -1 );
			}
		}

		void update_geometry( )
		{
			if ( current != NULL )
			{
				NLE::Set_Preview_Speed( 0 );
				GeometryItem item;
				NLE::Project( ).block( );
				float x = positioner->xvalue( );
				float y = positioner->yvalue( );
				float w = rollers[ 0 ]->value( );
				float h = rollers[ 1 ]->value( );
				x -= w / 2;
				y -= h / 2;
				float position = ( float )( NLE::Project( ).position( ) - cut_offset + current->get_in( ) );
				item.frame( ( int )position );
				item.x( x );
				item.y( y );
				item.w( w );
				item.h( h );
				item.mix( 100 );
				geometry.insert( item );

				int region = area->value( );
				char temp[ 512 ];
				sprintf( temp, "meta.attr.obscure%d.geometry", region );

				current->set( temp, geometry.serialise( ) );
				NLE::Project( ).unblock( );
				NLE::Refresh_Preview( );
				refresh_geometry( );
			}
		}
};

NLE_Panel *shotcut::Panel_Obscure( int x, int y, int w, int h )
{
	return new NLE_Obscure( x, y, w, h, "Incognito" );
}

