/*
 * Panel_Comments.h -- Comments panel
 * Copyright (C) 2004-2005 Ushodaya Enterprises Limited
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "NLE.h"
#include "Panel_Comments.h"
using namespace shotcut;

#include <FL/Fl_Multiline_Input.H>

class NLE_Comments : public NLE_Panel
{
	private:
		Producer *current;
		Fl_Input *title;
		Fl_Multiline_Input *project_input;
		Fl_Multiline_Input *clip_input;
		Fl_Multiline_Input *cut_input;

	public:
		NLE_Comments( int x, int y, int w, int h, const char *label ) :
			NLE_Panel( x, y, w, h, label ),
			current( NULL )
		{
			title = new Fl_Input( x + 5, y + 15, w - 10, 24 );
			title->labelsize( 10 );
			title->label( "Project Title" );
			title->align( FL_ALIGN_LEFT | FL_ALIGN_TOP );
			title->callback( static_input_cb, this );
			title->textsize( 12 );
			h -= 35;
			project_input = new Fl_Multiline_Input( x + 5, y + 55, w - 10, h / 3 - 20 );
			project_input->labelsize( 10 );
			project_input->label( "Project Comments" );
			project_input->align( FL_ALIGN_LEFT | FL_ALIGN_TOP );
			project_input->wrap( 1 );
			project_input->callback( static_input_cb, this );
			project_input->textsize( 12 );
			clip_input = new Fl_Multiline_Input( x + 5, y + h / 3 + 50, w - 10, h / 3 - 20 );
			clip_input->label( "Clip Comments" );
			clip_input->labelsize( 10 );
			clip_input->wrap( 1 );
			clip_input->align( FL_ALIGN_LEFT | FL_ALIGN_TOP );
			clip_input->callback( static_input_cb, this );
			clip_input->textsize( 12 );
			cut_input = new Fl_Multiline_Input( x + 5, y + 2 * h / 3 + 50, w - 10, h / 3 - 20 );
			cut_input->labelsize( 10 );
			cut_input->label( "Cut Comments" );
			cut_input->align( FL_ALIGN_LEFT | FL_ALIGN_TOP );
			cut_input->wrap( 1 );
			cut_input->callback( static_input_cb, this );
			cut_input->textsize( 12 );
			end( );
		}

		~NLE_Comments( )
		{
			delete current;
		}

		static void static_input_cb( Fl_Widget *widget, void *obj )
		{
			( ( NLE_Comments * )obj )->input_cb( ( Fl_Multiline_Input * )widget );
		}

		void input_cb( Fl_Multiline_Input *input )
		{
			NLE::Project( ).block( );
			if ( input == title )
				NLE::Project( ).set( "title", ( char * )input->value( ) );
			else if ( input == project_input )
				NLE::Project( ).set( "nle_comments", ( char * )input->value( ) );
			else if ( input == clip_input )
				current->parent( ).set( "nle_comments", ( char * )input->value( ) );
			else if ( input == cut_input )
				current->set( "nle_comments", ( char * )input->value( ) );
			NLE::Project( ).unblock( );
		}

		void update( )
		{
			delete current;
			current = NULL;

			try
			{
				title->value( NLE::Project( ).get( "title" ) );
				project_input->value( NLE::Project( ).get( "nle_comments" ) );
				project_input->activate( );
			}
			catch( const char *e )
			{
				title->value( "" );
				project_input->value( "" );
				title->deactivate( );
				project_input->deactivate( );
			}

			Producer *clip = NLE::Fetch_Selected( );
			if ( clip != NULL )
			{
				clip_input->activate( );
				current = new Producer( clip );
				if ( current->parent( ).get( "nle_comments" ) != NULL )
					clip_input->value( ( char * )current->parent( ).get( "nle_comments" ) );
				else
					clip_input->value( "" );

				if ( NLE::Current_Mode( ) == project_mode )
				{
					cut_input->activate( );
					if ( current->get( "nle_comments" ) != NULL )
						cut_input->value( ( char * )current->get( "nle_comments" ) );
					else
						cut_input->value( "" );
				}
				else
				{
					cut_input->deactivate( );
					cut_input->value( "" );
				}
			}
			else
			{
				clip_input->deactivate( );
				clip_input->value( "" );
				cut_input->deactivate( );
				cut_input->value( "" );
			}
			delete clip;
		}
};

NLE_Panel *shotcut::Panel_Comments( int x, int y, int w, int h )
{
	return new NLE_Comments( x, y, w, h, "Comments" );
}

