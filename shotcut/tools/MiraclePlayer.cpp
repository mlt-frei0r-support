
#include "MiraclePlayer.h"
using namespace Mlt;
using namespace shotcut;

class MiracleServer : public Miracle
{
	private:
		AvPlayer *player;

	public:
		MiracleServer( AvPlayer *player_, char *id, int port ) :
			Miracle( id, port ),
			player( player_ )
		{
			start( );
		}

		~MiracleServer( )
		{
		}

		Response *execute( char *command )
		{
			valerie_response response = valerie_response_init( );
			return new Response( response );
		}
		
		Response *push( char *command, Service *service )
		{
			valerie_response response = valerie_response_init( );
			if ( service != NULL )
			{
				Producer producer( *service );
				char *mlt_service = producer.get( "mlt_service" );

				if ( mlt_service != NULL &&
					 ( !strcmp( mlt_service, "pixbuf" ) || 
					   !strcmp( mlt_service, "pango" ) ||
					   !strcmp( mlt_service, "colour" ) ) )
				{
					producer.set_in_and_out( 0, 249 );
					producer.set( "length", 250 );
				}

				valerie_response_set_error( response, 200, "OK" );
				player->play( producer );
			}
			return new Response( response );
		}
};

MiraclePlayer::MiraclePlayer( Properties &properties, int port, int x, int y, int w, int h ) :
	AvPlayer( properties, NULL, true, x, y, w, h )
{
	miracle = new MiracleServer( this, "MiraclePlayer", port );
}

MiraclePlayer::~MiraclePlayer( )
{
	delete miracle;
}


