
#include "config.h"
#include <FL/Fl.h>
#include <stdlib.h>
#include "MltPlayer.h"
using namespace Mlt;
using namespace fltk;

static void consumer_frame_show( mlt_consumer sdl, MltPlayer *self, mlt_frame frame_ptr )
{
	Frame frame( frame_ptr );
	self->frame_shown( frame );
}

void my_lock( )
{
	Fl::lock( );
}

void my_unlock( )
{
	Fl::unlock( );
}

MltPlayer::MltPlayer( int x, int y, int w, int h ) :
	AvPlayer( x, y, w, h ),
	event( NULL ),
	consumer( NULL ),
	current( NULL ),
	previous( NULL ),
	position( 0 )
{
	consumer = new Consumer( "sdl_preview:360x288" );
	event = consumer->listen( "consumer-frame-show", this, ( mlt_listener )consumer_frame_show );
	consumer->set( "app_locked", 1 );
	consumer->set( "app_lock", ( void * )my_lock, 0 );
	consumer->set( "app_unlock", ( void * )my_unlock, 0 );
}

MltPlayer::~MltPlayer( )
{
	if ( event != NULL )
		delete event;
	consumer->stop( );
	delete consumer;
	if ( previous != NULL )
		delete previous;
	if ( current != NULL )
		delete current;
}

bool MltPlayer::restart( )
{
	bool ret = current != NULL && consumer->is_stopped( ) && shown( ) && xid( ) != 0;
	if ( ret )
	{
		char temp[ 132 ];
		sprintf( temp, "%d", ( int )xid( ) );
		setenv( "SDL_WINDOWID", temp, 1 );
		consumer->set( "resize", 1 );
		consumer->set( "progressive", 1 );
		consumer->start( );
	}
	return ret;
}

void MltPlayer::on_hide( )
{
	consumer->stop( );
}

void MltPlayer::on_show( )
{
	restart( );
}

void MltPlayer::on_close( )
{
	consumer->stop( );
}

int MltPlayer::handle( int e )
{
	switch( e )
	{
		case FL_NO_EVENT:
			consumer->set( "changed", 1 );
			break;
		case FL_HIDE:
			if ( current != NULL )
				current->set_speed( 0 );
			break;
	}
	return AvPlayer::handle( e );
}

void MltPlayer::play( Producer &producer )
{
	if ( previous != NULL )
		delete previous;
	previous = current;
	current = new Producer( producer );
	if ( previous != NULL && 
		 current->get( "title" ) != NULL && 
		 previous->get( "title" ) != NULL &&
		 !strcmp( current->get( "title" ), previous->get( "title" ) ) )
	{
		current->set_speed( previous->get_speed( ) );
		current->seek( previous->position( ) );
	}
	consumer->connect( *current );
	restart( );
}

void MltPlayer::set_speed( double speed )
{
	if ( current != NULL )
		current->set_speed( 0 );
}

void MltPlayer::play( char *clip )
{
	Producer producer( clip );
	play( producer );
}

void MltPlayer::position_change( int value, int in, int out )
{
	if ( current != NULL )
	{
		current->set_in_and_out( in, out );
		current->seek( value );
	}
}

void MltPlayer::frame_shown( Frame &frame )
{
	if ( current != NULL )
	{
		indicate_speed( frame.get_double( "_speed" ) );
		mlt_position in = current->get_in( );
		mlt_position out = current->get_out( );
		mlt_position length = current->get_length( );
		position = frame.get_int( "_position" );
		if ( out > in )
		{
			set_position( position, in, out, true );
		}
		else
		{
			set_position( 0, 0, 1, true );
		}
	}
	else
	{
		set_position( 0, 0, 1, true );
	}
}

void MltPlayer::seek( int offset )
{
	if ( current != NULL )
	{
		int pos = position + offset;
		current->seek( pos < 0 ? 0 : pos > current->get_out( ) ? current->get_out( ) : pos );
	}
}

void MltPlayer::button_press( string id )
{
	if ( current != NULL )
	{
		if ( id == "start" )
			current->seek( 0 );
		else if ( id == "rew" )
		{
			if ( current->get_speed( ) != -12 )
			{
				current->set( "_mlt_player_rew", current->get_speed( ) );
				current->set_speed( -12 );
			}
			else
			{
				int speed = current->get_int( "_mlt_player_rew" );
				current->set_speed( speed );
			}
		}
		else if ( id == "prev" )
			current->set_speed( current->get_speed( ) == -1 ? 0 : -1 );
		else if ( id == "play" )
			current->set_speed( current->get_speed( ) == 1 ? 0 : 1 );
		else if ( id == "next" )
		{
			int in_next = current->get_int( "_mlt_player_next" );
			current->set_speed( in_next == 0 ? 1 : 0 );
			current->set( "_mlt_player_next", !in_next );
		}
		else if ( id == "ffwd" )
		{
			if ( current->get_speed( ) != 12 )
			{
				current->set( "_mlt_player_ffwd", current->get_speed( ) );
				current->set_speed( 12 );
			}
			else
			{
				int speed = current->get_int( "_mlt_player_ffwd" );
				current->set_speed( speed );
			}
		}
		else if ( id == "end" )
			current->seek( current->get_out( ) );
	}
}
