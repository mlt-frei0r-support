/*
 * shotcut-nle.cpp -- the main class(es) for the shotcut nle component
 * Copyright (C) 2004-2004 Ushodaya Enterprises Limited
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Light_Button.H>
#include <FL/Fl_Multiline_Input.H>
#include <FL/Fl_Tabs.H>
#include <FL/Fl_Tile.H>
#include <FL/fl_ask.H>
#include <FL/Fl_File_Chooser.H>

#include <mlt++/Mlt.h>
using namespace Mlt;

#include "AvUtils.h"
#include "AvWindow.h"
#include "AvPlayer.h"
#include "AvCutter.h"
#include "AvMultitrack.h"
#include "NLE_Project.h"
#include "NLE.h"
#include "NLE_Browser.h"
#include "NLE_Server.h"
#include "NLE_Panel.h"
#include "NLE_NewProject.h"
#include "Panel_Attributes.h"
#include "Panel_Comments.h"
#include "Panel_Super.h"
#include "Panel_Obscure.h"
using namespace shotcut;

/** The extended NLE Cutter adds buttons and position labels to the base AvCutter class.
 	Kept seperate from the NLE_ClipEditor to ease maintance.
*/

class NLE_Cutter : public Fl_Group
{
	private:
		// The preview we're associated to (can be NULL)
		AvPlayer *preview;
		// The real producer is the cut from the multitrack
		// when the producer is direct from the browser, this is NULL
		Producer *real;
		int real_offset;
		AvCutter cutter;
		Fl_Button button_in;
		Fl_Button button_lock;
		Fl_Button button_position;
		Fl_Button button_link;
		Fl_Button button_out;
		bool changed;

	public:
		NLE_Cutter( AvPlayer *_preview, int x, int y, int w, int h ) :
			Fl_Group( x, y, w, h ),
			preview( _preview ),
			real( NULL ),
			real_offset( 0 ),
			cutter( x, y + 14, w, h - 14 ),
			button_in( x, y, w / 6, 14, "In" ),
			button_lock( x + w / 6, y, w / 6, 14, "Lock" ),
			button_position( x + 2 * w / 6, y, 2 * w / 6, 14, "[Position]" ),
			button_link( x + 4 * w / 6, y, w / 6, 14, "Link" ),
			button_out( x + 5 * w / 6, y, w / 6, 14, "Out" ),
			changed( false )
		{
			cutter.callback( static_cutter_cb, this );
			button_in.callback( static_button_in_cb, this );
			button_out.callback( static_button_out_cb, this );
			button_lock.callback( static_button_lock_cb, this );
			button_link.callback( static_button_link_cb, this );
			button_in.labelsize( 10 );
			button_out.labelsize( 10 );
			button_lock.labelsize( 10 );
			button_link.labelsize( 10 );
			button_lock.type( FL_TOGGLE_BUTTON );
			button_link.type( FL_TOGGLE_BUTTON );
			button_in.clear_visible_focus( );
			button_out.clear_visible_focus( );
			button_lock.clear_visible_focus( );
			button_link.clear_visible_focus( );
			end( );
			button_position.clear_visible_focus( );
			button_position.box( FL_FLAT_BOX );
			button_position.down_box( FL_FLAT_BOX );
			button_position.labelsize( 10 );
		}

		~NLE_Cutter( )
		{
			delete real;
		}

		void set_real( Producer *producer, int offset = 0 )
		{
			delete real;
			real = producer != NULL ? new Producer( *producer ) : NULL;
			real_offset = offset;
		}

		int offset( )
		{
			return real_offset;
		}

		Producer *get_real( )
		{
			return real;
		}

		void set_values( int p, int in, int out, int length )
		{
			if ( !pushed( ) )
				cutter.set_values( p, in, out, length );
		}

		void position( int p )
		{
			if ( !pushed( ) )
				cutter.position( p );
		}

		bool pushed( )
		{
			return Fl::pushed( ) == &cutter || cutter.pressed( );
		}

		int in( )
		{
			return cutter.in( );
		}

		int out( )
		{
			return cutter.out( );
		}

		void fixate( int flags )
		{
			button_in.activate( );
			button_out.activate( );
			button_link.activate( );
			if ( flags & IN ) button_in.deactivate( );
			if ( flags & OUT ) button_out.deactivate( );
			if ( flags & ( IN | OUT ) ) button_link.deactivate( );
			cutter.fixate( flags );
		}

		static void static_cutter_cb( Fl_Widget *cutter, void *arg )
		{
			( ( NLE_Cutter * )arg )->cutter_cb( );
		}

		void drag( )
		{
			Producer *source = real == NULL && preview != NULL ? preview->get_current( ) : real;
			Producer *cut = source->cut( cutter.in( ), cutter.out( ) );
			NLE::Forward_Drop_Object( cut );
			delete cut;
		}

		void insert( )
		{
			set_focus( POSITION );
			Producer *source = preview->get_current( );
			Producer *cut = source->cut( cutter.in( ), cutter.out( ) );
			NLE::Insert_Into_Project( cut );
			delete cut;
			NLE::Change_Mode( project_mode, true );
		}

		void cutter_cb( )
		{
			// Determine absolute position after the operation (default is correct for non project related modes)
			int absolute_position = cutter.position( );

			// Correct for project modes
			if ( NLE::Current_Mode( ) == project_mode || NLE::Current_Mode( ) == preview_mode )
				absolute_position += real_offset - cutter.in( );
				
			if ( cutter.action( ) == NONE && changed )
			{
				if ( NLE::Current_Mode( ) == project_mode )
				{
					NLE::Ignore_Changes( false );
					NLE::Check_Point( );
				}
				changed = false;
				preview->refresh( );
			}
			else if ( real != NULL && ( cutter.in( ) != real->get_in( ) || cutter.out( ) != real->get_out( ) ) )
			{
				preview->set_speed( 0 );
				int in = cutter.in( );
				int out = cutter.out( );
				changed = true;
				if ( NLE::Current_Mode( ) == project_mode )
					NLE::Ignore_Changes( true );
				NLE::Resize_Cut( real, in, out, absolute_position );
			}
			else if ( cutter.position( ) == cutter.out( ) )
			{
				preview->set_speed( 0 );
			}

			if ( cutter.action( ) == DND )
				drag( );
			else if ( cutter.action( ) == INSERT )
				insert( );
			else if ( NLE::Current_Mode( ) == project_mode || NLE::Current_Mode( ) == preview_mode )
				NLE::Change_Position( cutter.position( ) + real_offset - cutter.in( ), cutter_position, true );
			else 
				NLE::Change_Position( cutter.position( ), cutter_position, true );
		}

		static void static_button_in_cb( Fl_Widget *button, void *arg )
		{
			( ( NLE_Cutter * )arg )->button_in_cb( );
		}

		void button_in_cb( )
		{
			if ( button_in.active( ) )
			{
				cutter.in( cutter.position( ) );
				cutter.position( cutter.in( ) );
			}
			cutter_cb( );
		}

		static void static_button_out_cb( Fl_Widget *button, void *arg )
		{
			( ( NLE_Cutter * )arg )->button_out_cb( );
		}

		void button_out_cb( )
		{
			if ( button_out.active( ) )
			{
				cutter.out( cutter.position( ) );
				cutter.position( cutter.out( ) );
			}
			cutter_cb( );
		}

		static void static_button_lock_cb( Fl_Widget *button, void *arg )
		{
			( ( NLE_Cutter * )arg )->button_lock_cb( );
		}

		void button_lock_cb( )
		{
			int i = in( );
			int o = out( );
			int p = cutter.position( );
			cutter.locked( button_lock.value( ) );
			if ( button_lock.value( ) )
				position( p < i ? i : p > o ? o : p );
			cutter_cb( );
		}

		static void static_button_link_cb( Fl_Widget *button, void *arg )
		{
			( ( NLE_Cutter * )arg )->button_link_cb( );
		}

		void button_link_cb( )
		{
			cutter.linked( button_link.value( ) == 1 );
		}

		void locked( bool value )
		{
			cutter.locked( value );
			button_lock.value( value );
			if ( preview->locked( ) )
				button_lock.deactivate( );
			else
				button_lock.activate( );
		}

		bool locked( )
		{
			return cutter.locked( );
		}

		void toggle_lock( )
		{
			if ( button_lock.active( ) )
			{
				button_lock.value( !button_lock.value( ) );
				button_lock_cb( );
			}
		}

		void set_position( int value, bool request )
		{
			switch( NLE::Current_Mode( ) )
			{
				case project_mode:
				case preview_mode:
					if ( value >= offset( ) && value <= offset( ) + out( ) - in( ) )
					{
						if ( request )
							position( value - offset( ) + in( ) );
						show_position( value - offset( ), out( ) - in( ) + 1 );
					}
					break;
				case browser_mode:
					if ( request ) 
						position( value );
					if ( locked( ) )
						show_position( value - in( ), out( ) - in( ) + 1 );
					else
						show_position( value, out( ) - in( ) + 1 );
					break;
				case disable_mode:
					fprintf( stderr, "Cutter position while disabled\n" );
					break;
			}
		}

		void show_position( int value, int value2 )
		{
			static char tmp[ 100 ];
			AvUtils::timecode( tmp, value );
			strcat( tmp, " / " );
			AvUtils::timecode( tmp + strlen( tmp ), value2 );
			button_position.label( tmp );
		}

		void set_focus( int flag )
		{
			cutter.set_focus( flag );
		}
};


typedef NLE_Panel *( *panel_ctor )( int, int, int, int );

class NLE_ClipDetails : public Fl_Tabs
{
	private:
		panel_ctor fn[ 4 ];
		NLE_Panel *page[ 4 ];

	public:
		NLE_ClipDetails( int x, int y, int w, int h ) :
			Fl_Tabs( x, y, w, h )
		{
			fn[ 0 ] = Panel_Comments;
			fn[ 1 ] = Panel_Attributes;
			fn[ 2 ] = Panel_Super;
			fn[ 3 ] = Panel_Obscure;

			for ( int i = 0; i < 4; i ++ )
			{
				page[ i ] = ( fn[ i ] )( x, y + 20, w, h - 20 );
				page[ i ]->hide( );
				page[ i ]->labelsize( 10 );
				page[ i ]->end( );
			}

			Fl_Group::current( )->resizable( page[ 0 ] );
			end( );
			Fl_Group::current( )->resizable( this );
			callback( static_tab_cb, this );
			value( page[ 0 ] );
			tab_cb( false );
			clear_visible_focus( );
		}

		~NLE_ClipDetails( )
		{
		}

		void change_page( int page_idx )
		{
			if ( page_idx >= 0 && page_idx < 4 )
			{
				value( page[ page_idx ] );
				tab_cb( );
			}
		}

		static void static_tab_cb( Fl_Widget *widget, void *obj )
		{
			( ( NLE_ClipDetails * )obj )->tab_cb( );
		}

		// Check if any panel is in a dirty state and check point as necessary
		void clear_dirty( )
		{
			bool dirty = false;
			NLE::Ignore_Changes( false );
			for ( int i = 0; i < 4; i ++ )
			{
				dirty |= page[ i ]->dirty( );
				page[ i ]->dirty( false );
			}
			if ( dirty )
				NLE::Check_Point( );
		}

		void tab_cb( bool update = true )
		{
			if ( update )
				clear_dirty( );
			for ( int i = 0; i < 4; i ++ )
				page[ i ]->labeltype( value( ) == page[ i ] ?  FL_NORMAL_LABEL : FL_NORMAL_LABEL );
			if ( update )
				( ( NLE_Panel * )value( ) )->update( );
		}

		void update( )
		{
			clear_dirty( );
			Producer *current = NLE::Fetch_Selected( );
			nle_mode mode = NLE::Current_Mode( );
			for ( int i = 0; i < 4; i ++ )
			{
				if ( i == 0 || ( page[ i ]->active_modes( ) & mode && current != NULL ) )
					page[ i ]->activate( );
				else
					page[ i ]->deactivate( );
			}
			delete current;
			( ( NLE_Panel * )value( ) )->update( );
		}

		void set_position( int position )
		{
			( ( NLE_Panel * )value( ) )->set_position( position );
		}
};

class NLE_ClipEditor : public AvWindow
{
	private:
		// This is the current cut we're working with
		Producer *cut;
		// These are set if we're working with the project
		Tractor *tractor;
		AvPlayer *preview;
		NLE_Cutter *cutter;
		NLE_ClipDetails *details;
		pthread_mutex_t mutex;
		Fl_Button *thumb;
		Fl_Image *image;
		Fl_Button *info;
		char text[ 2048 ];
	
	public:
		NLE_ClipEditor( Properties &properties, Fl_Slider *slider, int x, int y, int w, int h ) :
			AvWindow( x, y, w, h ),
			cut( NULL ),
			tractor( NULL ),
			preview( NULL ),
			details( NULL ),
			image( NULL )
		{
			// Since this is a window, set x and y to 0
			x = y = 0;

			// Create a tile widget to hold the preview and panel
			//Fl_Tile *tile = new Fl_Tile( x, y, w, h );

			int preview_w = 2 * w / 3;
			int panel_w = w - preview_w;

			// Create a group for the clip summary, preview and cutter
			Fl_Group *group = new Fl_Group( x, y, preview_w, h );

			Fl_Group *group2 = new Fl_Group( x, y, preview_w - 1, 48 );
			thumb = new Fl_Button( x + 5, y + 7, 44, 36 );
			thumb->clear_visible_focus( );
			thumb->box( FL_FLAT_BOX );
			thumb->down_box( FL_FLAT_BOX );
			info = new Fl_Button( x + 55, y + 5, preview_w - 60, 42 );
			info->box( FL_FLAT_BOX );
			info->down_box( FL_FLAT_BOX );
			info->clear_visible_focus( );
			info->align( FL_ALIGN_INSIDE | FL_ALIGN_LEFT | FL_ALIGN_TOP | FL_ALIGN_CLIP );
			info->labelsize( 12 );
			group2->resizable( info );
			group2->box( FL_EMBOSSED_BOX );
			group2->end( );

			preview = new AvPlayer( properties, slider, false, x, y + 49, preview_w, h - 64 - 49 );
			cutter = new NLE_Cutter( preview, x, y + h - 64, preview_w, 64 );
			group->resizable( preview );
			group->end( );

			// Now place the details on the right
			Fl_Window *win = new Fl_Double_Window( x + preview_w, y, panel_w, h );
			details = new NLE_ClipDetails( 0, 0, panel_w, h );
			win->end( );

			// Ensure we never shrink the preview too much
			//Fl_Box *box = new Fl_Box( x + 320, y, w - 320, h );
			//tile->resizable( box );

			// Clean up the tile
			//tile->end( );

			// Set the resizable
			resizable( this );
			end( );

			pthread_mutex_init( &mutex, NULL );
			preview->set_moved( static_moved_cb, this );
		}

		~NLE_ClipEditor( )
		{
			delete cut;
			delete tractor;
			delete image;
		}

		AvPlayer &player( )
		{
			return *preview;
		}

		void play( Tractor *tractor_ )
		{
			pthread_mutex_lock( &mutex );
			cutter->set_real( NULL, 0 );
			delete cut;
			cut = NULL;
			delete tractor;
			tractor = new Tractor( *tractor_ );
			tractor->set_speed( 0 );
			preview->locked( true );
			preview->play( *tractor );
			//preview->set_speed( 0 );
			preview->refresh( );
			cutter->locked( true );
			pthread_mutex_unlock( &mutex );
		}

		void play( Producer &producer )
		{
			pthread_mutex_lock( &mutex );

			delete tractor;
			delete cut;

			tractor = NULL;

			preview->locked( false );
			cutter->locked( false );
			cutter->fixate( NONE );
			cutter->set_real( NULL );

			cut = producer.parent( ).cut( 0, producer.get_out( ) );

			if ( cutter->locked( ) )
				cut->seek( producer.position( ) );
			else
				cut->seek( producer.position( ) + producer.get_in( ) );

			if ( cut != NULL )
			{
				cut->set_speed( 0 );
				preview->set_speed( 0 );
				cutter->set_values( cut->get_in( ), cut->get_in( ), cut->get_out( ), cut->get_length( ) );
				cutter->show( );
				cutter->set_position( producer.position( ), true );
				preview->play( *cut );
			}

			pthread_mutex_unlock( &mutex );
		}

		void populate_cutter( )
		{
			pthread_mutex_lock( &mutex );
			Producer *producer = NULL;
			int offset = 0, in = 0, out = 0, length = 0, flags = 0;

			if ( tractor )
				producer = NLE::Project( ).cut_info( offset, in, out, length, flags );
			else if ( cut )
				producer = new Producer( *cut );

			if ( !producer || !producer->is_valid( ) || ( tractor != NULL && tractor->get_length( ) == 0 ) )
			{
				cutter->set_real( NULL );
				cutter->fixate( NONE );
				cutter->hide( );
			}
			else if ( cutter->get_real( ) == NULL || cutter->get_real( )->get_producer( ) != producer->get_producer( ) )
			{
				if ( tractor )
				{
					if ( Fl::focus( ) == cutter )
						cutter->set_focus( POSITION );
					cutter->show( );
					cutter->set_real( producer, offset );
					cutter->fixate( flags );
					cutter->set_values( producer->get_in( ) + tractor->position( ) - offset, in, out, length );
					NLE::Highlight_In_Browser( producer );
				}
				else
				{
					if ( Fl::focus( ) == cutter )
						cutter->set_focus( POSITION );
					cutter->set_real( producer, 0 );
					cutter->fixate( NONE );
					cutter->set_values( producer->get_in( ), producer->get_in( ), producer->get_out( ), producer->get_length( ) );
					cutter->show( );
				}
			}

			delete producer;
			pthread_mutex_unlock( &mutex );
		}

		// This is a move from the preview
		// Will be triggered in one of two ways:
		// 1. the project is playing, then the request to change position will be false
		// 2. the project is paused, then the request to change position will be true
		// the latter case will occur when the previews slider is moved manually
		static void static_moved_cb( void *obj, int position, bool request )
		{
			NLE::Change_Position( position, player_position, request );
		}

		// This request is coming from the main project Change_Position to change to the
		// absolute position specified
		void set_position( int position, bool request )
		{
			cutter->set_position( position, request );
		}

		// The pass on the position to the panel
		void set_panel_position( int position )
		{
			if ( details != NULL )
				details->set_position( position );
		}

		void stop( )
		{
			preview->stop( );
			cutter->hide( );
		}

		void set_speed( double speed )
		{
			preview->set_speed( speed );
		}

		double get_speed( )
		{
			return preview->get_current( ) != NULL ? preview->get_current( )->get_double( "_speed" ) : 0;
		}

		void set_in( )
		{
			cutter->button_in_cb( );
		}

		void set_out( )
		{
			cutter->button_out_cb( );
		}

		void toggle_lock( )
		{
			cutter->toggle_lock( );
		}

		void set_focus( int flag )
		{
			cutter->set_focus( flag );
		}

		void update( )
		{
			populate_cutter( );
			Producer *current = NLE::Fetch_Selected( );
			thumb->image( NULL );
			delete image;
			image = NULL;
			if ( current != NULL && current->parent( ).get( "title" ) && !current->is_blank( ) )
			{
				int size;
				unsigned char *rgb = ( unsigned char * )current->parent( ).get_data( "thumb", size );
				image = new Fl_RGB_Image( rgb, 44, 36 );
				thumb->image( image );
				sprintf( text, "Title: %s\n"
						       "File: %s\n"
							   "Service: %s\n",
							   current->parent( ).get( "title" ),
							   current->parent( ).get( "resource" ),
							   current->parent( ).get( "mlt_service" ) );
				info->label( text );
				thumb->redraw( );
			}
			else
			{
				strcpy( text, "" );
				info->label( text );
			}
			thumb->redraw( );
			delete current;
		}

		void change_page( int page )
		{
			details->change_page( page );
		}

		void panel_update( )
		{
			if ( details )
				details->update( );
		}

		void refresh( )
		{
			preview->refresh( );
		}

		void seek( int position )
		{
			preview->seek( position );
			preview->refresh( );
		}

		bool playing( )
		{
			Producer *current = preview->get_current( );
			return current != NULL && current->get_speed( ) != 0;
		}

		void button_press( string button )
		{
			preview->button_press( button );
		}
};

class NLE_Properties : public Properties
{
	public:
		NLE_Properties::NLE_Properties( ) : Properties( )
		{
			set( "normalisation", "PAL" );
			set( "preview", "sdl_preview:360x288" );
			set( "preview.resize", 1 );
			set( "preview.progressive", 1 );
			set( "preview.rescale", "hyper" );
			set( "colour.foreground", ( char * )NULL );
			set( "colour.background", ( char * )NULL );
			set( "colour.background2", ( char * )NULL );
			set( "colour.hot", "0x6c0101" );
			set( "colour.warm", "0x4c0000" );
			set( "colour.luke", "0xffb900" );
			set( "colour.cold", "0x404080" );
			set( "font.application", ( char * )NULL );
			set( "font.data_entry", ( char * )NULL );
			set( "multitrack.insert_length", 124 );
			set( "publish", "localhost:5250" );

			// Now override with user specified config
			string value( getenv( "HOME" ) );
			value = value + "/.shotcut/shotcut-nle.conf";
			load( value.c_str( ) );

			// Set the application font
			if ( get( "font.application" ) != NULL )
				Fl::set_font( FL_HELVETICA, get( "font.application" ) );

			// Extract stuff from the properties
			Fl::visual( FL_DOUBLE | FL_RGB );

			if ( get( "colour.foreground" ) )
			{
				unsigned int c = get_int( "colour.foreground" );
				Fl::foreground( ( c >> 16 ) & 0xff, ( c >> 8 ) & 0xff, c & 0xff );
			}
			if ( get( "colour.background" ) )
			{
				unsigned int c = get_int( "colour.background" );
				Fl::background( ( c >> 16 ) & 0xff, ( c >> 8 ) & 0xff, c & 0xff );
			}
			if ( get( "colour.background2" ) )
			{
				unsigned int c = get_int( "colour.background2" );
				Fl::background2( ( c >> 16 ) & 0xff, ( c >> 8 ) & 0xff, c & 0xff );
			}

			if ( get( "colour.black" ) )
				Fl::set_color( FL_BLACK, get_int( "colour.black" ) << 8 );
			if ( get( "colour.white" ) )
				Fl::set_color( FL_WHITE, get_int( "colour.white" ) << 8 );
			if ( get( "colour.red" ) )
				Fl::set_color( FL_BLUE, ( unsigned int )get_int( "colour.red" ) << 8 );
			if ( get( "colour.blue" ) )
				Fl::set_color( FL_RED, ( unsigned int )get_int( "colour.blue" ) << 8 );

			Fl::set_color( NLE_HOT, ( unsigned int )get_int( "colour.hot" ) << 8 );
			Fl::set_color( NLE_WARM, ( unsigned int )get_int( "colour.warm" ) << 8 );
			Fl::set_color( NLE_LUKE, ( unsigned int )get_int( "colour.luke" ) << 8 );
			Fl::set_color( NLE_COLD, ( unsigned int )get_int( "colour.cold" ) << 8 );
		}
};

class NLE_Window : public AvWindow
{
	private:
		// The global config
		NLE_Properties properties;
		// The current mode
		nle_mode mode_;
		// The producer associated to the current mode
		Producer *selected_producer_;
		// The browser
		NLE_Browser *browser;
		// The browser type buttons
		Fl_Light_Button *browser_show[ 4 ];
		// The browser clean button
		Fl_Button *clean;
		// Button for switching browser tracking on and off
		Fl_Button *tracking;
		// The clip editor
		NLE_ClipEditor *editor;
		// The multitrack
		AvMultitrack *multitrack;
		// Buttons that control the whole system
		Fl_Button *buttons[ 8 ];
		// The event associated to the current project
		Event *event;
		// The current project
		NLE_Project *project_;
		// The item being dropped
		Producer *drop_object;
		// The undo stack and redo queue
		pthread_mutex_t undo_lock;
		bool ignore_changes_;
		Deque undo_;
		Deque redo_;
		Consumer serialiser;
		Consumer auto_save;
		// Default data entry font
		Fl_Font data_entry_font;

	public:
		NLE_Window( int port, int x, int y, int w, int h ) :
			AvWindow( x, y, w, h ),
			properties( ),
			mode_( disable_mode ),
			selected_producer_( NULL ),
			event( NULL ),
			project_( NULL ),
			drop_object( NULL ),
			ignore_changes_( false ),
			serialiser( "westley", "memory" ),
			auto_save( "westley" ),
			data_entry_font( FL_HELVETICA )
		{
			// Size constants
			const int bottom_w = w;
			const int bottom_h = 18;
			const int bottom_x = 0;
			const int bottom_y = h - bottom_h;

			const int multitrack_w = w;
			const int multitrack_h = 240 - bottom_h;
			const int multitrack_x = 0;
			const int multitrack_y = h - 240;

			const int browser_w = 200;
			const int browser_h = h - multitrack_h - bottom_h;
			const int browser_x = 0;
			const int browser_y = 0;

			const int editor_w = w - browser_w;
			const int editor_h = browser_h;
			const int editor_x = browser_w;
			const int editor_y = 0;

			// Bottom buttons
			const int button_count = 8;
			buttons[ 0 ] = new Fl_Button( bottom_x, bottom_y, bottom_w / button_count, bottom_h, "-" );
			buttons[ 1 ] = new Fl_Button( bottom_x + bottom_w / button_count, bottom_y, bottom_w / button_count, bottom_h, "+" );
			buttons[ 2 ] = new Fl_Light_Button( bottom_x + 2 * bottom_w / button_count, bottom_y, bottom_w / button_count, bottom_h, "Lock Voice Over" );
			buttons[ 2 ]->when( FL_WHEN_CHANGED );
			buttons[ 3 ] = new Fl_Light_Button( bottom_x + 3 * bottom_w / button_count, bottom_y, bottom_w / button_count, bottom_h, "Preview" );
			buttons[ 3 ]->when( FL_WHEN_CHANGED );
			buttons[ 4 ] = new Fl_Button( bottom_x + 4 * bottom_w / button_count, bottom_y, bottom_w / button_count, bottom_h, "New Project" );
			buttons[ 5 ] = new Fl_Button( bottom_x + 5 * bottom_w / button_count, bottom_y, bottom_w / button_count, bottom_h, "Save" );
			buttons[ 6 ] = new Fl_Button( bottom_x + 6 * bottom_w / button_count, bottom_y, bottom_w / button_count, bottom_h, "Save as..." );
			buttons[ 7 ] = new Fl_Button( bottom_x + 7 * bottom_w / button_count, bottom_y, bottom_w / button_count, bottom_h, "Publish" );
			for ( int i = 0; i < button_count; i ++ )
			{
				buttons[ i ]->shortcut( FL_ALT | '1' + i );
				buttons[ i ]->clear_visible_focus( );
				buttons[ i ]->labelsize( 10 );
				buttons[ i ]->callback( static_buttons_cb, this );
			}
			buttons[ 2 ]->type( FL_TOGGLE_BUTTON );
			buttons[ 2 ]->value( 0 );
			buttons[ 3 ]->type( FL_TOGGLE_BUTTON );
			buttons[ 3 ]->value( 0 );

			// Browser group
			Fl_Group *group = new Fl_Group( browser_x, browser_y, browser_w, browser_h );
			browser_show[ 0 ] = new Fl_Light_Button( browser_x, browser_y, browser_w / 4, 12, "Projects" );
			browser_show[ 1 ] = new Fl_Light_Button( browser_w / 4, browser_y, browser_w / 4, 12, "Stories" );
			browser_show[ 2 ] = new Fl_Light_Button( browser_x + 2 * browser_w / 4, browser_y, browser_w / 4, 12, "Audio" );
			browser_show[ 3 ] = new Fl_Light_Button( browser_x + 3 * browser_w / 4, browser_y, browser_w / 4, 12, "Stills" );
			for ( int i = 0; i < 4; i ++ )
			{
				browser_show[ i ]->labelsize( 10 );
				browser_show[ i ]->clear_visible_focus( );
				browser_show[ i ]->value( 1 );
				browser_show[ i ]->box( FL_NO_BOX );
				browser_show[ i ]->callback( static_browser_show_cb, this );
			}
			browser = new NLE_Browser( browser_x, browser_y + 12, browser_w, browser_h - 40 );
			tracking = new Fl_Button( browser_x, browser_y + browser_h - 28, browser_w, 14, "Tracking" );
			tracking->clear_visible_focus( );
			tracking->labelsize( 10 );
			tracking->type( FL_TOGGLE_BUTTON );
			tracking->value( false );
			clean = new Fl_Button( browser_x, browser_y + browser_h - 14, browser_w, 14, "Clean" );
			clean->clear_visible_focus( );
			clean->labelsize( 10 );
			clean->callback( static_clean_cb, this );
			group->resizable( browser );
			group->end( );

			// Construct the main editor and multitrack components
			multitrack = new AvMultitrack( multitrack_x, multitrack_y, multitrack_w, multitrack_h );
			editor = new NLE_ClipEditor( properties, multitrack->slider( ), editor_x, editor_y, editor_w, editor_h );

			remove( multitrack );
			remove( editor );
			insert( *editor, 0 );
			insert( *multitrack, 1 );
			resizable( editor );

			end( );

			callback( static_close_cb, this );

			// Assign the autosave file name
			char temp[ 512 ];
			sprintf( temp, "%s/.shotcut/autosave.%d.westley", getenv( "HOME" ), port );
			auto_save.set( "resource", temp );

			pthread_mutex_init( &undo_lock, NULL );
		}

		~NLE_Window( )
		{
			ignore_changes( false );
			check_point( );
			multitrack->project( NULL );
			delete drop_object;
			delete selected_producer_;
			pthread_mutex_destroy( &undo_lock );
			while ( undo_.count( ) )
				free( undo_.pop_front( ) );
			while ( redo_.count( ) )
				free( redo_.pop_front( ) );
			delete event;
			delete project_;
		}

		static void static_close_cb( Fl_Widget *widget, void *arg )
		{
			( ( NLE_Window * ) arg )->editor->player( ).stop( );
			exit( 0 );
		}

		Properties &get_config( )
		{
			return properties;
		}

		// This is the first method called after the constructor
		void show( int argc, char **argv )
		{
			AvWindow::show( argc, argv );

			// Configure westley consumers to store the nle_ items
			auto_save.set( "store", "nle_" );
			serialiser.set( "store", "nle_" );

			// Reload the project if it existed previously
			NLE_Project *project = new NLE_Project( "westley", auto_save.get( "resource" ) );

			// If it's invalid, then create a new project
			if ( project == NULL || !project->is_valid( ) )
			{
				delete project;
				project = NLE::New_Project( false );
			}

			switch_project( project );

			check_point( );
			change_mode( project_mode );
			NLE::Change_Position( project_->position( ), multitrack_position, true );
		}

		void switch_project( NLE_Project *project )
		{
			// Change to disable mode while the switch is happening
			change_mode( disable_mode, true );

			if ( project->get( "nle_font_capture" ) )
				Fl::set_font( FL_TIMES, project->get( "nle_font_capture" ) );
			else
				Fl::set_font( FL_TIMES, data_entry_font );

			delete drop_object;
			drop_object = NULL;
			auto_save.connect( *project );
			auto_save.start( );
			project->set( "_westley", ( char * )NULL );
			project->set( "_mlt_service_hidden", ( char * )NULL );
			multitrack->project( NULL );
			delete event;
			delete project_;
			project_ = project;
			project_->set_speed( 0 );
			auto_save.connect( *project_ );
			buttons[ 2 ]->value( project_->get_int( "nle_lock_tracks" ) );
			buttons[ 3 ]->value( project_->get_int( "nle_transitions" ) );
			project_->seek( project_->get_int( "nle_position" ) );
			browser->refresh( project_ );
			serialiser.connect( *project_ );
			multitrack->project( project_ );
			event = project_->listen( "producer-changed", ( void * )this, ( mlt_listener )static_listener_change );
		}

		static void static_buttons_cb( Fl_Widget *widget, void *obj )
		{
			( ( NLE_Window * )obj )->buttons_cb( ( Fl_Button * )widget );
		}

		void buttons_cb( Fl_Button *button )
		{
			int i = 0;
			for ( i = 0; i < 7; i ++ )
				if ( button == buttons[ i ] )
					break;

			switch( i )
			{
				case 0:
					multitrack->zoom( multitrack->zoom( ) - 1.0 );
					break;
				case 1:
					multitrack->zoom( multitrack->zoom( ) + 1.0 );
					break;
				case 2:
					multitrack->lock_tracks( !button->value( ) );
					break;
				case 3:
					change_mode( disable_mode, true );
					multitrack->transitions( button->value( ) );
					change_mode( project_mode, true );
					break;
				case 4:
					{
						NLE_Project *project = NLE::New_Project( );
						if ( project != NULL )
						{
							change_mode( disable_mode, true );
							switch_project( project );
							change_mode( project_mode, true );
							NLE::Change_Position( project_->position( ), multitrack_position, true );
						}
						NLE::Refresh_Preview( );
					}
					break;
				case 5:
				case 6:
					{
						// Ask for a file name and allow a change if user selected 'Save As'
						char *file = save_file_name( i == 6 );

						// If NULL is returned, the user cancelled the save
						if ( file != NULL )
						{
							// Make sure we don't accidentally trigger an undo save
							change_mode( disable_mode, true );

							// Always save in preview mode
							multitrack->transitions( true );

							// Save the file
							Consumer westley( "westley", file );
							westley.connect( *project_ );
							westley.set( "store", "nle_" );
							westley.start( );

							// Return to project state
							multitrack->transitions( buttons[ 3 ]->value( ) );
							change_mode( project_mode, true );

							free( file );
						}
					}
					break;
				case 7:
					{
						change_mode( disable_mode, true );
						multitrack->transitions( true );
						Consumer valerie( "valerie", properties.get( "publish" ) );
						valerie.connect( *project_ );
						valerie.start( );
						multitrack->transitions( buttons[ 3 ]->value( ) );
						change_mode( project_mode, true );
					}
					break;
			}
		}

		char *save_file_name( bool change )
		{
			char *file = project_->get( "nle_file_name" );
			if ( change || file == NULL )
			{
				char directory[ 512 ];
				char *prefix = project_->get( "nle_prefix" );
				char *template_name = project_->get( "nle_template" );
				char *title = project_->get( "title" );

				strcpy( directory, getenv( "HOME" ) );
				strcat( directory, "/shotcut" );
				if ( !fl_filename_isdir( directory ) )
					mkdir( directory, 0700 );
				strcat( directory, "/" );
				if ( prefix == NULL )
				{
					strcat( directory, template_name ? template_name : "Unknown" );
					if ( !fl_filename_isdir( directory ) )
						mkdir( directory, 0700 );
					strcat( directory, "/" );
				}
				else
				{
					strcat( directory, prefix );
				}
				strcat( directory, title ? title : "" );
				if ( title && !strstr( title, ".westley" ) )
					strcat( directory, ".westley" );

				file = fl_file_chooser( "Save as", "*.westley", directory );

				// Save the file name for reuse
				if ( file != NULL )
				{
					char *converted = AvUtils::iconv_convert( file, "iso8859-1", "UTF-8" );
					project_->set( "nle_file_name", converted );
					free( converted );
					file = strdup( file );
				}
			}
			else
			{
				file = AvUtils::iconv_convert( file, "UTF-8", "iso8859-1" );
			}

			return file;
		}

		void change_mode( nle_mode mode, bool force = true )
		{
			// Special case - trap all attempts to switch to project_mode while 
			// transitions are turned on
			if ( project_ != NULL )
				mode = project_->transitions( ) && mode == project_mode ? preview_mode : mode;

			// Change modes if different or forced
			if ( mode_ != mode || force )
			{
				switch( mode )
				{
					case disable_mode:
						mode_ = mode;
						delete drop_object;
						drop_object = NULL;
						delete selected_producer_;
						selected_producer_ = NULL;
						editor->set_speed( 0 );
						break;

					case preview_mode:
					case project_mode:
						mode_ = mode;
						editor->play( project_ );
						project_->set_speed( 0 );
						refresh_multitrack( true );
						break;

					case browser_mode:
						if ( browser->selection( ) != NULL )
						{
							mode_ = mode;
							NLE_Producer *selected = ( NLE_Producer * )browser->selection( );
							delete selected_producer_;
							selected_producer_ = selected->producer->cut( 0, selected->producer->get_out( ) );
							editor->play( *selected_producer_ );
							refresh_multitrack( true );
						}
						break;
				}
			}
			editor->refresh( );
		}

		nle_mode current_mode( )
		{
			return mode_;
		}

		NLE_Project &project( )
		{
			if ( project_ != NULL )
				return *project_;
			else
				throw "No project available";
		}

		// Return the currently selected item 
		Producer *fetch_selected( )
		{
			pthread_mutex_lock( &undo_lock );
			Producer *selected = NULL;
			try
			{
				if ( mode_ == browser_mode )
					selected = new Producer( selected_producer_ );
				else if ( mode_ == project_mode || mode_ == preview_mode )
					selected = project( ).get_cut( );
			}
			catch( const char *e )
			{
				selected = NULL;
			}
			pthread_mutex_unlock( &undo_lock );
			return selected;
		}

		void ignore_changes( bool value )
		{
			ignore_changes_ = value;
		}

		bool ignore_changes( )
		{
			return ignore_changes_;
		}

		void check_point( )
		{
			pthread_mutex_lock( &undo_lock );
			if ( !ignore_changes_ )
			{
				char *last = ( char * )undo_.peek_back( );
				project_->set( "nle_fixed", multitrack->fixed( ) );
				project_->set( "nle_position", project_->position( ) );
				serialiser.start( );
				char *doc = serialiser.get( "memory" );
				if ( last == NULL || ( doc != NULL && strcmp( last, doc ) ) )
				{
					auto_save.start( );
					undo_.push_back( strdup( doc ) );
					while ( redo_.count( ) )
						free( redo_.pop_front( ) );
				}
			}
			pthread_mutex_unlock( &undo_lock );
		}

		void undo( )
		{
			if ( redo_.count( ) == 0 )
			{
				ignore_changes_ = false;
				check_point( );
			}
			pthread_mutex_lock( &undo_lock );
			if ( undo_.count( ) > 0 )
			{
				char *doc = ( char * )undo_.pop_back( );
				redo_.push_front( doc );
				doc = ( char * )undo_.peek_back( );
				NLE_Project *project = new NLE_Project( "westley-xml", doc );
				if ( project->is_valid( ) )
					switch_project( project );
				else
					delete project;
			}
			pthread_mutex_unlock( &undo_lock );
			change_mode( project_mode, true );
			NLE::Change_Position( project_->get_int( "nle_position" ), multitrack_position, true );
		}

		void redo( )
		{
			pthread_mutex_lock( &undo_lock );
			if ( redo_.count( ) > 0 )
			{
				char *doc = ( char * )redo_.pop_front( );
				undo_.push_back( doc );
				NLE_Project *project = new NLE_Project( "westley-xml", doc );
				if ( project->is_valid( ) )
					switch_project( project );
				else
					delete project;
			}
			pthread_mutex_unlock( &undo_lock );
			change_mode( project_mode, true );
			NLE::Change_Position( project_->get_int( "nle_position" ), multitrack_position, true );
		}

		static void static_clean_cb( Fl_Widget *widget, void *obj )
		{
			NLE::Change_Mode( disable_mode );
			( ( NLE_Window * )obj )->browser->clean( );
			NLE::Change_Mode( project_mode );
		}

		static void static_browser_show_cb( Fl_Widget *widget, void *obj )
		{
			( ( NLE_Window * )obj )->browser_show_cb( widget );
		}

		void browser_show_cb( Fl_Widget *widget )
		{
			int flags = 0;

			if ( Fl::event_clicks( ) == 1 )
			{
				browser_show[ 0 ]->value( widget == browser_show[ 0 ] );
				browser_show[ 1 ]->value( widget == browser_show[ 1 ] );
				browser_show[ 2 ]->value( widget == browser_show[ 2 ] );
				browser_show[ 3 ]->value( widget == browser_show[ 3 ] );
			}

			if ( browser_show[ 0 ]->value( ) ) flags |= PROJECTS;
			if ( browser_show[ 1 ]->value( ) ) flags |= STORIES;
			if ( browser_show[ 2 ]->value( ) ) flags |= AUDIO;
			if ( browser_show[ 3 ]->value( ) ) flags |= STILLS;
			browser->display( flags );
		}

		int handle_keys( int key )
		{
			static char *jkl = getenv( "SHOTCUT_JKL" );
			int ret = 1;
			int state = Fl::event_state( );
			if ( state & FL_NUM_LOCK ) state ^= FL_NUM_LOCK;
			if ( state & FL_CAPS_LOCK ) state ^= FL_CAPS_LOCK;
			if ( jkl != NULL && key == 'j' && state == 0 )
				set_speed( -1 );
			else if ( jkl != NULL && key == 'k' && state == 0 )
				set_speed( 0 );
			else if ( jkl != NULL && key == 'l' && state == 0 )
				set_speed( 1 );
			else if ( key == ' ' && state == 0 )
				set_speed( get_speed( ) != 1.0 ? 1.0 : 0 );
			else if ( key == '+' || key == '=' && state == 0 )
				multitrack->zoom( multitrack->zoom( ) + 1.0 );
			else if ( key == '-' && state == 0 )
				multitrack->zoom( multitrack->zoom( ) - 1.0 );
			else if ( key == 'u' && Fl::event_state( FL_CTRL ) )
				editor->toggle_lock( );
			else if ( key == 'i' && Fl::event_state( FL_CTRL ) )
				editor->set_focus( IN );
			else if ( key == 'o' && Fl::event_state( FL_CTRL ) )
				editor->set_focus( OUT );
			else if ( key == 'p' && Fl::event_state( FL_CTRL ) )
				editor->set_focus( POSITION );
			else if ( key == 'i' && state == 0 )
				editor->set_in( );
			else if ( key == 'o' && state == 0 )
				editor->set_out( );
			else if ( key == 'z' && Fl::event_state( FL_CTRL ) )
				undo( );
			else if ( key == 'r' && Fl::event_state( FL_CTRL ) )
				redo( );
			else if ( key == '<' || key == ',' )
				editor->button_press( "start" );
			else if ( key == '>' || key == '.' )
				editor->button_press( "end" );
			else if ( ( key >= '1' && key <= '9' ) && Fl::event_state( FL_CTRL ) )
				editor->change_page( key - '1' );
			else
				ret = 0;
			return ret;
		}

		int handle( int e )
		{
			int ret = 0;

			switch( e )
			{
				case FL_KEYDOWN:
					ret = handle_keys( Fl::event_key( ) );
					break;
			}

			return ret == 0 ? AvWindow::handle( e ) : ret;
		}

		void set_speed( double speed )
		{
			editor->set_speed( speed );
		}

		double get_speed( )
		{
			return editor->get_speed( );
		}

		void forward_browser( char *title, char *doc )
		{
			browser->add( title, doc );
		}

		void highlight_in_browser( Producer *producer )
		{
			if ( mode_ != browser_mode && tracking->value( ) )
				browser->set_selection( producer );
		}

		void generate_thumb( Producer *producer, int position = 0 )
		{
			if ( producer != NULL )
			{
				// Special case - check for an existing filter associated to a playlist
				Filter *test = producer->filter( 0 ) ;
				bool not_attached = test == NULL;
				delete test;

				Filter convert( "avcolour_space" );
				convert.set( "forced", mlt_image_yuv422 );
				producer->attach( convert );
				producer->seek( position );
				Frame *frame = producer->get_frame( );
				if ( frame != NULL )
				{
					// Determine type
					if ( producer->get( "nle_template" ) != NULL )
					{
						producer->set( "nle_type", PROJECTS | STORIES );
						Tractor tractor( producer->parent( ) );
						browser->refresh( &tractor );
					}
					else if ( producer->type( ) == playlist_type && not_attached )
					{
						producer->set( "nle_type", STORIES );
						Playlist playlist( producer->parent( ) );
						browser->refresh( &playlist );
						producer->set( "_no_show", 1 );
					}
					else if ( frame->get_int( "test_image" ) && frame->get_int( "test_audio" ) )
						producer->set( "nle_type", INVALID );
					else if ( frame->get_int( "test_image" ) )
						producer->set( "nle_type", AUDIO );
					else if ( frame->get_int( "test_audio" ) )
						producer->set( "nle_type", STILLS );
					else
						producer->set( "nle_type", STORIES );

					frame->set( "rescale", "nearest" );
					unsigned char *thumb = frame->fetch_image( mlt_image_rgb24, 44, 36, 1 );
					producer->set( "thumb", thumb, 44 * 36 * 3, mlt_pool_release );
					frame->set( "image", thumb, 0, NULL, NULL );
					delete frame;
				}
				producer->detach( convert );
				producer->seek( 0 );
			}
		}

		void load_producer( NLE_Producer *producer )
		{
			if ( producer->producer == NULL )
			{
				producer->producer = new Producer( producer->document );
			}
			if ( producer->producer )
			{
				int len = 0;
				producer->producer->set( "title", producer->title );
				if ( producer->producer->get_data( "thumb", len ) == NULL )
					generate_thumb( producer->producer );
			}
		}

		void set_preview_speed( double speed )
		{
			editor->set_speed( speed );
		}

		void forward_drop_object( Producer *producer, const char *info )
		{
			delete drop_object;
			if ( !buttons[ 3 ]->value( ) && producer )
			{
				drop_object = new Producer( *producer );
				Fl::selection( *this, info, strlen( info ) );
				Fl::dnd( );
				cursor( FL_CURSOR_DEFAULT );
			}
			else
			{
				cursor( FL_CURSOR_DEFAULT );
				drop_object = NULL;
			}
		}

		Producer *fetch_drop_object( )
		{
			return drop_object;
		}

		void refresh_multitrack( bool force = false )
		{
			if ( multitrack->refresh( force ) )
			{
				editor->update( );
				editor->panel_update( );
			}
		}

		void refresh_preview( )
		{
			editor->refresh( );
		}

		void resize_cut( Producer *producer, int &in, int &out, int absolute )
		{
			if ( mode_ == project_mode )
			{
				int track = 0;
				int cut = 0;
				if ( project( ).locate_cut( track, cut, producer ) )
					project( ).resize_cut( track, cut, in, out, absolute );
				multitrack->refresh( true );
			}
			else
			{
				Producer *selected = fetch_selected( );
				if ( selected != NULL && selected->is_valid( ) )
					selected->set_in_and_out( in, out );
				delete selected;
			}
		}

		void insert_into_project( Producer *producer )
		{
			multitrack->insert_at( project_->position( ) < 0 ? 0 : project_->position( ), producer );
		}

		// This is the centralised seeking method - all requests for seeking or notification of
		// changes in position come through here
		void change_position( int position, PositionChanger changer, bool request )
		{
			// We're mostly interested in project and preview modes
			if ( mode_ == project_mode || mode_ == preview_mode )
			{
				// Handle the request for a position change
				if ( request )
				{
					// Determine if there's going to be a change 
					bool changed = multitrack->refresh( position );

					// We always need to seek in the case of a request
					project_->seek( position );

					// Requests come from:
					// * the cutter (any change to in, out or position)
					// * the user moving the multitrack slider (player_position)
					// * the user clicking on the multitrack or changing the multitrack
	
					// Optimisation - if playing and the new position requires a change
					// in the editor, then update
					if ( editor->playing( ) && changer == cutter_position )
					{
						if ( multitrack->refresh( changed ) )
							editor->panel_update( );
						return;
					}
					else if ( editor->playing( ) && changer == panel_position )
					{
						if ( multitrack->refresh( changed ) )
							editor->update( );
						return;
					}
					else if ( editor->playing( ) )
					{
						if ( multitrack->refresh( changed ) )
						{
							editor->update( );
							editor->panel_update( );
						}
						return;
					}

					switch( changer )
					{
						case cutter_position:
							// Refresh the multitrack but shouldn't ever change the clip in the cutter
							if ( multitrack->refresh( changed ) )
								editor->panel_update( );
							// Set the position on the multitrack 
							multitrack->set_position( position, false );
							// Update the editor with minimal info (timecode)
							editor->set_position( position, false );
							editor->set_panel_position( position );
							break;

						case panel_position:
							// Refresh the multitrack but shouldn't ever change the clip in the panel
							if ( multitrack->refresh( changed ) )
								editor->update( );
							// Set the position on the multitrack 
							multitrack->set_position( position, false );
							// Set the position on the cutter too
							editor->set_position( position, true );
							break;

						case player_position:
						case multitrack_position:
							// Refresh the multitrack and update editor if necessary
							if ( multitrack->refresh( changed ) )
							{
								editor->update( );
								editor->panel_update( );
							}
							// Set the position on the multitrack (centred)
							multitrack->set_position( position, false );
							// Set the position in the editor (forced)
							editor->set_position( position, true );
							editor->set_panel_position( position );
							break;
					}

					// Refresh the preview, just in case...
					editor->refresh( );
				}

				// Handle a notification
				else
				{
					// These are only generated during playback

					// Handle the multitrack refresh first and notify editor if cut has changed
					if ( multitrack->refresh( position ) )
					{
						multitrack->set_position( position, true );
						multitrack->refresh( );
						editor->update( );
						editor->panel_update( );
					}
					else
					{
						// Set the position on the multitracks slider (centred)
						multitrack->set_position( position, true );
					}
					// Notify the editor of the change
					editor->set_position( position, true );
					editor->set_panel_position( position );
				}
			}
			else if ( mode_ == browser_mode )
			{
				if ( request )
				{
					editor->seek( position );

					switch ( changer )
					{
						case cutter_position:
							editor->set_position( position, false );
							break;
						case multitrack_position:
							// Set the position on the multitrack - affects nothing else
							multitrack->set_position( position, true );
							break;
						case panel_position:
						case player_position:
							fprintf( stderr, "player move in browser mode == WRONG\n" );
							break;
					}
					// Refresh the preview, just in case...
					editor->refresh( );
				}
				else
				{
					switch ( changer )
					{
						case cutter_position:
							editor->set_position( position, true );
							break;
						case panel_position:
						case multitrack_position:
							fprintf( stderr, "multitrack move in browser mode == WRONG\n" );
							break;
						case player_position:
							editor->set_position( position, true );
							break;
					}
				}
			}
			else
			{
				fprintf( stderr, "Position change in invalid mode...\n" );
			}
		}

	protected:
		static void static_listener_change( Properties *project_, NLE_Window *object )
		{
			object->listener_change( );
		}

		void listener_change( )
		{
			project_->block( );
			NLE::Check_Point( );
			project_->optimise( );
			project_->unblock( false );
		}
};

NLE_Window *nle = NULL;

Properties &NLE::Get_Config( )
{
	return nle->get_config( );
}

void NLE::Change_Mode( nle_mode mode, bool force )
{
	nle->change_mode( mode, force );
}

nle_mode NLE::Current_Mode( )
{
	return nle->current_mode( );
}

void NLE::Load_Producer( NLE_Producer *producer )
{
	nle->load_producer( producer );
}

void NLE::Forward_Browser( char *title, char *doc )
{
	nle->forward_browser( title, doc );
}

void NLE::Set_Preview_Speed( double speed )
{
	nle->set_preview_speed( speed );
}

void NLE::Forward_Drop_Object( Producer *producer, const char *info )
{
	nle->forward_drop_object( producer, info );
}

Producer *NLE::Fetch_Drop_Object( )
{
	return nle->fetch_drop_object( );
}

Producer *NLE::Fetch_Selected( )
{
	return nle->fetch_selected( );
}

void NLE::Refresh_Multitrack( bool forced )
{
	nle->refresh_multitrack( forced );
}

void NLE::Refresh_Preview( )
{
	nle->refresh_preview( );
}

void NLE::Highlight_In_Browser( Producer *producer )
{
	nle->highlight_in_browser( producer );
}

void NLE::Resize_Cut( Producer *producer, int &in, int &out, int absolute )
{
	nle->resize_cut( producer, in, out, absolute );
}

void NLE::Insert_Into_Project( Producer *producer )
{
	nle->insert_into_project( producer );
}

void NLE::Change_Position( int position, PositionChanger changer, bool request )
{
	nle->change_position( position, changer, request );
}

void NLE::Check_Point( )
{
	nle->check_point( );
}

void NLE::Ignore_Changes( bool value )
{
	nle->ignore_changes( value );
}

bool NLE::Ignore_Changes( )
{
	return nle->ignore_changes( );
}

NLE_Project &NLE::Project( )
{
	return nle->project( );
}

static void close_nle( )
{
	delete nle;
}

void get_display_size( int &width, int &height )
{
	Display *display = fl_display;
	if ( display != NULL )
	{
		width = DisplayWidth( display, DefaultScreen( display ) );
		height = DisplayHeight( display, DefaultScreen( display ) );
	}
}

int main( int argc, char **argv )
{
	int valid = 1;
	char *name = "NLE";
	int port = 5260;
	int log_level = 0;
	int normal_window = 0;

	for ( int i = 1; i < argc && valid; i ++ )
	{
		if ( !strncmp( argv[ i ], "--name=", 7 ) )
			name = argv[ i ] + 7;
		else if ( !strncmp( argv[ i ], "--port=", 7 ) )
			port = atoi( argv[ i ] + 7 );
		else if ( !strncmp( argv[ i ], "--log-level=", 12 ) )
			log_level = atoi( argv[ i ] + 12 );
		else if ( !strcmp( argv[ i ], "--normal-window" ) )
			normal_window = 1;
		else
			valid = 0;
	}

	if ( valid )
	{
		// Initialise the MLT core
		setenv( "MLT_WESTLEY_DEEP", "true", 1 );
		setenv( "MLT_TEST_CARD", SHOTCUT_SHARE "pixmaps/logo.png", 0 );
		Factory::init( );

		// Start the server
		Miracle::log_level( log_level );
		NLE_Server server( name, port );

		// Fake 'splash' window
		Fl_Window win( 50, 50 );
		win.show( );
		win.hide( );

		// We can now obtain the width and height if required
		int width = 1024;
		int height = 700;
		if ( !normal_window )
			get_display_size( width, height );

		// Create the window 
		nle = new NLE_Window( port, 0, 50, width, height );

		// Register a clean up (this ensures that all mlt objects are destroyed)
		atexit( close_nle );

		// Set up the fltk locking mechanism
		Fl::lock( );

		// Display the window, either maximised or as a normal window
		if ( !normal_window )
			nle->clear_border( );
		nle->show( 1, argv );
		if ( !normal_window )
			nle->maximize( true );

		// Run fltk
		return Fl::run( );
	}
	else
	{
		fprintf( stderr, "Usage: shotcut-nle [ --name=Name ] [ --port=port ]\n" );
		return 0;
	}
}

