/*
 * AvPlayer.h -- FLTK media playing window
 * Copyright (C) 2003-2004 Ushodaya Enterprises Limited
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _FLTK_AVPLAYER_H_
#define _FLTK_AVPLAYER_H_

#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Value_Slider.H>
#include <FL/x.H>

#include <string>
using namespace std;

#include "AvWindow.h"

#include <mlt++/Mlt.h>
using namespace Mlt;

namespace shotcut
{
	/** An FLTK media player.
	*/

	class AvPlayer : public AvWindow
	{
		private:
			Fl_Window *window;
			Fl_Button *buttons[ 8 ];
			Fl_Image *images[ 8 ];
			Fl_Slider *slider;
			Fl_Group *group;
			double speed;
			Event *event;
			Consumer *consumer;
			Producer *current;
			int position;
			void ( *_move )( void *, int, bool );
			void *_move_obj;
			bool _locked;
			pthread_mutex_t mutex;
			bool sticky_;
			int step;
			int previous_speed;

		public:
			AvPlayer( Properties &properties, Fl_Slider *, bool sticky, int x, int y, int w, int h );
			virtual ~AvPlayer( );
			void initialise( Properties &properties, Fl_Slider *slider = NULL, bool sticky = false );
			void locked( bool value );
			bool locked( );
			void set_speed( double speed );
			void play( Producer &producer );
			void play( char *file );
			bool restart( );
			void seek( int position );
			void seek_relative( int offset );
			int handle( int e );
			void position_change( int position );
			void set_moved( void ( *move )( void *, int, bool ), void *obj );
			void paste( const char *text );
			Producer *get_current( );
			void refresh( );
			void stop( );
			void button_press( string id );

		private:
			void frame_shown( Frame &frame );
			void indicate_speed( double speed );
			static void button_callback( Fl_Widget *widget, void *object );
			static void slider_callback( Fl_Widget *widget, void *object );
			void button_press( Fl_Button *button );
			Window xid( );
			static void consumer_frame_show( mlt_consumer sdl, AvPlayer *self, mlt_frame frame_ptr );
			void handle_start_and_end( string id );
			static void static_button_timeout( void *obj );
			void button_timeout( );
	};
}

#endif
