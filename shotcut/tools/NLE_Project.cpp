/*
 * NLE_Project.cpp -- NLE Project class
 * Copyright (C) 2004-2005 Ushodaya Enterprises Limited
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <string.h>

#include <FL/Fl.H>
#include <FL/fl_ask.H>

#include "AvCutter.h"
#include "NLE.h"
using namespace shotcut;

#include <mlt++/Mlt.h>
using namespace Mlt;

// Constructor from a string - can be a westley file or a previously 
// serialised westley document)
NLE_Project::NLE_Project( char *id, char *resource ) :
	Tractor( id, resource ),
	tractor( this )
{
	// Initialise the project
	initialise( );
}

// Constructor for the NLE class
NLE_Project::NLE_Project( Tractor *tractor_ ) :
	Tractor( *tractor_ ),
	tractor( this )
{
	// Initialise the project
	initialise( );
}

// Constructor for the NLE class
NLE_Project::NLE_Project( Tractor &tractor_ ) :
	Tractor( tractor_ ),
	tractor( this )
{
	// Initialise the project
	initialise( );
}

// Destructor for the NLE class
NLE_Project::~NLE_Project( )
{
	if ( tracks_ )
	{
		for ( int i = 0; i < tractor->count( ); i ++ )
			delete tracks_[ i ];
		delete [] tracks_;
	}
}

void NLE_Project::initialise( )
{
	// Seek on constrcution
	if ( is_valid( ) )
		seek( get_int( "nle_position" ) );

	// Create references to each track (currently, there's no support for track
	// addition or removal)
	valid_ = false;
	tracks_ = NULL;
	if ( tractor != NULL && tractor->is_valid( ) )
	{
		valid_ = true;
		tracks_ = new Playlist *[ tractor->count( ) ];
		for ( int i = 0; i < tractor->count( ); i ++ )
		{
			Producer *producer = tractor->track( i );
			if ( producer->type( ) == playlist_type )
				tracks_[ i ] = new Playlist( *producer );
			else 
				valid_ = false;
			delete producer;
		}

		// This indirectly and safely prepares blank cuts for use without an event
		if ( valid_ )
		{
			block( );
			unblock( false );
		}
	}
}

// Returns true if this is a valid nle project
bool NLE_Project::valid_project( )
{
	return valid_;
}

// Block events to allow multiple updates
void NLE_Project::block( )
{
	tractor->lock( );
	tractor->block( );
	tractor->set( "nle_position", tractor->position( ) );
}

// Unblock events and optionally fire a changed event
void NLE_Project::unblock( bool changed )
{
	refresh_text( );
	prepare_cuts( );
	tractor->unblock( );
	tractor->unlock( );
	if ( changed )
		tractor->fire_event( "producer-changed" );
}

// Get current track index
int NLE_Project::current_track( )
{
	return tractor->get_int( "_current_track" );
}

// Set the current track
void NLE_Project::current_track( int index )
{
	block( );
	if ( index >= 0 && index < tracks( ) )
		tractor->set( "_current_track", index );
	unblock( );
}

// Get the super track
int NLE_Project::super_track( )
{
	// Search for a track of type text
	for ( int i = 1; i < tracks( ); i ++ )
		if ( type( i ) == TEXT )
			return i;
	return -1;
}

// Get the current position in the project
int NLE_Project::position( )
{
	return tractor->position( );
}

// Get the track at the index
Playlist &NLE_Project::track( int index )
{
	if ( index >= 0 && index < tracks( ) )
		return *tracks_[ index ];
	int current = tractor->get_int( "_current_track" );
	if ( current >= 0 && current < tracks( ) )
		return *tracks_[ current ];
	return *tracks_[ 0 ];
}

// Turn transitions off or on
void NLE_Project::transitions( bool value )
{
	block( );
	tractor->set( "nle_transitions", value );
	if ( value )
	{
		apply_transitions( );
	}
	else
	{
		for ( int i = 0; i < count( 0 ); i ++ )
			remove_transitions( 0, i );
	}
	unblock( );
}

bool NLE_Project::transitions( )
{
	return tractor->get_int( "nle_transitions" );
}

// Turn track locking on or off
void NLE_Project::lock_tracks( bool value )
{
	tractor->set( "nle_lock_tracks", !value );
}

bool NLE_Project::lock_tracks( )
{
	return tractor->get_int( "nle_lock_tracks" );
}

// Apply transitions to the track specified
void NLE_Project::apply_transitions( int track )
{
	int original = length( 0 );
	bool on = tractor->get_int( "nle_transitions" );
	int duration = tracks_[ 0 ]->get_int( "nle_transition_length" );
	for ( int i = count( track ) - 1; on && duration > 0 && track == 0 && i > 0; i -- )
	{
		Producer *left = tracks_[ 0 ]->get_clip( i - 1 );
		Producer *right = tracks_[ 0 ]->get_clip( i );
		if ( !right->parent( ).get_int( "nle_mix" ) && right->get_playtime( ) > duration && !left->parent( ).same_clip( right->parent( ) ) &&
		     ( !left->is_blank( ) && !right->is_blank( ) ) )
		{
			Tractor mixer;
			mixer.set( "nle_type", right->parent( ).get_int( "nle_type" ) );
			mixer.set( "title", right->parent( ).get( "title" ) );
			mixer.set( "global_feed", 1 );
			mixer.set( "nle_mix", 1 );
			Playlist track0;
			mixer.set_track( track0, 0 );
			// Note that the producer cut is relative to the parent hence the absolute position here
			// TODO: Check if that's necessary
			int position = left->get_in( ) + left->get_playtime( ) - 1;
			Producer *out_frame = left->cut( position, position );
			track0.append( *out_frame );
			track0.repeat( 0, duration );

			mixer.set_track( *right, 1 );
			char *transition = tracks_[ 0 ]->get( "nle_transition" );
			if ( transition == NULL || !strcmp( transition, "" ) )
				transition = "luma";
			Transition luma( transition );
			luma.set_in_and_out( 0, duration - 1 );
			luma.pass_values( *tracks_[ 0 ], "nle_transition." );
			mixer.plant_transition( luma );
			tracks_[ 0 ]->insert( mixer, i, 0, mixer.get_playtime( ) );
			tracks_[ 0 ]->remove( i + 1 );

			// Promotion of meta properties to the cut on the playlist
			Producer *cut = tracks_[ 0 ]->get_clip( i );
			for ( int j = 0; j < left->count( ); j ++ )
			{
				if ( !strncmp( left->get_name( j ), "meta.attr.", 10 ) )
					out_frame->set( left->get_name( j ), left->get( j ) );
			}
			delete cut;
			delete out_frame;
		}
		delete left;
		delete right;
	}
	// This should never happen since these transitions don't alter length
	if ( original != length( 0 ) )
		fprintf( stderr, "apply transitions altered the length of the track %d %d = %d\n", original, length( 0 ), original - length( 0 ) );
}

// Get the text attributes associated to this track
const char *NLE_Project::attributes( int track )
{
	if ( track == -1 )
		track = current_track( );
	return tracks_[ track ]->get( "nle_attributes" );
}

// Remove transitions from the track specified
void NLE_Project::remove_transitions( int track, int cut )
{
	int original = length( 0 );
	if ( track == 0 && cut >= 0 && cut < count( 0 ) )
	{
		Producer *right = tracks_[ 0 ]->get_clip( cut );
		if ( right->parent( ).get_int( "nle_mix" ) )
		{
			Tractor tractor( right->parent( ) );
			Producer *track = tractor.track( 1 );
			tracks_[ 0 ]->remove( cut );
			tracks_[ 0 ]->insert( *track, cut );
			delete track;
		}
		delete right;
	}
	// This should never happen since these transitions don't alter length
	if ( original != length( 0 ) )
		fprintf( stderr, "remove transitions altered the length of the track\n" );
}

// Prepare cutting
void NLE_Project::prepare_cuts( )
{
	for ( int track = 0; track < tracks( ); track ++ )
		tracks_[ track ]->consolidate_blanks( );

	for ( int track = 0; track < tracks( ); track ++ )
	{
		// Ensure all tracks are the same length
		normalise_length( track );

		// For each cut in track
		for ( int cut = 0; cut < count( track ); cut ++ )
		{
			// Get the cut
			Producer *item = tracks_[ track ]->get_clip( cut );

			// Special cases for blanks and stills
			if ( item->is_blank( ) )
			{
				int unused = tracks_[ track ]->blanks_from( cut, 1 );
				item->set( "_max_length", unused );
			}
			else if ( item->parent( ).get_int( "nle_type" ) == STILLS )
			{
				int unused = tracks_[ track ]->blanks_from( cut + 1, tracks_[ track ]->count( ) ) + item->get_playtime( );
				if ( item->parent( ).get_length( ) < unused )
				{
					item->set( "length", unused );
					item->parent( ).set( "length", unused );
				}
				item->set( "_max_length", unused );
			}

			delete item;
		}
	}
}

// Returns the total length of the project
int NLE_Project::length( )
{
	if ( tractor != NULL )
		return tractor->get_playtime( );
	return 0;
}

// Returns the number of tracks
int NLE_Project::tracks( )
{
	return tractor->count( );
}

// Returns the length of a specific track
int NLE_Project::length( int track )
{
	if ( track >= 0 && track < tracks( ) )
		return tracks_[ track ]->get_playtime( );
	return 0;
}

// Returns the number of cuts on the track
int NLE_Project::count( int track )
{
	if ( track >= 0 && track < tracks( ) )
		return tracks_[ track ]->count( );
	return 0;
}

// Return true if the cut specified is a blank
bool NLE_Project::is_blank( int track, int cut )
{
	if ( track >= 0 && track < tracks( ) && cut >= 0 && cut < count( track ) )
		return tracks_[ track ]->is_blank( cut );
	return false;
}

// Return true if the cut specified is a blank
bool NLE_Project::is_blank_at( int track, int value )
{
	if ( track == -1 )
		track = current_track( );
	if ( value == -1 )
		value = position( );
	int cut = cut_index_at( track, value );
	if ( track >= 0 && track < tracks( ) && cut >= 0 && cut < count( track ) )
		return tracks_[ track ]->is_blank( cut );
	return false;
}

// Return the cut producer at the track and cut specified
Producer *NLE_Project::get_cut( int track, int cut )
{
	// Determine track and cut if either is given as -1
	track = track == -1 && tractor != NULL ? tractor->get_int( "_current_track" ) : track;
	if ( tractor != NULL && cut == -1 )
	{
		int position = tractor->position( );
		cut = tracks_[ track ]->get_clip_index_at( position );
	}

	// If we have a valid position, the return the real cut at the indexes provided
	if ( track >= 0 && track < tracks( ) && cut >= 0 && cut < count( track ) )
	{
		Producer *producer = tracks_[ track ]->get_clip( cut );
		// Special case for the nle auto transition type
		if ( producer->parent( ).get_int( "nle_mix" ) )
		{
			Tractor tractor( producer->parent( ) );
			delete producer;
			producer = tractor.track( 1 );
		}
		return producer;
	}
	return NULL;
}

// Return the cut producer at the position and track specified
Producer *NLE_Project::get_cut_at( int position, int track )
{
	// Determine track and cut if either is given as -1
	track = track == -1 && tractor != NULL ? tractor->get_int( "_current_track" ) : track;
	int cut = tracks_[ track ]->get_clip_index_at( position );
	return get_cut( track, cut );
}

bool NLE_Project::locate_cut( int &track, int &cut, Producer *producer )
{
	bool found = false;
	if ( producer != NULL && producer->is_valid( ) )
	{
		for ( int i = 0; !found && i < tracks( ); i ++ )
		{
			for ( int j = 0; !found && j < count( i ); j ++ )
			{
				Producer *item = get_cut( i, j );
				found = item->get_producer( ) == producer->get_producer( );
				track = i;
				cut = j;
				delete item;
			}
		}
	}
	return found;
}

// Returns the index of the cut at the position
int NLE_Project::cut_index_at( int track, int position )
{
	if ( track >= 0 && track < tracks( ) )
		return tracks_[ track ]->get_clip_index_at( position );
	return -1;
}

// Returns the type of a specific track
BrowserShow NLE_Project::type( int track )
{
	if ( track >= 0 && track < tracks( ) )
		return ( BrowserShow )tracks_[ track ]->get_int( "nle_type" );
	return INVALID;
}

// Returns the type of a producer
BrowserShow NLE_Project::type( Producer *producer )
{
	if ( producer != NULL && producer->is_valid( ) )
	{
		BrowserShow ptype = ( BrowserShow )producer->parent( ).get_int( "nle_type" );
		return ptype;
	}
	return INVALID;
}

// Returns true if the track accepts the producer
bool NLE_Project::accepts( int track, Producer *producer )
{
	return type( track ) & type( producer );
}

// Returns the cut index at the absolute position
int NLE_Project::cut_index( int track, int position )
{
	if ( track >= 0 && track < tracks( ) )
		return tracks_[ track ]->get_clip_index_at( position );
	return -1;
}

// Returns the start position of the producer's place in the track or -1
int NLE_Project::insert_at( int track, int position, Producer *producer )
{
	int ret = -1;

	if ( accepts( track, producer ) )
	{
		int original_track_length = length( track );
		int original_length = length( );
		BrowserShow track_type = type( track );
		int cut_idx = -1;
		bool maintain_lower = true;

		block( );

		// Handle the insertion depending on the type of the producer or track
		if ( producer->parent( ).get_int( "nle_type" ) & PROJECTS && track_type & PROJECTS )
		{
			// Get the in and out point of the cut
			int in = producer->get_in( );
			int out = producer->get_out( );

			// Calculate the real insertion point
			position = nearest_insert( track, position );

			// Should be sufficient to typecast the producer parent to an NLE_Project
			Tractor tractor( producer->parent( ) );
			NLE_Project project( tractor );

			// Turn the transitions off
			project.transitions( false );

			// Ensure that we get an update but no post insert mods to other tracks
			cut_idx = tracks_[ track ]->get_clip_index_at( position );
			maintain_lower = false;
			ret = tracks_[ track ]->clip_start( cut_idx );

			// Remove transitions to the left and right
			remove_transitions( track, cut_idx - 1 );
			remove_transitions( track, cut_idx + 1 );

			// Now for the fun stuff :-/
			for ( int i = 0; i < tracks( ) && i < project.tracks( ); i ++ )
			{
				// Normalise both tracks
				normalise_length( i );
				project.normalise_length( i );

				// Split the track at the position
				if ( position < length( ) )
					tracks_[ track ]->split_at( position );

				// Determine the destination
				int dst = tracks_[ track ]->get_clip_index_at( position );

				// Now loop through each of the cuts 
				for ( int j = in; j < out; )
				{
					int added = 0;
					int idx = project.track( i ).get_clip_index_at( j );
					Producer *cut = project.track( i ).get_clip( idx );
					int s = project.track( i ).clip_start( idx );
					int l = project.track( i ).clip_length( idx );
					if ( cut->is_blank( ) )
					{
						added = out - j;
						if ( added > l ) added = l - 1;
						tracks_[ i ]->insert_blank( dst ++, added );
					}
					else if ( s <= j && s + l > out )
					{
						tracks_[ i ]->insert( cut->parent( ), dst ++, cut->get_in( ) + j - s, cut->get_in( ) + out - j );
					}
					else
					{
						tracks_[ i ]->insert( cut->parent( ), dst ++, cut->get_in( ) + j - s, cut->get_in( ) + l - 1 );
					}

					// Obtain the new cut
					Producer *dest = tracks_[ i ]->get_clip_at( j );

					// Update the current position
					j += dest->get_playtime( );

					// Copy meta data from the original cut to the new cut
					for ( int p = 0; p < cut->count( ); p ++ )
						if ( !strncmp( cut->get_name( p ), "meta.", 5 ) )
							dest->set( cut->get_name( p ), cut->get( p ) );

					// Clean up
					delete dest;
					delete cut;
				}
			}
		}
		else if ( track == 0 )
		{
			producer->parent( ).set( "global_feed", 0 );
			cut_idx = tracks_[ track ]->insert_at( position, producer );
			// Remove transitions to the left and right
			remove_transitions( track, cut_idx - 1 );
			remove_transitions( track, cut_idx + 1 );
		}
		else if ( track_type & AUDIO )
		{
			producer->parent( ).set( "global_feed", 0 );
			cut_idx = tracks_[ track ]->insert_at( position, producer, 1 );

			// Special case - audio should not extend the length of the project
			// so try to shrink down any blank created to the left
			if ( length( track ) > original_length && cut_idx > 0 && cut_idx == count( track ) - 1 )
			{
				if ( tracks_[ track ]->is_blank( cut_idx - 1 ) )
				{
					int blanks = tracks_[ track ]->clip_length( cut_idx - 1 );
					int junk = length( track ) - original_length + 1;
					if ( junk >= blanks )
						tracks_[ track ]->remove( -- cut_idx );
					else
						tracks_[ track ]->resize_clip( cut_idx - 1, 0, blanks - junk );
				}
			}
		}
		else if ( track_type & TEXT )
		{
			cut_idx = tracks_[ track ]->insert_at( position, producer, 1 );
		}

		// Now handle the modifications of the other tracks
		if ( cut_idx != -1 && maintain_lower )
		{
			int start = tracks_[ track ]->clip_start( cut_idx );
			int playtime = producer->get_playtime( );
			for ( int i = 0; i < tracks( ); i ++ )
			{
				if ( i != track )
				{
					BrowserShow affected_type = type( i );
					if ( lock_tracks( ) && track == 0 && affected_type & VOICE && length( i ) > start && original_track_length > 0 )
					{
						int idx = tracks_[ i ]->get_clip_index_at( start );
						if ( tracks_[ i ]->clip_start( idx ) == start && !tracks_[ i ]->is_blank( idx ) )
							tracks_[ i ]->insert_blank( idx, playtime );
						else
							tracks_[ i ]->pad_blanks( start, playtime, 1 );
						tracks_[ i ]->consolidate_blanks( );
					}
					else if ( track == 0 && affected_type == TEXT && length( i ) > start )
					{
						int idx = tracks_[ i ]->get_clip_index_at( start );
						if ( tracks_[ i ]->clip_start( idx ) == start && !tracks_[ i ]->is_blank( idx ) )
							tracks_[ i ]->insert_blank( idx, playtime );
						else
							tracks_[ i ]->pad_blanks( start, playtime, 1 );
						tracks_[ i ]->consolidate_blanks( );
					}
				}
			}
			ret = start;
		}

		apply_transitions( track );
		unblock( ret != -1 );
	}
	return ret;
}

// Returns the new position after the delete or -1
int NLE_Project::delete_cut( int track, int cut_idx )
{
	BrowserShow track_type = type( track );
	int ret = -1;

	if ( track_type && cut_idx >= 0 && cut_idx < count( track ) )
	{
		int start = tracks_[ track ]->clip_start( cut_idx );
		int playtime = tracks_[ track ]->clip_length( cut_idx );

		block( );

		if ( track == 0 )
		{
			remove_transitions( track, cut_idx - 1 );
			remove_transitions( track, cut_idx + 1 );
			tracks_[ track ]->remove( cut_idx );
			if ( cut_idx > 0 && cut_idx == count( track ) ) cut_idx --;
		}
		else if ( track_type & AUDIO )
		{
			if ( tracks_[ track ]->is_blank( cut_idx ) )
			{
				tracks_[ track ]->remove( cut_idx );
				if ( cut_idx > 0 && cut_idx == count( track ) ) cut_idx --;
				if ( cut_idx < count( track ) - 1 )
				{
					int len = tracks_[ track ]->clip_length( cut_idx + 1 );
					if ( tracks_[ track ]->is_blank( cut_idx + 1 ) )
						tracks_[ track ]->resize_clip( cut_idx + 1, 0, len + playtime );
					else
						tracks_[ track ]->insert_blank( cut_idx + 1, playtime );
				}
				tracks_[ track ]->consolidate_blanks( );
			}
			else
			{
				Producer *old = tracks_[ track ]->replace_with_blank( cut_idx );
				delete old;
				tracks_[ track ]->consolidate_blanks( );
				// Ensure that position doesn't change in this case
				cut_idx = -1;
			}
			// Deletes here do not affect other tracks
			playtime = 0;
		}
		else
		{
			playtime = 0;
		}

		// Now correct the other tracks
		for ( int i = 0; playtime && i < tracks( ); i ++ )
		{
			if ( i != track )
			{
				BrowserShow affected_type = type( i );
				if ( lock_tracks( ) && track == 0 && length( i ) > start )
				{
					// If the overlapping clip is a blank, then we need to remove
					// start to start + playtime from it
					int idx = tracks_[ i ]->get_clip_index_at( start );
					if ( tracks_[ i ]->is_blank( idx ) )
					{
						int overlap_start = tracks_[ i ]->clip_start( idx );
						if ( overlap_start < start )
						{
							tracks_[ i ]->split( idx ++, start - overlap_start );
							overlap_start += start - overlap_start;
						}
						int overlap_length = tracks_[ i ]->clip_length( idx );
						if ( overlap_length > playtime )
							tracks_[ i ]->split( idx, playtime );
						tracks_[ i ]->remove( idx );
						tracks_[ i ]->consolidate_blanks( );
					}
				}
				else if ( track == 0 && length( i ) > start && affected_type & TEXT )
				{
					remove_section( i, start, start + playtime - 1 );
				}
			}
		}

		// Finally return the start if the cut_idx is not -1
		if ( cut_idx != -1 )
			ret = tracks_[ track ]->clip_start( cut_idx );

		apply_transitions( track );
		if ( ret != -1 )
			seek( ret );
		unblock( );
	}
	return ret;
}

// Split the cut at the absolute position and return the first frame of the 
// right hand cut or -1 if none occurred
int NLE_Project::split_at( int track, int position )
{
	int start = -1;
	BrowserShow track_type = type( track );
	if ( track_type != INVALID && position >= 0 && position < length( track ) )
	{
		int done = 0;
		int index = tracks_[ track ]->get_clip_index_at( position );

		block( );
		remove_transitions( track, index );
		remove_transitions( track, index + 1 );
		start = tracks_[ track ]->clip_start( index );
		if ( position - start > 0 )
		{
			if ( track == 0 )
			{
				done = !tracks_[ track ]->split_at( position - 1 );
				if ( super_track( ) > 0 && length( super_track( ) ) > position )
					tracks_[ super_track( ) ]->split_at( position - 1 );
			}
			else if ( track_type & AUDIO && !tracks_[ track ]->is_blank( index ) )
			{
				done = !tracks_[ track ]->split_at( position - 1 );
			}
		}
		start = done ? tracks_[ track ]->clip_start( index + 1 ) : -1;
		apply_transitions( track );
		unblock( );
	}
	return start;
}

// Returns the cut index if the cut is movable or -1
int NLE_Project::movable( int track, int position )
{
	int cut = -1;
	BrowserShow track_type = type( track );
	if ( track_type != INVALID && position >= 0 && position <= length( track ) )
	{
		cut = tracks_[ track ]->get_clip_index_at( position );
		if ( track_type & AUDIO && tracks_[ track ]->is_blank( cut ) )
			cut = -1;
		else if ( track_type & TEXT )
			cut = -1;
	}
	return cut;
}

// Moves the cut to the position or near there [depending on track rules]
// Returns the real start position used or -1 on failure
int NLE_Project::move_to( int track, int cut, int position )
{
	int ret = -1;
	BrowserShow track_type = type( track );
	if ( track_type != INVALID && cut >= 0 && cut < tracks_[ track ]->count( ) )
	{
		int original_length = length( );

		// Ensure that the position is valid
		if ( position < 0 ) position = 0;

		int start = tracks_[ track ]->clip_start( cut );
		int playtime = tracks_[ track ]->clip_length( cut );
		int index = tracks_[ track ]->get_clip_index_at( position );
		int index_start = tracks_[ track ]->clip_start( index );
		int index_length = tracks_[ track ]->clip_length( index );
		int offset = this->position( ) - start;

		block( );

		if ( track == 0 )
		{
			remove_transitions( track, cut - 1 );
			remove_transitions( track, cut );
			remove_transitions( track, cut + 1 );
			remove_transitions( track, index - 1 );
			remove_transitions( track, index );
			remove_transitions( track, index + 1 );
			if ( cut < index && position < index_start + index_length / 2 && index > 0 )
				index --;
			else if ( cut > index && position >= index_start + index_length / 2 && index < count( track ) - 1 )
				index ++;
			if ( cut != index )
				tracks_[ track ]->move( cut, index );
			else
				playtime = 0;
			if ( position > length( track ) ) index --;
			ret = tracks_[ track ]->clip_start( index ) + offset;
		}
		else if ( track_type & AUDIO || track_type & TEXT )
		{
			if ( length( track ) < length( ) )
				tracks_[ track ]->blank( length( ) - length( track ) );

			int displaced = 0;
			Producer *original = tracks_[ track ]->replace_with_blank( cut );
			tracks_[ track ]->consolidate_blanks( true );

			// Recalculate drop position after blank modification
			index = tracks_[ track ]->get_clip_index_at( position );
			index_start = tracks_[ track ]->clip_start( index );
			index_length = tracks_[ track ]->clip_length( index );

			// Try to find a blank to the left or right first
			if ( index > 0 && index < count( track ) && !tracks_[ track ]->is_blank( index ) )
			{
				int midpoint = index_start + index_length / 2;
				if ( cut < index && position < midpoint && index > 0 && tracks_[ track ]->is_blank( index - 1 ) )
					index --;
				else if ( cut > index && position >= midpoint && index < count( track ) - 1 && tracks_[ track ]->is_blank( index + 1 ) )
					index ++;
				index_start = tracks_[ track ]->clip_start( index );
				index_length = tracks_[ track ]->clip_length( index );
			}

			if ( index >= 0 && index < count( track ) && tracks_[ track ]->is_blank( index ) )
			{
				// Try to fit it into the existing blank first
				if ( position + playtime > index_start + index_length && index_length > playtime )
					position = index_start + index_length - playtime;

				// If we can, then split a section in the middle of the blank
				if ( position + playtime <= index_start + index_length )
				{
					if ( position > index_start )
					{
						tracks_[ track ]->split( index, position - index_start - 1 );
						index ++;
						index_start = tracks_[ track ]->clip_start( index );
						index_length = tracks_[ track ]->clip_length( index );
					}
					if ( playtime < index_length )
						tracks_[ track ]->split( index, playtime );
					tracks_[ track ]->remove( index );
					tracks_[ track ]->insert( *original, index );
				}
				else
				{
					displaced = playtime - index_length;
					tracks_[ track ]->remove( index );
					tracks_[ track ]->insert( *original, index );
				}
			}
			else
			{
				int midpoint = index_start + index_length / 2;
				if ( cut < index && position < midpoint && index > 0 )
					index --;
				else if ( cut > index && position >= midpoint && index < count( track ) - 1 )
					index ++;
				tracks_[ track ]->insert( *original, index );
			}

			// Handle the displaced blanks
			for ( int i = index + 1; displaced > 0 && i < count( track ); i ++ )
			{
				if ( tracks_[ track ]->is_blank( i ) )
				{
					int len = tracks_[ track ]->clip_length( i );
					if ( displaced < len )
						tracks_[ track ]->resize_clip( i, 0, len - displaced );
					else if ( displaced >= len )
						tracks_[ track ]->remove( i );
				}
			}
			tracks_[ track ]->consolidate_blanks( );

			// Special case - audio should not extend the length of the project
			// so try to shrink down any blank created to the left
			if ( length( track ) > original_length && index > 0 && index == count( track ) - 1 )
			{
				if ( tracks_[ track ]->is_blank( index - 1 ) )
				{
					int blanks = tracks_[ track ]->clip_length( index - 1 );
					int junk = length( track ) - original_length + 1;
					if ( junk >= blanks )
						tracks_[ track ]->remove( -- index );
					else
						tracks_[ track ]->resize_clip( index - 1, 0, blanks - junk );
				}
			}

			delete original;
			ret = tracks_[ track ]->clip_start( index ) + offset;
		}

		for ( int i = 0; playtime && i < tracks( ); i ++ )
		{
			if ( i != track )
			{
				BrowserShow affected_type = type( i );
				if ( track == 0 && ( ( lock_tracks( ) ) || affected_type & TEXT ) )
				{
					// Pad out the track to match lengths
					if ( length( i ) < length( ) )
						tracks_[ i ]->blank( length( ) - length( i ) );

					// Split out the entire section for start to start + playtime
					tracks_[ i ]->split_at( start );
					tracks_[ i ]->split_at( start + playtime );

					// Get the index of the left hand cut 
					int left = tracks_[ i ]->get_clip_index_at( start );

					// Get the index of the right hand cut
					int right = tracks_[ i ]->get_clip_index_at( start + playtime - 1 );

					// Move all the displaced cuts into a temporary playlist
					Playlist temp;
					for ( int j = left; j <= right && left < count( i ); j ++ )
					{
						Producer *cut = tracks_[ i ]->get_clip( left );
						tracks_[ i ]->remove( left );
						temp.append( *cut );
						delete cut;
					}

					// Split at the drop point and get the index of the cut
					tracks_[ i ]->split_at( ret - offset );
					left = tracks_[ i ]->get_clip_index_at( ret - offset );
					for ( int j = 0; j < temp.count( ); j ++ )
					{
						Producer *cut = temp.get_clip( j );
						tracks_[ i ]->insert( *cut, left + j );
						delete cut;
					}

					tracks_[ i ]->consolidate_blanks( );
				}
				else if ( track == 0 && length( i ) > start && affected_type & TEXT )
				{
					tracks_[ i ]->move_region( start, playtime, position );
					tracks_[ i ]->consolidate_blanks( );
				}
			}
		}
	
		apply_transitions( track );
		if ( ret != -1 )
			seek( ret );
		unblock( playtime != 0 );
	}
	return ret;
}

int NLE_Project::resize_cut( int track, int cut, int &in, int &out, int position )
{
	int ret = -1;
	BrowserShow track_type = type( track );

	if ( track_type != INVALID && cut >= 0 && cut < count( track ) )
	{
		block( );
		
		int original_length = length( );
		int original_cut_length = tracks_[ track ]->clip_length( cut );
		int new_length = out - in + 1;
		int difference = original_cut_length - new_length;

		ClipInfo info;
		tracks_[ track ]->clip_info( cut, &info );
		int in_diff = info.frame_in - in;
		int out_diff = out - info.frame_out;

		if ( track == 0 )
		{
			remove_transitions( track, cut - 1 );
			remove_transitions( track, cut );
			remove_transitions( track, cut + 1 );
			tracks_[ track ]->resize_clip( cut, in, out );

			// Special case - resize the supers and other locked tracks associated to the visual
			for ( int affected = 1; difference != 0 && affected < tracks( ); affected ++ )
			{
				if ( affected == super_track( ) || lock_tracks( ) )
				{
					if ( out_diff < 0 )
						remove_section( affected, info.start + info.frame_count + out_diff, info.start + info.frame_count - 1 );
					else if ( out_diff > 0 )
						tracks_[ affected ]->insert_blank( tracks_[ affected ]->get_clip_index_at( info.start + info.frame_count ), out_diff - 1 );
		
					if ( in_diff < 0 )
						remove_section( affected, info.start, info.start - in_diff - 1 );
					else if ( in_diff > 0 )
						tracks_[ affected ]->insert_blank( tracks_[ affected ]->get_clip_index_at( info.start ), in_diff - 1 );
				}
			}
		}
		else if ( track_type & AUDIO || track_type & TEXT )
		{
			int following = -1;

			if ( track_type & TEXT )
			{
				normalise_length( track );
				int bound_out = tracks_[ track ]->clip_length( cut ) - 1;
				int owner = tracks_[ 0 ]->get_clip_index_at( info.start );
				tracks_[ track ]->split_at( tracks_[ 0 ]->clip_start( owner + 1 ) );
				if ( cut < tracks_[ track ]->count( ) - 1 && tracks_[ track ]->is_blank( cut + 1 ) )
					bound_out += tracks_[ track ]->clip_length( cut + 1 );
				if ( out > bound_out )
				{
					out = bound_out;
					new_length = out - in + 1;
					difference = original_cut_length - new_length;
				}
			}

			// Resize the clip
			tracks_[ track ]->resize_clip( cut, in, out );

			// Locate the next non-blank
			if ( tracks_[ track ]->is_blank( cut ) )
				following = cut + 2;
			else
				following = cut + 1;

			// Increase by the difference
			if ( following < count( track ) && difference > 0 )
			{
				tracks_[ track ]->insert_blank( following, difference - 1 );
			}

			// Decrease by the difference
			else if ( following < count( track ) && difference < 0 )
			{
				difference = - difference;

				while ( difference != 0 )
				{
					// Look for the next blank
					while ( following < count( track ) && !tracks_[ track ]->is_blank( following ) )
						following ++;
					
					if ( following < count( track ) )
					{
						int length = tracks_[ track ]->clip_length( following );
						if ( length > difference )
						{
							tracks_[ track ]->resize_clip( following, 0, length - difference - 1 );
							difference = 0;
						}
						else if ( length <= difference )
						{
							tracks_[ track ]->remove( following );
							difference -= length;
						}
					}
					else
					{
						difference = 0;
					}
				}
			}

			tracks_[ track ]->consolidate_blanks( );
			// Special case - audio should not extend the length of the project
			// so try to shrink down any blank created to the left
			int index = count( track ) - 1;
			if ( length( track ) > original_length && index > 0 && index == count( track ) - 1 )
			{
				if ( tracks_[ track ]->is_blank( index - 1 ) )
				{
					int blanks = tracks_[ track ]->clip_length( index - 1 );
					int junk = length( track ) - original_length + 1;
					if ( junk >= blanks )
						tracks_[ track ]->remove( -- index );
					else
						tracks_[ track ]->resize_clip( index - 1, 0, blanks - junk );
				}
			}

			tracks_[ track ]->consolidate_blanks( );
		}

		if ( position != -1 )
			seek( position );

		apply_transitions( track );
		unblock( );
	}

	return ret;
}

// Forwards the drop object at the given position
int NLE_Project::forward_drop_object_at( int track, int position )
{
	int ret = 0;
	int index = movable( track, position );
	if ( index != -1 )
	{
		Producer *cut = tracks_[ track ]->get_clip( index );
		NLE::Forward_Drop_Object( cut, "move" );
		delete cut;
		ret = 1;
	}
	return ret;
}

// Get information about the current cut for further trimming
Producer *NLE_Project::cut_info( int &offset, int &in, int &out, int &length, int &flags )
{
	Producer *producer = NULL;
	int track = tractor->get_int( "_current_track" );
	BrowserShow track_type = type( track );
	int cut = -1;
	if ( track_type != INVALID )
		cut = tracks_[ track ]->get_clip_index_at( tractor->position( ) );

	if ( track_type != INVALID && track_type != TEXT && cut >= 0 && cut < count( track ) )
	{
		ClipInfo *info = tracks_[ track ]->clip_info( cut );
		producer = get_cut( track, cut );
		offset = info->start;
		if ( info->producer->get_int( "nle_mix" ) )
		{
			in = producer->get_in( );
			out = producer->get_out( );
			length = producer->get_length( );
		}
		else
		{
			in = info->frame_in;
			out = info->frame_out;
			length = info->cut->get_length( );
		}
		flags = DND | INSERT;
		if ( producer->is_blank( ) || producer->parent( ).get_int( "nle_type" ) == STILLS )
		{
			length = producer->get_int( "_max_length" );
			if ( producer->is_blank( ) ) flags |= IN;
		}

		if ( tractor->get_int( "nle_transitions" ) )
			flags |= IN | OUT;

		delete info;
	}
	return producer;
}

// Delete the super at the specified position
void NLE_Project::delete_super_at( int value )
{
	int track = super_track( );
	if ( track > 0 )
	{
		if ( value == -1 ) value = position( );
		int cut = tracks_[ track ]->get_clip_index_at( value );
		if ( value >= 0 && value < length( track ) && !tracks_[ track ]->is_blank( cut ) )
		{
			block( );
			delete tracks_[ track ]->replace_with_blank( cut );
			tracks_[ track ]->consolidate_blanks( );
			unblock( );
		}
	}
}

// Fetch or create a super at the specified position
Producer *NLE_Project::fetch_super_at( int type, int &start, int &in, int &out, int &len, int value )
{
	Producer *result = NULL;
	int track = super_track( );
	if ( track > 0 )
	{
		// Ensure the position is valid
		if ( value == -1 ) value = position( );

		// Fetch the 'owner' of the super (this is always the cut on the track which overlaps
		// with the position - ultimately, the super track doesn't behave like a track at all 
		// and really shouldn't be one :-/).
		int owner = tracks_[ track - 1 ]->get_clip_index_at( value );
		int owner_in = tracks_[ track - 1 ]->clip_start( owner );
		int owner_out = owner_in + tracks_[ track - 1 ]->clip_length( owner ) - 1;

		// Get the cut at the position
		int cut = tracks_[ track ]->get_clip_index_at( value );

		// If the cut is not a blank, then fetch it
		if ( value >= 0 && value < length( super_track( ) ) && !tracks_[ track ]->is_blank( cut ) )
		{
			result = tracks_[ track ]->get_clip( cut );

			// Allow type modifications on existing supers
			if ( type != 0 && result->get_int( "meta.super.type" ) != type )
			{
				block( );
				result->set( "meta.super.type", type );
				unblock( );
			}
		}
		else if ( type != 0 && value >= 0 && value < length( ) )
		{
			// Add a new super
			block( );

			// We'll start by padding the entire track so that they're the same length
			normalise_length( track );

			// Now we'll split at the bounds and the position
			tracks_[ track ]->split_at( owner_in );
			tracks_[ track ]->split_at( owner_out, false );
			tracks_[ track ]->split_at( value );

			// Re-evaluate the cut index
			cut = tracks_[ track ]->get_clip_index_at( value );

			// Now we'll split the cut at 5 seconds 
			if ( tracks_[ track ]->clip_length( cut ) > 125 )
				tracks_[ track ]->split_at( value + 124, false );

			// Now we create a new producer (arbitrary - colour:black used here)
			Producer original( "colour", "black" );
			original.set( "length", owner_out - owner_in + 1 );
			original.set( "out", owner_out - owner_in );
			tracks_[ track ]->insert( original, cut, 0, tracks_[ track ]->clip_length( cut ) - 1 );

			// Now we remove the displaced blank
			tracks_[ track ]->remove( cut + 1 );

			// Now we fetch the cut and assign it the type
			result = tracks_[ track ]->get_clip( cut );
			result->set( "meta.super.type", type );

			// Clean up and fire an event
			tracks_[ track ]->consolidate_blanks( );
			unblock( );
		}

		// Calculate start, in, out and length
		if ( result != NULL )
		{
			// Determine the 'owner' being the cut on the video above the super start
			start = tracks_[ track - 1 ]->clip_start( owner );
			len = tracks_[ track - 1 ]->clip_length( owner );
			in = tracks_[ track ]->clip_start( cut );
			out = in + tracks_[ track ]->clip_length( cut ) - 1;
		}
	}
	return result;
}

bool NLE_Project::sizeof_super_at( int &start, int &in, int &out, int &len, int value )
{
	int track = super_track( );
	if ( track > 0 )
	{
		// Ensure the position is valid
		if ( value == -1 ) value = position( );

		// Fetch the 'owner' of the super (this is always the cut on the track which overlaps
		// with the position - ultimately, the super track doesn't behave like a track at all 
		// and really shouldn't be one :-/).
		int owner = tracks_[ track - 1 ]->get_clip_index_at( value );
		int owner_in = tracks_[ track - 1 ]->clip_start( owner );
		int owner_out = owner_in + tracks_[ track - 1 ]->clip_length( owner ) - 1;

		// Get the cut at the position
		int cut = tracks_[ track ]->get_clip_index_at( value );

		start = tracks_[ track - 1 ]->clip_start( owner );
		len = tracks_[ track - 1 ]->clip_length( owner );
		in = owner_in;
		out = owner_out;

		// If a super or blank exists, then correct in/out accordingly
		if ( value >= 0 && value < length( super_track( ) ) )
		{
			in = tracks_[ track ]->clip_start( cut );
			out = in + tracks_[ track ]->clip_length( cut ) - 1;
		}
		else if ( start < length( super_track( ) ) )
		{
			in = length( super_track( ) );
		}

		// Now ensure that we're in the bounds of the owner
		if ( in < start )
			in = start;
		if ( out >= start + len )
			out = start + len - 1;

		return true;
	}
	else
	{
		return false;
	}
}

// Return the type of the super the specified position
int NLE_Project::typeof_super_at( int value )
{
	int type = 0;
	int track = super_track( );
	if ( track > 0 )
	{
		if ( value == -1 ) value = position( );
		Producer *temp = tracks_[ track ]->get_clip_at( value );
		if ( temp != NULL && !temp->is_blank( ) )
			type = temp->get_int( "meta.super.type" );
		delete temp;
	}
	return type;
}

int NLE_Project::move_super_to( Producer *super, int value )
{
	int track, cut;
	int position = value;
	if ( locate_cut( track, cut, super ) && track == super_track( ) )
	{
		block( );
		// Correct the value
		if ( value < 0 ) value = 0;
		// Normalise the length
		normalise_length( track );
		// The new position must fall in the bounds of two adjacent blanks
		int bound_in = tracks_[ track ]->clip_start( cut );
		int bound_out = bound_in + tracks_[ track ]->clip_length( cut ) - 1;
		if ( cut > 0 && tracks_[ track ]->is_blank( cut - 1 ) )
			bound_in = tracks_[ track ]->clip_start( cut - 1 );
		if ( cut < tracks_[ track ]->count( ) - 1 && tracks_[ track ]->is_blank( cut + 1 ) )
			bound_out = tracks_[ track ]->clip_start( cut + 1 ) + tracks_[ track ]->clip_length( cut + 1 ) - 1;
		position = value;
		if ( position < bound_in )
			position = bound_in;
		if ( position + super->get_playtime( ) > bound_out )
			position = bound_out - super->get_playtime( ) + 1;
		super = tracks_[ track ]->replace_with_blank( cut );
		tracks_[ track ]->consolidate_blanks( true );
		if ( position - 1 > 0 && tracks_[ track ]->is_blank_at( position - 1 ) )
			tracks_[ track ]->insert_at( position - 1, super );
		else
			tracks_[ track ]->insert_at( position, super );
		tracks_[ track ]->consolidate_blanks( true );
		delete super;
		unblock( );
	}
	return position;
}

bool NLE_Project::is_sb( int cut )
{
	Producer *cut_ = get_cut( 0, cut );
	bool ret = cut_->get_int( "nle_sb" );
	delete cut_;
	return ret;
}

void NLE_Project::convert_sb( bool value, int cut )
{
	block( );
	Producer *cut_ = get_cut( 0, cut );
	cut_->set( "nle_sb", value );
	delete cut_;
	if ( value )
	{
	}
	unblock( );
}

void NLE_Project::refresh_text( bool lock )
{
	int track = 0;

	// Lock if requested to do so
	if ( lock ) block( );

	// Get the attributes associated to the track
	const char *attr = attributes( track );

	// Tokenise
	Tokeniser tokens( ( char * )attr, "," );

	// Iterate through each attribute
	for ( int idx = 0; idx < tokens.count( ); idx ++ )
	{
		// Get the token and drop the special case flags
		char *token = tokens.get( idx );
		if ( *token == '*' ) token ++;

		// Set up the properties we use here
		char property[ 512 ];
		char temp_in[ 512 ];
		char temp_out[ 512 ];
		sprintf( property, "meta.attr.%s", token );
		sprintf( temp_in, "meta.attr.%s.in", token );
		sprintf( temp_out, "meta.attr.%s.out", token );

		// Loop through each cut and determine length of sequential attributes
		for ( int cut_idx = 0; cut_idx < tracks_[ track ]->count( ); )
		{
			int start_idx = cut_idx;
			int in = 0;
			int out = 0;
			int done = 0;

			do
			{
				Producer *cut = tracks_[ track ]->get_clip( cut_idx ++ );
				// Special case for mix items - we follow the attributes on the
				// lower track
				if ( cut->parent( ).get_int( "nle_mix" ) )
				{
					Tractor mix( cut->parent( ) );
					Producer *right = mix.track( 1 );
					if ( right->get_int( property ) != 0 )
						out += tracks_[ track ]->clip_length( cut_idx - 1 );
					else
						done = 1;
					delete right;
				}
				else
				{
					if ( cut->get_int( property ) != 0 )
						out += tracks_[ track ]->clip_length( cut_idx - 1 );
					else
						done = 1;
				}
				delete cut;
			}
			while( cut_idx < tracks_[ track ]->count( ) && !done );

			while( start_idx < cut_idx )
			{
				Producer *cut = tracks_[ track ]->get_clip( start_idx ++ );
				if ( cut->get_int( property ) != 0 )
				{
					cut->set( temp_in, in + cut->get_in( ) );
					cut->set( temp_out, out + cut->get_in( ) - 1 );
					in -= tracks_[ track ]->clip_length( start_idx - 1 );
					out -= tracks_[ track ]->clip_length( start_idx - 1 );
				}
				else
				{
					cut->set( temp_in, ( char * )NULL );
					cut->set( temp_out, ( char * )NULL );
				}
				delete cut;
			}
		}
	}

	// Now do something similar with the super track
	if ( super_track( ) > 0 )
	{
		// Get the super track index
		track = super_track( );

		// Set up the properties we use here
		char temp_in[ 512 ];
		char temp_out[ 512 ];
		sprintf( temp_in, "meta.attr.super.in" );
		sprintf( temp_out, "meta.attr.super.out" );

		// Loop through each cut and determine length of sequential supers
		for ( int cut_idx = 0; cut_idx < tracks_[ track ]->count( ); )
		{
			int start_idx = cut_idx;
			int in = 0;
			int out = 0;
			int done = 0;

			do
			{
				Producer *cut = tracks_[ track ]->get_clip( cut_idx ++ );
				if ( !cut->is_blank( ) )
					out += cut->get_playtime( );
				else
					done = 1;
				delete cut;
			}
			while( cut_idx < tracks_[ track ]->count( ) && !done );

			while( start_idx < cut_idx )
			{
				Producer *cut = tracks_[ track ]->get_clip( start_idx ++ );
				if ( !cut->is_blank( ) )
				{
					cut->set( temp_in, in + cut->get_in( ) );
					cut->set( temp_out, out + cut->get_in( ) - 1 );
					in -= cut->get_playtime( );
					out -= cut->get_playtime( );
				}
				delete cut;
			}
		}
	}

	// Unlock if necessary
	if ( lock ) unblock( );
}

int NLE_Project::insert_blanks( int length )
{
	int track = current_track( );
	int value = position( );
	if ( track != 0 )
	{
		int cut = cut_index_at( track, value );
		block( );
		// Ensure that we don't overrun the project length
		tracks_[ track ]->consolidate_blanks( );
		if ( length + tracks_[ track ]->get_playtime( ) > get_playtime( ) )
			length = get_playtime( ) - tracks_[ track ]->get_playtime( ) - 1;
		if ( length > 0 )
		{
			tracks_[ track ]->insert_blank( cut, length );
			value += length;
			seek( value + length );
		}
		unblock( );
	}
	return value;
}

void NLE_Project::snap( int align )
{
	int track = current_track( );
	int value = position( );
	if ( track != 0 && track != super_track( ) && value < tracks_[ track ]->get_length( ) && !tracks_[ track ]->is_blank_at( value ) )
	{
		// Get the lower cut info first
		int lower_cut = tracks_[ track ]->get_clip_index_at( value );
		ClipInfo lower_info;
		tracks_[ track ]->clip_info( lower_cut, &lower_info );
	
		// Get the visual at the start of the lower cut
		int visual_cut = tracks_[ 0 ]->get_clip_index_at( lower_info.start );
		ClipInfo visual_info;
		tracks_[ 0 ]->clip_info( visual_cut, &visual_info );

		switch ( align )
		{
			case 0:
				if ( visual_info.start < lower_info.start && 
					 tracks_[ track ]->is_blank_at( visual_info.start ) && 
					 lower_cut == tracks_[ track ]->get_clip_index_at( visual_info.start ) + 1 )
					move_to( track, lower_cut, visual_info.start );
				break;
			case 1:
				break;
			case 2:
				if ( visual_info.start < lower_info.start && 
					 tracks_[ track ]->is_blank_at( visual_info.start + visual_info.frame_count - 1 ) && 
					 lower_cut == tracks_[ track ]->get_clip_index_at( visual_info.start + visual_info.frame_count - 1 ) + 1 )
					move_to( track, lower_cut, visual_info.start + visual_info.frame_count - lower_info.frame_count - 1 );
				break;
		}
	}
}

void NLE_Project::match( )
{
	// Not sure how best to determine the correct track here (template dependent)
	int track = super_track( ) + 1;

	// Determine the current position
	int value = position( );

	// If we're on the first track and the determined track accepts video
	if ( current_track( ) == 0 && track != 0 && type( track ) & STORIES && value < length( track ) && value < length( 0 ) )
	{
		// Get the visual at the current position
		int visual_cut = tracks_[ 0 ]->get_clip_index_at( value );
		ClipInfo visual_info;
		tracks_[ 0 ]->clip_info( visual_cut, &visual_info );

		// Get the lower cut info
		int lower_cut = tracks_[ track ]->get_clip_index_at( value );
		ClipInfo lower_info;
		tracks_[ track ]->clip_info( lower_cut, &lower_info );

		// We need the absolute positions in the cuts at the current position
		int abs_visual = visual_info.frame_in + value - visual_info.start;
		int abs_lower = lower_info.frame_in + value - lower_info.start;

		// If we're same clip and the absolute points in the clip don't coincide here
		if ( lower_info.producer->same_clip( *visual_info.producer ) && abs_visual != abs_lower )
		{
			// Determine the difference at the current position
			int difference = abs_visual - abs_lower;

			// Determine the corrected in/out points for the visual
			int in = visual_info.frame_in - difference;
			int out = visual_info.frame_out - difference;

			// If in/out are valid then apply a resize on the cut accordingly
			if ( ( in >= 0 && in < visual_info.length ) && ( out >= 0 && out < visual_info.length ) && out > in )
				resize_cut( 0, visual_cut, in, out );
			else
				fl_beep( );
		}
		else
		{
			fl_beep( );
		}
	}
	else
	{
		fl_beep( );
	}
}

// UNBLOCKED UTILITY FUNCTIONS

void NLE_Project::remove_section( int track, int in, int out )
{
	// Split left and right respectively
	in = tracks_[ track ]->split_at( in, true );
	out = tracks_[ track ]->split_at( out, false );
	int l = tracks_[ track ]->get_clip_index_at( in );
	int r = tracks_[ track ]->get_clip_index_at( out );
	do
		tracks_[ track ]->remove( r -- );
	while( r >= l );
}

int NLE_Project::nearest_insert( int track, int position )
{
	// Determine the real insertion point of the project
	if ( position < length( track ) )
	{
		int i = tracks_[ track ]->get_clip_index_at( position );
		int s = tracks_[ track ]->clip_start( i );
		int l = tracks_[ track ]->clip_length( i );
		if ( position < s + l / 2 )
			position = s;
		else
			position = s + l;
	}
	else
	{
		// This is the position relative to the project length, not track
		position = position >= length( ) ? length( ) : position;
	}
	return position;
}

void NLE_Project::normalise_length( int track )
{
	if ( length( ) > length( track ) )
		tracks_[ track ]->blank( length( ) - length( track ) - 1 );
}
