/*
 * Panel_Super.h -- Super panel
 * Copyright (C) 2004-2005 Ushodaya Enterprises Limited
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "NLE.h"
#include "Panel_Super.h"
#include "AvCutter.h"
#include "AvUtils.h"
using namespace shotcut;

#include <FL/Fl.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Round_Button.H>
#include <FL/Fl_Multiline_Input.H>

#include <stdlib.h>

class NLE_Super : public NLE_Panel
{
	private:
		AvCutter *cutter;
		Fl_Round_Button *buttons[ 4 ];
		Fl_Input *inputs[ 2 ];
		Producer *super;
		Fl_Button *controls[ 3 ];
		int offset;
		bool changed;

	public:
		NLE_Super( int x, int y, int w, int h, const char *label ) :
			NLE_Panel( x, y, w, h, label ),
			super( NULL ),
			changed( false )
		{
			Fl_Group *inner = new Fl_Group( x, y + 15, w, h - 50 - 15 );
			Fl_Group *radios = new Fl_Group( x + 5, y + 15, w - 10, 55 + 10 );
			buttons[ 0 ] = new Fl_Round_Button( x + 10, y + 20, w - 20, 14, "N&one" );
			buttons[ 1 ] = new Fl_Round_Button( x + 10, y + 34, w - 20, 14, "Na&me && Designation" );
			buttons[ 2 ] = new Fl_Round_Button( x + 10, y + 48, w - 20, 14, "Single &Line" );
			buttons[ 3 ] = new Fl_Round_Button( x + 10, y + 62, w - 20, 14, "&Double" );
			for ( int i = 0; i < 4; i ++ )
			{
				buttons[ i ]->callback( static_type_cb, this );
				buttons[ i ]->labelsize( 12 );
				buttons[ i ]->clear_visible_focus( );
				buttons[ i ]->when( FL_WHEN_CHANGED | FL_WHEN_RELEASE );
			}
			radios->box( FL_ENGRAVED_BOX );
			radios->label( "Type" );
			radios->labelsize( 10 );
			radios->align( FL_ALIGN_LEFT | FL_ALIGN_TOP );
			radios->end( );
			Fl_Group *text = new Fl_Group( x + 5, y + 95, w - 10, 72 );
			inputs[ 0 ] = new Fl_Input( x + 10, y + 105 , w - 20, 24 );
			inputs[ 1 ] = new Fl_Input( x + 10, y + 105 + 28, w - 20, 24 );
			for ( int i = 0; i < 2; i ++ )
			{
				inputs[ i ]->callback( static_input_cb, this );
				inputs[ i ]->textfont( FL_TIMES );
				inputs[ i ]->textsize( 16 );
				inputs[ i ]->deactivate( );
				inputs[ i ]->when( FL_WHEN_CHANGED );
			}
			text->box( FL_ENGRAVED_BOX );
			text->label( "Transcription" );
			text->labelsize( 10 );
			text->align( FL_ALIGN_LEFT | FL_ALIGN_TOP );
			text->end( );
			controls[ 0 ] = new Fl_Button( x + 5, y + 95 + 72 + 5, 40, 20, "&Prev" );
			controls[ 1 ] = new Fl_Button( x + w / 2 - 20, y + 95 + 72 + 5, 40, 20, "&Split" );
			controls[ 2 ] = new Fl_Button( x + w - 45, y + 95 + 72 + 5, 40, 20, "&Next" );
			Fl_Group *pad = new Fl_Group( x + 5, y + 167, w - 10, h - 167 - 65 );
			pad->end( );
			for ( int i = 0; i < 3; i ++ )
			{
				controls[ i ]->callback( static_control_cb, this );
				controls[ i ]->labelsize( 10 );
				controls[ i ]->clear_visible_focus( );
			}
			inner->resizable( pad );
			inner->end( );
			cutter = new AvCutter( x, y + h - 50, w, 50 );
			cutter->set_values( 0, 0, 999, 1000 );
			end( );
			buttons[ 0 ]->value( true );
			cutter->hide( );
			cutter->locked( true );
			cutter->callback( static_cutter_cb, this );
			resizable( inner );
		}

		~NLE_Super( )
		{
			delete super;
		}

		static void static_type_cb( Fl_Widget *widget, void *object )
		{
			( ( NLE_Super * )object )->type_cb( ( Fl_Round_Button * )widget );
			if ( ( Fl_Round_Button * )widget != ( ( NLE_Super * )object )->buttons[ 0 ] )
				Fl::focus( ( ( NLE_Super * )object )->inputs[ 0 ] );
			NLE::Refresh_Multitrack( true );
		}

		void type_cb( Fl_Round_Button *button, bool with_update = true )
		{
			int index = 0;
			int start, in, out, length;

			for ( int i = 0; i < 4; i ++ )
			{
				if ( buttons[ i ] == button )
					index = i;
				buttons[ i ]->value( buttons[ i ] == button );
			}

			switch( index )
			{
				case 0:
					inputs[ 0 ]->deactivate( );
					inputs[ 1 ]->deactivate( );
					break;
				case 1:
				case 3:
					inputs[ 0 ]->activate( );
					inputs[ 1 ]->activate( );
					break;
				case 2:
					inputs[ 0 ]->activate( );
					inputs[ 1 ]->deactivate( );
					break;
			}

			for ( int i = 0; i < 2; i ++ )
				inputs[ i ]->value( "" );

			if ( NLE::Current_Mode( ) == project_mode )
			{
				// This isn't ideal, but we may as well remove the current
				// super object (if we have one)
				delete super;
				super = NULL;

				controls[ 0 ]->activate( );
				controls[ 1 ]->activate( );
				controls[ 2 ]->activate( );

				if ( with_update )
				{
					if ( index == 0 )
					{
						// If the user has selected None, then we remove the super object
						// at the current position
						NLE::Project( ).delete_super_at( );
						NLE::Project( ).sizeof_super_at( start, in, out, length );
						cutter->fixate( IN | OUT | DND | INSERT );
					}
					else
					{
						const char *font = NLE::Project( ).get( "nle_font_display" );
						font = font == NULL ? "Sans" : font;
						const char *size = NLE::Project( ).get( "nle_font_super_size" );
						size = size == NULL ? "32" : size;

						// Fetch or create a super and assign it the new type
						super = NLE::Project( ).fetch_super_at( index, start, in, out, length );
						NLE::Project( ).block( );
						super->set( "meta.attr.super", 1 );
						if ( super->get( "meta.attr.super.0" ) == NULL )
							super->set( "meta.attr.super.0", "" );
						if ( super->get( "meta.attr.super.1" ) == NULL || index == 2 )
							super->set( "meta.attr.super.1", "" );
						super->set( "meta.attr.super.align", ( char * )( index == 2 ? "centre" : "top" ) );
						super->set( "meta.attr.super.weight", index == 3 ? 400 : 700 );
						super->set( "meta.attr.super.f0", font );
						super->set( "meta.attr.super.f1", font );
						super->set( "meta.attr.super.s0", size );
						super->set( "meta.attr.super.s1", size );
						super->set( "meta.attr.super.final", 1 );
						NLE::Project( ).unblock( );
						cutter->fixate( DND | INSERT );
					}
				}
				else
				{
					super = NLE::Project( ).fetch_super_at( index, start, in, out, length );
					if ( super != NULL )
					{
						cutter->fixate( DND | INSERT );
					}
					else
					{
						NLE::Project( ).sizeof_super_at( start, in, out, length );
						cutter->fixate( IN | OUT | DND | INSERT );
					}
				}

				cutter->set_values( NLE::Project( ).position( ) - start, in - start, out - start, length );
				offset = start;
				cutter->show( );

				if ( super != NULL )
				{
					for ( int i = 0; i < 2; i ++ )
					{
						char temp[ 20 ];
						sprintf( temp, "meta.attr.super.%d", i );
						char *result = AvUtils::iconv_convert( super->get( temp ), "UTF-8", "iso8859-1" );
						inputs[ i ]->value( result != NULL ? result : "" );
						free( result );
					}
				}

				if ( in == start )
					controls[ 0 ]->deactivate( );
				if ( super == NULL )
					controls[ 1 ]->deactivate( );
				if ( out == start + length - 1 )
					controls[ 2 ]->deactivate( );

				NLE::Refresh_Preview( );
			}

			if ( with_update && NLE::Current_Mode( ) == project_mode )
			{

			}
			else if ( !with_update && NLE::Current_Mode( ) == project_mode )
			{
				if ( super != NULL )
				{
					cutter->set_values( NLE::Project( ).position( ) - start, in - start, out - start, length );
					offset = start;
					cutter->fixate( DND | INSERT );
					cutter->show( );
					for ( int i = 0; i < 2; i ++ )
					{
						char temp[ 20 ];
						sprintf( temp, "meta.attr.super.%d", i );
						char *result = AvUtils::iconv_convert( super->get( temp ), "UTF-8", "iso8859-1" );
						inputs[ i ]->value( result != NULL ? result : "" );
						free( result );
					}
				}
				else
				{
					NLE::Project( ).sizeof_super_at( start, in, out, length );
					cutter->set_values( NLE::Project( ).position( ) - start, in - start, out - start, length );
					offset = start;
					cutter->fixate( IN | OUT | DND | INSERT );
					cutter->show( );
					controls[ 1 ]->deactivate( );
				}
			}
		}

		void update( )
		{
			bool active = false;

			if ( NLE::Current_Mode( ) == project_mode )
			{
				// Fetch the project
				NLE_Project &project = NLE::Project( );

				// Get the current super track index
				int super_track = project.super_track( );

				if ( super_track != -1 && project.length( ) > 0 )
				{
					active = true;
					int t_start, t_in, t_out, t_length;
					NLE::Project( ).sizeof_super_at( t_start, t_in, t_out, t_length );
					type_cb( buttons[ project.typeof_super_at( ) ], false );
				}
			}

			// If not active, deactivate everything
			if ( !active )
			{
				deactivate( );
				cutter->hide( );
				delete super;
				super = NULL;
			}
		}

		void set_position( int position )
		{
			if ( cutter->visible( ) && position >= offset && position < offset + cutter->length( ) )
				cutter->position( position - offset );
		}

		static void static_cutter_cb( Fl_Widget *widget, void *obj )
		{
			( ( NLE_Super * )obj )->cutter_cb( );
		}

		void cutter_cb( )
		{
			int in = 0;
			int out = cutter->out( ) - cutter->in( );
			int position;

			switch( cutter->action( ) )
			{
				case NONE:
					cutter->linked( false );
					if ( changed )
					{
						NLE::Ignore_Changes( false );
						NLE::Check_Point( );
						changed = false;
					}
					break;

				case IN:
					changed = true;
					NLE::Ignore_Changes( true );
					cutter->linked( true );
					position = NLE::Project( ).move_super_to( super, offset + cutter->in( ) );
					NLE::Change_Position( position, panel_position, true );
					NLE::Refresh_Multitrack( true );
					break;

				case OUT:
					changed = true;
					cutter->linked( false );
					NLE::Ignore_Changes( true );
					NLE::Resize_Cut( super, in, out );
					cutter->out( cutter->in( ) + out );
					cutter->position( cutter->in( ) + out );
					NLE::Change_Position( offset + cutter->out( ), panel_position, true );
					break;

				case POSITION:
					cutter->linked( false );
					NLE::Change_Position( offset + cutter->position( ), panel_position, true );
					break;

				default:
					cutter->linked( false );
					break;
			}
		}

		static void static_input_cb( Fl_Widget *widget, void *obj )
		{
			( ( NLE_Super * )obj )->input_cb( ( Fl_Input * )widget );
		}

		void input_cb( Fl_Input *input )
		{
			int i = 0;
			for ( i = 0; i < 2; i ++ )
				if ( inputs[ i ] == input )
					break;
			NLE::Ignore_Changes( true );
			dirty( true );
			NLE::Project( ).block( );
			if ( i < 2 )
			{
				char temp[ 20 ];
				sprintf( temp, "meta.attr.super.%d", i );
				char *result = AvUtils::iconv_convert( ( char * )input->value( ), "iso8859-1", "UTF-8" );
				super->set( temp, result );
				free( result );
			}
			NLE::Project( ).unblock( );
			NLE::Refresh_Preview( );
		}

		static void static_control_cb( Fl_Widget *widget, void *obj )
		{
			( ( NLE_Super * )obj )->control_cb( ( Fl_Button * )widget );
		}

		void control_cb( Fl_Button *button )
		{
			NLE_Project &project = NLE::Project( );
			int position = project.position( );
			Playlist &super_track = project.track( project.super_track( ) );
			int index = project.track( project.super_track( ) ).get_clip_index_at( position );
			int seek_to = -1;

			int i = 0;
			for ( i = 0; i < 3; i ++ )
				if ( controls[ i ] == button )
					break;

			switch( i )
			{
				case 0:
					if ( index > 0 )
						seek_to = index - 1;
					break;
				case 1:
					project.block( );
					super_track.split_at( position );
					project.unblock( );
					NLE::Change_Mode( project_mode, true );
					break;
				case 2:
					seek_to = index + 1;
					break;
			}

			if ( seek_to != -1 )
			{
				int cut_idx = project.track( 0 ).get_clip_index_at( position );
				int cut_start = project.track( 0 ).clip_start( cut_idx );
				int cut_end = cut_start + project.track( 0 ).clip_length( cut_idx ) - 1;
				int start = super_track.clip_start( seek_to );
				if ( start >= cut_start && start <= cut_end )
					NLE::Change_Position( start, player_position, true );
				else if ( start < cut_start )
					NLE::Change_Position( offset, player_position, true );
			}
		}
};

NLE_Panel *shotcut::Panel_Super( int x, int y, int w, int h )
{
	return new NLE_Super( x, y, w, h, "Super" );
}


