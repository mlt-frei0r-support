/**
 * NLE_Browser.h - Provides the NLE browser component
 * Copyright (C) 2004-2005 Charles Yates
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _NLE_BROWSER_H_
#define _NLE_BROWSER_H_

#include <FL/Fl_Browser_.H>
#include <mlt++/Mlt.h>
using namespace Mlt;

#include "NLE.h"
using namespace shotcut;

namespace shotcut
{
	class NLE_Browser : public Fl_Browser_
	{
		private:
			NLE_Producer *first;
			NLE_Producer *last;
			int lines;
			int showing;
			bool active;
	
		public:
			NLE_Browser( int x, int y, int w, int h );
			~NLE_Browser( );
			void selection_cb( );
			int handle( int e );
			void set_selection( Producer *producer );
			void clear( );
			void add( char *title, char *document );
			void insert( int position, char *title, char *document );
			void insert( int position, NLE_Producer *p );
			NLE_Producer *find( int position );
			void clean( );
			void display( int flags );
			void refresh( Tractor *tractor );
			void refresh( Playlist *playlist );
			void *selection( ) { return Fl_Browser_::selection( ); }

		protected:
			// Refresh an individual producer
			void refresh( Producer &producer );
			virtual void item_draw(void *p,int x,int y,int width,int height) const;
			/** This method must be provided by the subclass to return the first item in the list. */
			virtual void *item_first() const;
			virtual void *item_last() const;
			/** This method must be provided by the subclass to return the item in the list after \c p. */
			virtual void *item_next(void *p) const;
			/** This method must be provided by the subclass to return the item in the list before \c p. */
			virtual void *item_prev(void *p) const;
			/** This method must be provided by the subclass to return the height of the item \c p
			* in pixels. Allow for two additional pixels for the list selection box. */
			virtual int item_height(void *p) const;
			/** This method must be provided by the subclass to return the width of the item \c p in 
			* pixels. Allow for two additional pixels for the list selection box. */
			virtual int item_width(void *p) const;
			/** This method may be provided by the subclass to return the height of the item \c p 
			* in pixels. Allow for two additional pixels for the list selection box. This method 
			* differs from item_height in that it is only called for selection and scrolling 
			* operations. The default implementation calls item_height. */
			//virtual int item_quick_height(void *p) const { return 32; }
	
			/** This method must be provided by the subclass to draw the item p in the area 
			* indicated by x, y, w, and h. */
			// you don't have to provide these but it may help speed it up:
			/** This method may be provided by the subclass to indicate the full width of the item 
			* list in pixels. The default implementation computes the full width from the
			* item widths. */
			//virtual int full_width() const { }

			/** This method may be provided by the subclass to indicate the full height of the item 
			* list in pixels. The default implementation computes the full height from the 
			* item heights. */
			//virtual int full_height() const { }
	
			/** This method may be provided to return the average height of all items, to be used 
			* for scrolling. The default implementation uses the height of the first item. */
			//virtual int incr_height() const { }
	};
}

#endif
