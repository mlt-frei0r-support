
#include "config.h"

#include "AvPlayer.h"
using namespace shotcut;

#include <FL/Fl.H>
#include <FL/Fl_Single_Window.H>
#include <FL/Fl_Widget.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Value_Slider.H>
#include <FL/Fl_XPM_Image.H>
#include <string>
#include <math.h>
#include <ctype.h>
using namespace std;

class PreviewWindow : public Fl_Single_Window
{
	private:
		AvPlayer *consumer;

	public:
		PreviewWindow( AvPlayer *consumer_, int x, int y, int w, int h ) :
			Fl_Single_Window( x, y, w, h ),
			consumer( consumer_ )
		{
		}

		void draw( ) 
		{
			consumer->restart( );
		}
};

AvPlayer::AvPlayer( Properties &properties, Fl_Slider *slider_, bool sticky, int x, int y, int w, int h ) :
	AvWindow( x, y, w, h ),
	slider( NULL ),
	speed( 1.0 ),
	event( NULL ),
	consumer( NULL ),
	current( NULL ),
	position( 0 ),
	_move( NULL ),
	_move_obj( NULL ),
	_locked( false ),
	sticky_( true ),
	step( 0 ),
	previous_speed( 0 )
{
	pthread_mutex_init( &mutex, NULL );
	initialise( properties, slider_, sticky );
}

void AvPlayer::initialise( Properties &properties, Fl_Slider *slider_, bool sticky )
{
	// This is needed because we're positioning relative to a window 
	// [x,y logic retained in case of change to group later]
	int x = 0;
	int y = 0;
	int w = this->w( );
	int h = this->h( );

	slider = slider_;

	// Button width and x coord
	int bw = 18;
	int bx = w - bw * 7;
	if ( slider != NULL )
		bx /= 2;

	// Have an inner group for the buttons
	group = new Fl_Group( x, y + h - 18, w, 18 );
	buttons[ 0 ] = new Fl_Button( ( int )( x + bx ), y + h - 18, ( int )bw, 18 );
	buttons[ 1 ] = new Fl_Button( ( int )( x + bx + bw ), y + h - 18, ( int )bw, 18 ); buttons[ 1 ]->when( FL_WHEN_CHANGED );
	buttons[ 2 ] = new Fl_Button( ( int )( x + bx + bw * 2 ), y + h - 18, ( int )bw, 18 ); buttons[ 2 ]->when( FL_WHEN_CHANGED );
	buttons[ 3 ] = new Fl_Button( ( int )( x + bx + bw * 3 ), y + h - 18, ( int )bw, 18 );
	buttons[ 4 ] = new Fl_Button( ( int )( x + bx + bw * 3 ), y + h - 18, ( int )bw, 18 );
	buttons[ 5 ] = new Fl_Button( ( int )( x + bx + bw * 4 ), y + h - 18, ( int )bw, 18 ); buttons[ 5 ]->when( FL_WHEN_CHANGED );
	buttons[ 6 ] = new Fl_Button( ( int )( x + bx + bw * 5 ), y + h - 18, ( int )bw, 18 ); buttons[ 6 ]->when( FL_WHEN_CHANGED );
	buttons[ 7 ] = new Fl_Button( ( int )( x + bx + bw * 6 ), y + h - 18, ( int )bw, 18 );

	images[ 0 ] = new Fl_XPM_Image( SHOTCUT_SHARE "pixmaps/editor_movie_i.xpm" ); buttons[ 0 ]->image( images[ 0 ] );
	images[ 1 ] = new Fl_XPM_Image( SHOTCUT_SHARE "pixmaps/editor_fast_i.xpm" ); buttons[ 1 ]->image( images[ 1 ] );
	images[ 2 ] = new Fl_XPM_Image( SHOTCUT_SHARE "pixmaps/editor_step_i.xpm" ); buttons[ 2 ]->image( images[ 2 ] );
	images[ 3 ] = new Fl_XPM_Image( SHOTCUT_SHARE "pixmaps/editor_pause.xpm" ); buttons[ 3 ]->image( images[ 3 ] );
	images[ 4 ] = new Fl_XPM_Image( SHOTCUT_SHARE "pixmaps/editor_play.xpm" ); buttons[ 4 ]->image( images[ 4 ] );
	images[ 5 ] = new Fl_XPM_Image( SHOTCUT_SHARE "pixmaps/editor_step.xpm" ); buttons[ 5 ]->image( images[ 5 ] );
	images[ 6 ] = new Fl_XPM_Image( SHOTCUT_SHARE "pixmaps/editor_fast.xpm" ); buttons[ 6 ]->image( images[ 6 ] );
	images[ 7 ] = new Fl_XPM_Image( SHOTCUT_SHARE "pixmaps/editor_movie.xpm" ); buttons[ 7 ]->image( images[ 7 ] );

	for ( int i = 0; i < 8; i ++ )
	{
		buttons[ i ]->callback( button_callback, this );
		buttons[ i ]->labelsize( 0 );
		buttons[ i ]->clear_visible_focus( );
		buttons[ i ]->box( FL_FLAT_BOX );
		buttons[ i ]->down_box( FL_DOWN_FRAME );
	}

	if ( slider == NULL )
	{
		slider = new Fl_Value_Slider( x, h - 18, w - 126, 18 );
		slider->type( FL_HOR_NICE_SLIDER );
		slider->slider_size( 0.02 );
		group->resizable( slider );
		sticky_ = sticky;
	}
	else
	{
		Fl_Box *box = new Fl_Box( x, h - 18, bx, 18 );
		box->box( FL_FLAT_BOX );
		group->resizable( box );
	}

	group->end( );

	slider->bounds( 0, 100 );
	slider->callback( slider_callback, this );
	slider->labelsize(0);
	slider->precision(0);
	slider->clear_visible_focus( );
	slider->selection_color( FL_BLUE );

	consumer = new Consumer( properties.get( "preview" ) );
	consumer->pass_values( properties, "preview." );
	event = consumer->listen( "consumer-frame-show", this, ( mlt_listener )consumer_frame_show );
	consumer->set( "app_locked", 1 );
	consumer->set( "app_lock", ( void * )Fl::lock, 0 );
	consumer->set( "app_unlock", ( void * )Fl::unlock, 0 );

	// Create the preview window
	window = new PreviewWindow( this, x, y, w, h - 18 );
	window->end( );

	// The main group resizes the window
	resizable( window );

	// End the window
	end( );

	if ( !sticky )
		clear_visible_focus( );
}

AvPlayer::~AvPlayer( )
{
	delete event;
	if ( consumer )
		consumer->stop( );
	delete consumer;
	delete current;
	pthread_mutex_destroy( &mutex );
	for ( int i = 0; consumer && i < 8; i ++ )
		delete images[ i ];
}

// These are the same as Current_Mode == project_mode or preview_mode
void AvPlayer::locked( bool value )
{
	_locked = value;
}

bool AvPlayer::locked( )
{
	return _locked;
}

Window AvPlayer::xid( )
{
	return this->window->shown( ) && this->window->visible( ) ? fl_xid( this->window ) : 0;
}

void AvPlayer::indicate_speed( double speed )
{
	if ( this->speed != speed )
	{
		this->speed = speed;
		if ( speed != 1 )
		{
			buttons[ 3 ]->hide( );
			buttons[ 4 ]->show( );
		}
		else
		{
			buttons[ 4 ]->hide( );
			buttons[ 3 ]->show( );
		}
	}
}

void AvPlayer::button_press( Fl_Button *button )
{
	int i = 0;
	for ( i = 0; i < 7; i ++ )
		if ( button == buttons[ i ] )
			break;
	switch( i )
	{
		case 0:
			button_press( "start" );
			break;
		case 1:
			button_press( "rew" );
			break;
		case 2:
			button_press( "prev" );
			break;
		case 3:
		case 4:
			button_press( "play" );
			break;
		case 5:
			button_press( "next" );
			break;
		case 6:
			button_press( "ffwd" );
			break;
		case 7:
			button_press( "end" );
			break;
	}
}

void AvPlayer::position_change( int value )
{
	if ( current != NULL )
	{
		// This is a move request
		if ( _move != NULL )
		{
			_move( _move_obj, value, true );
		}
		else
		{
			current->lock( );
			current->seek( value );
			current->unlock( );
		}
		refresh( );
	}
}

bool AvPlayer::restart( )
{
	bool ret = consumer->is_stopped( ) && shown( ) && visible( ) && xid( ) != 0;
	if ( ret )
	{
		char temp[ 132 ];
		sprintf( temp, "%d", ( int )xid( ) );
		if ( setenv( "SDL_WINDOWID", temp, 1 ) == 0 )
			consumer->start( );
	}
	if ( !consumer->is_stopped( ) )
		refresh( );
	return ret;
}

void AvPlayer::stop( )
{
	if ( consumer )
		consumer->stop( );
}

int AvPlayer::handle( int e )
{
	int ret = 1;
	switch( e )
	{
		case FL_HIDE:
			if ( current )
				set_speed( 0 );
			break;
		case FL_DND_ENTER:
		case FL_DND_DRAG:
		case FL_DND_LEAVE:
		case FL_DND_RELEASE:
		case FL_FOCUS:
			refresh( );
			if ( sticky_ == false )
				return 0;
			break;
		case FL_PUSH:
			refresh( );
			ret = 0;
			break;
		case FL_ENTER:
			break;
		case FL_PASTE:
			paste( Fl::event_text( ) );
			break;
		default:
			ret = 0;
			break;
	}
	return ret == 0 ? AvWindow::handle( e ) : ret;
}

int tohex( char p )
{
	return isdigit( p ) ? p - '0' : tolower( p ) - 'a' + 10;
}

char *url_decode( char *dest, char *src )
{
	char *p = dest;
	while ( *src )
	{
		if ( *src == '%' )
		{
			*p ++ = ( tohex( *( src + 1 ) ) << 4 ) | tohex( *( src + 2 ) );
			src += 3;
		}
		else if ( *src == '+' )
		{
			*p ++ = ' ';
			src ++;
		}
		else
		{
			*p ++ = *src ++;
		}
	}
	*p = *src;
	return dest;
}

void AvPlayer::paste( const char *text )
{
	char *title = NULL;
	Playlist playlist;
	Tokeniser tokens( ( char * )text, "\n" );
	for ( int i = 0; i < tokens.count( ) - 1; i ++ )
	{
		Tokeniser split( tokens.get( i ), "\r" );
		if ( !strncmp( split.get( 0 ), "file:/", 6 ) )
		{
			char temp[ 1024 ];
			url_decode( temp, split.get( 0 ) + 6 );
			Producer producer( temp );
			if ( producer.is_valid( ) )
				playlist.append( producer );
			if ( title == NULL )
			{
				if ( strchr( temp, '/' ) )
					title = strdup( strrchr( temp, '/' ) + 1 );
				else
					title = strdup( temp );
			}
		}
	}

	if ( playlist.get_out( ) > 0 )
	{
		playlist.set( "title", title );
		play( playlist );
		free( title );
	}
}

Producer *AvPlayer::get_current( )
{
	return current;
}

void AvPlayer::play( Producer &producer )
{
	pthread_mutex_lock( &mutex );
	Producer *previous = current;
	current = new Producer( producer );
	if ( previous != NULL && sticky_ &&
		 current->get( "title" ) != NULL && 
		 previous->get( "title" ) != NULL &&
		 !strcmp( current->get( "title" ), previous->get( "title" ) ) )
	{
		current->set_speed( previous->get_speed( ) );
		current->seek( previous->position( ) );
	}
	consumer->lock( );
	consumer->connect( *current );
	mlt_position in = current->get_in( );
	mlt_position out = current->get_out( );
	if ( locked( ) )
		slider->bounds( in, out );
	consumer->unlock( );
	if ( !consumer->is_stopped( ) )
		restart( );
	delete previous;
	pthread_mutex_unlock( &mutex );
}

void AvPlayer::play( char *clip )
{
	Producer producer( clip );
	play( producer );
}

void AvPlayer::set_speed( double speed )
{
	pthread_mutex_lock( &mutex );
	if ( current != NULL && current->get_speed( ) != speed )
	{
		current->lock( );
		position += 2 * ( int )current->get_speed( );
		current->seek( position );
		current->set_speed( speed );
		speed = current->get_speed( );
		current->unlock( );
		if ( _move != NULL )
			_move( _move_obj, position, false );
		else if ( Fl::pushed( ) != slider && _locked )
			slider->value( position );
	}

	refresh( );
	pthread_mutex_unlock( &mutex );
}

void AvPlayer::set_moved( void ( *move )( void *, int, bool ), void *obj )
{
	_move_obj = obj;
	_move = move;
}

void AvPlayer::frame_shown( Frame &frame )
{
	pthread_mutex_lock( &mutex );
	if ( current != NULL )
	{
		position = frame.get_int( "_position" );
		pthread_mutex_unlock( &mutex );
		// Only refresh the ui when speed != 0
		// When speed == 0, it's the apps responsibility
		if ( speed != 0 )
		{
			Fl::lock( );
			// All positions changes are handled via the callback when available
			if ( _move != NULL )
				_move( _move_obj, position, false );
			else if ( Fl::pushed( ) != slider && _locked )
				slider->value( position );
			Fl::check( );
			Fl::unlock( );
		}
		speed = frame.get_double( "_speed" );
	}
	else
	{
		pthread_mutex_unlock( &mutex );
	}
}

void AvPlayer::seek( int pos )
{
	if ( current != NULL )
	{
		current->lock( );
		current->seek( pos < 0 ? 0 : pos > current->get_out( ) ? current->get_out( ) : pos );
		current->unlock( );
	}
	refresh( );
}

void AvPlayer::seek_relative( int pos )
{
	if ( current != NULL )
	{
		current->lock( );
		current->seek( position + pos < 0 ? 0 : position + pos > current->get_out( ) ? current->get_out( ) : position + pos );
		current->unlock( );
		if ( _move != NULL )
			_move( _move_obj, position, false );
		else if ( Fl::pushed( ) != slider && _locked )
			slider->value( position );
	}
	refresh( );
}

void AvPlayer::static_button_timeout( void *obj )
{
	AvPlayer *player = ( AvPlayer * )obj;
	player->button_timeout( );
}

void AvPlayer::button_timeout( )
{
	if ( step != 0 )
		set_speed( step );
}

void AvPlayer::button_press( string id )
{
	if ( current != NULL )
	{
		if ( id == "start" )
		{
			handle_start_and_end( id );
		}
		else if ( id == "play" )
		{
			set_speed( current->get_speed( ) == 1 ? 0 : 1 );
		}
		else if ( id == "end" )
		{
			handle_start_and_end( id );
		}
		else 
		{
			int on_speed = 1;
			int off_speed = 0;

			if ( id == "prev" )
			{
				on_speed = -1;
				off_speed = previous_speed;
			}
			else if ( id == "next" )
			{
				on_speed = 1;
				off_speed = 0;
			}
			else if ( id == "rew" )
			{
				on_speed = -25;
				off_speed = previous_speed;
			}
			else if ( id == "ffwd" )
			{
				on_speed = 25;
				off_speed = previous_speed;
			}

			if ( step == 0 )
			{
				previous_speed = ( int )current->get_speed( );
				set_speed( 0 );
				step = on_speed;
				seek_relative( on_speed );
				Fl::add_timeout( 0.3, static_button_timeout, this );
			}
			else
			{
				step = 0;
				Fl::remove_timeout( static_button_timeout, this );
				set_speed( off_speed );
			}
		}
		indicate_speed( current->get_speed( ) );
	}
}

void AvPlayer::handle_start_and_end( string id )
{
	Filter *first = current->filter( 0 );

	if ( first == NULL && current->type( ) == playlist_type )
	{
		Playlist playlist( *current );
		int position = current->position( );
		int index = playlist.get_clip_index_at( position );
		if ( id == "start" )
		{
			if ( index > 0 ) index --;
			ClipInfo *info = playlist.clip_info( index );
			position_change( info->start );
			delete info;
		}
		else
		{
			if ( index < playlist.count( ) - 1 ) 
			{
				ClipInfo *info = playlist.clip_info( index + 1 );
				position_change( info->start );
				delete info;
			}
			else
			{
				position_change( current->get_out( ) );
			}
		}
	}
	else if ( current->type( ) == tractor_type )
	{
		Tractor tractor( *current );
		int position = current->position( );
		ClipInfo info;
		int seek_to = id == "start" ? 0 : current->get_out( );
		for ( int i = 0; i < tractor.count( ); i ++ )
		{
			Producer *producer = tractor.track( i );
			Playlist track( *producer );
			int index = track.get_clip_index_at( position );
			if ( id == "start" && index >= 0 && track.count( ) > 0 )
			{
				int grace = ( int )( 25 * current->get_speed( ) );
				if ( index >= track.count( ) ) index = track.count( ) - 1;
				track.clip_info( index, &info );
				if ( position > info.start + grace )
				{
					seek_to = ( position - info.start ) < ( position - seek_to ) ? info.start : seek_to;
				}
				else if ( position > info.start + info.frame_count + grace )
				{
					seek_to = ( position - info.start + info.frame_count ) < ( position - seek_to ) ? info.start + info.frame_count : seek_to;
				}
				else if ( index > 0 )
				{
					track.clip_info( index - 1, &info );
					seek_to = ( position - info.start ) < ( position - seek_to ) ? info.start : seek_to;
				}
			}
			else if ( id == "end" && index >= 0 && index < track.count( ) )
			{
				if ( index < track.count( ) - 1 )
				{
					track.clip_info( index + 1, &info );
					seek_to = ( info.start - position ) < ( seek_to - position ) ? info.start : seek_to;
				}
				else
				{
					track.clip_info( index, &info );
					seek_to = ( info.start + info.frame_count - position ) < ( seek_to - position ) ? info.start + info.frame_count - 1: seek_to;
				}
			}
		}

		position_change( seek_to );
	}
	else
	{
		position_change( id == "start" ? current->get_in( ) : current->get_out( ) );
	}

	delete first;
}

void AvPlayer::slider_callback( Fl_Widget *widget, void *object )
{
	Fl_Valuator *slider = ( Fl_Valuator * )widget;
	( ( AvPlayer * )object )->position_change( ( int )( slider->value( ) ) );
}

void AvPlayer::button_callback( Fl_Widget *widget, void *object )
{
	( ( AvPlayer * )object )->button_press( ( Fl_Button * )widget );
}

void AvPlayer::consumer_frame_show( mlt_consumer sdl, AvPlayer *self, mlt_frame frame_ptr )
{
	Frame frame( frame_ptr );
	self->frame_shown( frame );
}

void AvPlayer::refresh( )
{
	if ( consumer )
	{
		consumer->lock( );
		consumer->set( "refresh", 1 );
		consumer->unlock( );
	}
}
