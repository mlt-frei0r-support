/*
 * shotcut-inject.cpp -- A small inject client
 * Copyright (C) 2004-2004 Ushodaya Enterprises Limited
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <mlt++/Mlt.h>
using namespace Mlt;

char *load_file( char **title, char *file )
{
	char *buffer = NULL;
	char extra[ 512 ] = "";

	// Quick and dirty title assignment for now
	if ( strchr( file, '/' ) )
		*title = strdup( strrchr( file, '/' ) + 1 );
	else
		*title = strdup( file );

	// Ugly check for image files...
	if ( strstr( *title, ".png" ) || strstr( *title, ".jpg" ) || strstr( *title, ".tga" ) || strstr( *title, ".jpeg" ) )
		sprintf( extra, "out=\"249\" length=\"250\"" );

	if ( strstr( file, ".westley" ) )
	{
		FILE *f = fopen( file, "r" );
		if ( f != NULL )
		{
			fseek( f, 0, SEEK_END );
			int length = ftell( f );
			if ( length > 0 )
			{
				fseek( f, 0, SEEK_SET );
				buffer = ( char * )calloc( 1, length + 1 );
				if ( buffer != NULL )
					fread( buffer, length, 1, f );
				fclose( f );
			}
		}
	}
	else
	{
		char path[ 512 ];
		const char *temp = "<?xml version=\"1.0\"?>\n"
						   "<westley root=\"%s\" title=\"%s\">\n"
						   "  <producer resource=\"%s\" %s/>\n"
						   "</westley>\n";
		getcwd( path, 512 );
		buffer = ( char * )malloc( strlen( temp ) + strlen( file ) + strlen( *title ) + strlen( path ) + strlen( extra ) + 1 );
		if ( buffer != NULL )
			sprintf( buffer, temp, path, *title, file, extra );
	}
	return buffer;
}

int main( int argc, char **argv )
{
	Consumer *c = NULL;
	int ret = 0;
	char *server = "localhost";
	int port = 5250;
	int i;
	int init = 0;

	for ( i = 1; ret == 0 && i < argc; i ++ )
	{
		if ( *argv[ i ] == '-' )
		{
			if ( !strncmp( argv[ i ], "--server=", 9 ) )
				server = argv[ i ] + 9;
			else if ( !strncmp( argv[ i ], "--port=", 7 ) )
				port = atoi( argv[ i ] + 7 );
			else if ( !strcmp( argv[ i ], "--fork" ) )
			{
				if ( fork( ) )
					return 0;
			}
			else
				ret = 1;
		}
		else
		{
			char *title = NULL;
			char *contents = load_file( &title, argv[ i ] );

			if ( init == 0 )
			{
				Factory::init( );
				init = 1;
			}

			if ( contents != NULL )
			{
				char temp[ 1024 ];
	
				// This is a little incorrect - we should be specifying the title
				// used in the westley itself, but it will do for now...
				if ( strstr( argv[ i ], ".westley" ) )
					sprintf( temp, "file=\"%s\" title=\"%s\"", argv[ i ], title );
				else
					sprintf( temp, "title=\"%s\"", title );
	
				// Start a connection to the requested server and send the document
				if ( c == NULL || strcmp( c->get( "server" ), server ) || c->get_int( "port" ) != port )
				{
					delete c;
					c = new Consumer( "valerie" );
					c->set( "server", server );
					c->set( "port", port );
				}

				c->set( "westley", contents );
				c->set( "command", temp );
				c->run( );

				// Check if the document was sent
				ret = c->get_int( "_error" ) != 0 ? 2 : 0;
			}
			else
			{
				ret = 3;
			}

			// Free the contents of the document
			free( title );
			free( contents );
		}
	}

	delete c;

	switch( ret )
	{
		case 0:
			break;
		case 1:
			fprintf( stderr, "Invalid switch %s\n", argv[ i - 1 ] );
			break;
		case 2:
			fprintf( stderr, "Unable to send %s to %s:%d\n", argv[ i - 1 ], server, port );
			break;
		case 3:
			fprintf( stderr, "Invalid file %s\n", argv[ i - 1 ] );
			break;
	}

	return ret;
}
