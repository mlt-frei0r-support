/*
 * NLE_Types.h -- General types used throughout the project
 * Copyright (C) 2004-2005 Ushodaya Enterprises Limited
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _NLE_TYPES_H_
#define _NLE_TYPES_H_

#include <mlt++/Mlt.h>
using namespace Mlt;

namespace shotcut
{
	// Linked list for items in the browser.
	struct NLE_Producer
	{
		NLE_Producer *next;
		NLE_Producer *prev;
		Producer *producer;
		char *title;
		char *document;
		int hidden;
	};

	// The modes that application can be in
	typedef enum nle_mode
	{
		disable_mode = 0,
		browser_mode = 1,
		project_mode = 2,
		preview_mode = 4
	};

	// The types of clips and tracks
	typedef enum
	{
		INVALID = 0,
		STORIES = 1,
		AUDIO = 2,
		STILLS = 4,
		TEXT = 8,
		VOICE = 16,
		PROJECTS = 32
	}
	BrowserShow;

	// Identifier of what is changing position
	typedef enum
	{
		cutter_position,
		multitrack_position,
		player_position,
		panel_position
	}
	PositionChanger;
}

#endif
