/*
 * NLE_Project.h -- NLE Project class
 * Copyright (C) 2004-2005 Ushodaya Enterprises Limited
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _NLE_PROJECT_
#define _NLE_PROJECT_

#include <mlt++/Mlt.h>
using namespace Mlt;

#include "NLE_Types.h"

namespace shotcut
{
	/**
	 * The NLE_Project class provides methods for all updates and queries
	 * on the project tractor. It doesn't interact with the UI itself other
	 * than via the listener (when registered).
	 *
	 * NB: All non const objects returned as pointers must be deleted by the 
	 * caller.
	 */

	class NLE_Project : public Tractor
	{
		private:
			Tractor *tractor;
			Playlist **tracks_;
			bool valid_;

		public:
			// Constructor from a string - can be a westley file or a previously 
			// serialised westley document)
			NLE_Project( char *id, char *resource = NULL );
			// Constructor for the Project class
			NLE_Project( Tractor *tractor_ );
			// Constructor for the Project class
			NLE_Project( Tractor &tractor_ );
			// Destructor for the Project class
			~NLE_Project( );
			// Returns true if this is a valid nle project
			bool valid_project( );
			// Block event firing to allow multiple updates
			void block( );
			// Unblock event firing and optionally fire a changed event
			void unblock( bool changed = true );
			// Get current track index
			int current_track( );
			// Set the current track index
			void current_track( int track );
			// Get the super track (returns -1 if it doesn't exist)
			int super_track( );
			// Get the current position in the project
			int position( );
			// Return the track at the index - invalid index (< 0 or >= tracks) return the active track
			Playlist &track( int track = -1 );
			// Turn transitions off or on
			void transitions( bool value );
			// Determine if transitions are on or off
			bool transitions( );
			// Get the attributes associated to the track
			const char *attributes( int track = -1 );
			// Turn size locking off or on
			void lock_tracks( bool value );
			// Retrieve track locking
			bool lock_tracks( );
			// Apply transitions to a track
			void apply_transitions( int track = 0 );
			// Remove transitions from a track
			void remove_transitions( int track, int cut );
			// Prepare all the blanks for cutting
			void prepare_cuts( );
			// Returns the total length of the project
			int length( );
			// Returns the number of tracks
			int tracks( );
			// Returns the length of a specific track
			int length( int track );
			// Returns the number of cuts on the track
			int count( int track );
			// Returns true if the cut indicated is blank
			bool is_blank( int track, int cut );
			// Returns true if the cut at the position indicated is blank (-1 == active track/position)
			bool is_blank_at( int track = -1, int position = -1 );
			// Returns the cut on the track and cut index provided (-1 == active track/position)
			Producer *get_cut( int track = -1, int cut = -1 );
			// Returns the cut at the position and track (-1 = active track)
			Producer *get_cut_at( int position, int track = -1 );
			// Locate the cut and return true if it exists
			bool locate_cut( int &track, int &cut, Producer *producer );
			// Returns the index of the cut at the position
			int cut_index_at( int track, int position );
			// Returns the type of a specific track
			BrowserShow type( int track );
			// Returns the type of a producer
			BrowserShow type( Producer *producer );
			// Returns true if the track accepts the producer
			bool accepts( int track, Producer *producer );
			// Returns the cut index at the absolute position
			int cut_index( int track, int position );
			// Returns the start position of the producer's place in the track or -1
			int insert_at( int track, int position, Producer *producer );
			// Returns the new position after the delete or -1
			int delete_cut( int track, int cut_idx );
			// Split the cut at the absolute position and return the first frame of the 
			// right hand cut or -1 if none occurred
			int split_at( int track, int position );
			// Returns the cut index if the cut is movable or -1
			int movable( int track, int position );
			// Moves the cut to the position or near there [depending on track rules]
			// Returns the real start position used or -1 on failure
			int move_to( int track, int cut, int position );
			// Resize a cut in the project and seek to position if not -1
			int resize_cut( int track, int cut, int &in, int &out, int position = -1 );
			// Forwards the drop object at the given position
			int forward_drop_object_at( int track, int position );
			// Get info about the current cut, returns true if trimmable
			Producer *cut_info( int &offset, int &in, int &out, int &length, int &flags );
			// Delete the super at the specified position
			void delete_super_at( int position = -1 );
			// Fetch or create a super at the specified position
			Producer *fetch_super_at( int type, int &start, int &in, int &out, int &len, int position = -1 );
			bool sizeof_super_at( int &start, int &in, int &out, int &len, int position = -1 );
			// Return the type of the super the specified position
			int typeof_super_at( int position = -1 );
			// Move super to
			int move_super_to( Producer *super, int position );
			// Convert to and from an sb
			bool is_sb( int cut = -1 );
			void convert_sb( bool value, int cut = -1 );
			// Refresh attributes
			void refresh_text( bool lock = false );
			// Insert blanks into the current track
			int insert_blanks( int length );
			// Snap the current cut on the current track to the start of overlapping visual
			void snap( int align = 0 );
			// Match the visual to the audio on the voice over 
			void match( );

		protected:
			// Common initialisation for constructors
			void initialise( );
			// Remove section from a track
			void remove_section( int track, int in, int out );
			// Calculate nearest insertion without a split
			int nearest_insert( int track, int position );
			// Normalise the length of the track to that of the project
			void normalise_length( int track );
	};
}

#endif
