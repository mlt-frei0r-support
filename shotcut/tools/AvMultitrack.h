/*
 * AvMultitrack.h -- NLE Multitrack class
 * Copyright (C) 2004-2005 Ushodaya Enterprises Limited
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _FL_MULTITRACK_H
#define _FL_MULTITRACK_H

#include <FL/Fl_Button.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Scroll.H>
#include <FL/Fl_Widget.H>

#include <mlt++/Mlt.h>
using namespace Mlt;

#include "NLE.h"

namespace shotcut
{
	class AvMultitrack;

	class AvTractor_Render : public Fl_Widget
	{
		public:
			AvMultitrack &multitrack;
			int hover;
			AvTractor_Render( AvMultitrack &multitrack_, int x, int y, int w, int h );
			void draw( );
			int handle( int e );
			void map( int x, int y, int &track, int &position );
			static void static_handle_hover( void *obj );
	};

	class AvTimeline : public Fl_Widget
	{
		public:
			AvMultitrack &multitrack;
			AvTimeline( AvMultitrack &multitrack_, int x, int y, int w, int h );
			int handle( int e );
			void draw( );
	};

	class AvSlider : public Fl_Slider
	{
		public:
			AvMultitrack &multitrack;
			int hover;
			AvSlider( AvMultitrack &multitrack_, int x, int y, int w, int h );
			int handle( int e );
			void draw( );
			static void static_handle_hover( void *obj );
	};

	class AvScrollGroup : public Fl_Scroll
	{
		public:
			AvTimeline timeline;
			AvSlider slider;
			AvTractor_Render render;
			AvMultitrack &multitrack;
			AvScrollGroup( AvMultitrack &multitrack_, int x, int y, int w, int h );
			void resize( int x, int y, int w, int h );
			Fl_Slider *get_slider( ) { return &slider; }
	};

	class AvTimeline_Labels : public Fl_Widget
	{
		private:
			AvMultitrack &multitrack;
		public:
			AvTimeline_Labels( AvMultitrack &multitrack_, int x, int y, int w, int h );
			void draw( );
			int handle( int e );
	};

	class AvMultitrack : public Fl_Group
	{
		private:
			AvTimeline_Labels labels;
			AvScrollGroup timeline;
			double fixed_;
			double zoom_;
			NLE_Project *project_;

		public:
			AvMultitrack( int x, int y, int w, int h );
			~AvMultitrack( );
			// Change the current project
			void project( NLE_Project * );
			// Get a reference to the current project
			// this throws an exception if it's unavaiable
			NLE_Project &project( );
			// Returns true if a project has been assigned
			bool has_project( );
			// The fixed value indicates the real project length the multitrack can display
			// Typically it's larger than the current project length, but may be the same size
			double fixed( );
			void fixed( double );
			// The zoom level
			double zoom( );
			void zoom( double );
			// Refresh the display to the current project position
			// If forced it always redraws and returns true, if not forced, it only returns
			// true if the active cut on the active track has changed
			bool refresh( bool forced = false );
			// Refresh the display if the position changes the active cut on the active track
			bool refresh( int position );
			// Returns true if the focus is currently on the multitrack or its component widgets
			bool has_focus( ) { return Fl::focus( ) == &timeline.render || Fl::focus( ) == &timeline.slider; }
			// Take focus
			bool take_focus( ) { NLE::Change_Mode( project_mode ); return timeline.render.take_focus( ); }
			// Change the active track
			void change_active_track( int track = -1 );
			// Return the slider associated to the multitrack
			Fl_Slider *slider( ) { return timeline.get_slider( ); }
			// Set the current position and force it to the centre when specified and zoom > 1
			void set_position( int, bool centre = false );
			// Scroll to a particular position without changing position
			void scroll_to( int, bool centre = false, bool force = false );
			// Return the x coordinate that inidicates the start of the visible scroll component
			int xposition( );
			// Insert this producer at a specific position on the active track
			void insert_at( int position, Producer *producer );
			// Turn transitions on or off
			void transitions( bool value );
			void lock_tracks( bool value );
			void resize_items( );
	};
}

#endif
