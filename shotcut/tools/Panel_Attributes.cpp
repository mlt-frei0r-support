/*
 * Panel_Attributes.h -- Attributes panel
 * Copyright (C) 2004-2005 Ushodaya Enterprises Limited
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "NLE.h"
#include "Panel_Attributes.h"
#include "AvUtils.h"
using namespace shotcut;

#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Slider.H>

class NLE_Attributes : public NLE_Panel
{
	private:
		Producer *current;
		Fl_Check_Button *buttons[ 15 ];
		Fl_Input *inputs[ 15 ];
		char *display[ 15 ];
		char *labels[ 15 ];
		bool text_value[ 15 ];
		bool final[ 15 ];

	public:
		NLE_Attributes( int x, int y, int w, int h, const char *label ) :
			NLE_Panel( x, y, w, h, label ),
			current( NULL )
		{
			Fl_Group *outer = new Fl_Group( x + 5, y + 5, w, 15 * 32 );
			Fl_Group *group = new Fl_Group( x + 5, y + 5, 80, 15 * 32 );
			for ( int i = 0; i < 15; i ++ )
			{
				buttons[ i ] = new Fl_Check_Button( x + 5, y + 8 + i * 32, 80, 14, "<attribute>" );
				buttons[ i ]->labelsize( 12 );
				buttons[ i ]->hide( );
				buttons[ i ]->callback( static_button_cb, this );
				buttons[ i ]->clear_visible_focus( );
			}
			group->resizable( );
			group->end( );

			group = new Fl_Group( x + 85, y + 5, w - 95, 15 * 32 );
			for ( int i = 0; i < 15; i ++ )
			{
				inputs[ i ] = new Fl_Input( x + 90, y + 5 + i * 32, w - 95, 24 );
				inputs[ i ]->textfont( FL_TIMES );
				inputs[ i ]->textsize( 16 );
				inputs[ i ]->hide( );
				inputs[ i ]->callback( static_input_cb, this );
				inputs[ i ]->when( FL_WHEN_CHANGED );
				labels[ i ] = NULL;
				display[ i ] = NULL;
			}
			group->resizable( group );
			group->end( );

			outer->resizable( group );
			outer->end( );

			group = new Fl_Group( x + 5, y + 325, w - 10, h - 325 );
			group->end( );

			resizable( group );
			end( );
		}

		~NLE_Attributes( )
		{
			for ( int i = 0; i < 15; i ++ )
			{
				free( labels[ i ] );
				free( display[ i ] );
			}
			delete current;
		}

		static void static_button_cb( Fl_Widget *widget, void *obj )
		{
			( ( NLE_Attributes * )obj )->button_cb( ( Fl_Button * )widget );
		}

		void button_cb( Fl_Button *button )
		{
			int i = 0;
			clear_dirty( );
			for ( i = 0; i < 15; i ++ )
				if ( buttons[ i ] == button )
					break;
			if ( i < 15 )
			{
				char temp[ 200 ];
				NLE::Project( ).block( );
				sprintf( temp, "meta.attr.%s", labels[ i ] );
				current->set( temp, button->value( ) );
				if ( button->value( ) )
				{
					if ( text_value[ i ] )
					{
						inputs[ i ]->activate( );
						inputs[ i ]->take_focus( );
						sprintf( temp, "meta.attr.%s.markup", labels[ i ] );
						if ( current->get( temp ) == NULL )
							current->set( temp, "" );
						sprintf( temp, "meta.attr.%s.font", labels[ i ] );
						const char *font = NLE::Project( ).get( "nle_font_display" );
						font = font == NULL ? "Sans" : font;
						current->set( temp, font );
						sprintf( temp, "meta.attr.%s.size", labels[ i ] );
						const char *size = NLE::Project( ).get( "nle_font_attr_size" );
						size = size == NULL ? "24" : size;
						current->set( temp, size );
					}
					sprintf( temp, "meta.attr.%s.final", labels[ i ] );
					current->set( temp, final[ i ] );
				}
				else
				{
					inputs[ i ]->deactivate( );
				}
				NLE::Project( ).unblock( );
				NLE::Refresh_Preview( );
			}
		}

		static void static_input_cb( Fl_Widget *widget, void *obj )
		{
			( ( NLE_Attributes * )obj )->input_cb( ( Fl_Input * )widget );
		}

		void input_cb( Fl_Input *input )
		{
			int i = 0;

			NLE::Ignore_Changes( true );
			dirty( true );

			for ( i = 0; i < 15; i ++ )
				if ( inputs[ i ] == input )
					break;
			if ( i < 15 )
			{
				char temp[ 200 ];
				sprintf( temp, "meta.attr.%s.markup", labels[ i ] );
				NLE::Project( ).block( );
				if ( text_value[ i ] )
				{
					char *result = AvUtils::iconv_convert( ( char * )inputs[ i ]->value( ), "iso8859-1", "UTF-8" );
					current->set( temp, result );
					free( result );
				}
				NLE::Project( ).unblock( );
				NLE::Refresh_Preview( );
			}
		}

		void update( )
		{
			Producer *cut = NLE::Fetch_Selected( );
			if ( cut != NULL && cut->is_valid( ) && NLE::Current_Mode( ) == project_mode )
			{
				delete current;
				current = new Producer( cut );

				int used = 0;
				NLE_Project &project = NLE::Project( );
				const char *attributes = project.attributes( );
				if ( attributes != NULL )
				{
					Tokeniser tokens( ( char * )attributes, "," );
					for ( used = 0; used < tokens.count( ); used ++ )
					{
						char *name = tokens.get( used );
						char name_value[ 100 ];
						char name_active[ 100 ];
						free( display[ used ] );
						free( labels[ used ] );
						char *t = name;

						// Nasty little hacks to denote arguments used and final frame
						text_value[ used ] = *t != '*';
						if ( *t == '*' ) t ++;
						final[ used ] = *t != '@';
						if ( *t == '@' ) t ++;

						labels[ used ] = strdup( t );

						// Create the presentation of the label
						display[ used ] = strdup( t );
						char *p = display[ used ];
						bool uc = true;
						while( *p )
						{
							if ( *p == '_' )
							{
								*p = ' ';
								uc = true;
							}
							else if ( uc && !isspace( *p ) )
							{
								*p = toupper( *p );
								uc = false;
							}

							p ++;
						}

						buttons[ used ]->label( display[ used ] );
						buttons[ used ]->show( );
						if ( text_value[ used ] )
							inputs[ used ]->show( );
						else
							inputs[ used ]->hide( );

						sprintf( name_value, "meta.attr.%s.markup", labels[ used ] );
						sprintf( name_active, "meta.attr.%s", labels[ used ] );
						char *value = cut->get( name_value );
						int active = cut->get_int( name_active );
						char *result = AvUtils::iconv_convert( ( const char * )value, "UTF-8", "iso8859-1" );

						if ( active )
						{
							buttons[ used ]->value( 1 );
							inputs[ used ]->value( result == NULL ? "" : result );
							inputs[ used ]->activate( );
						}
						else 
						{
							buttons[ used ]->value( 0 );
							inputs[ used ]->value( result == NULL ? "" : result );
							inputs[ used ]->deactivate( );
						}

						free( result );
					}
				}
				for ( ; used < 15; used ++ )
				{
					buttons[ used ]->hide( );
					inputs[ used ]->hide( );
				}
			}
			else
			{
				delete current;
				current = NULL;
				for ( int i = 0; i < 15; i ++ )
				{
					buttons[ i ]->hide( );
					inputs[ i ]->hide( );
				}
			}
			delete cut;
		}
};

NLE_Panel *shotcut::Panel_Attributes( int x, int y, int w, int h )
{
	return new NLE_Attributes( x, y, w, h, "Attributes" );
}

