/**
 * AvCutter.h - An FLTK 'Cutting' Widget
 * Copyright (C) 2004-2005 Ushodaya Enterprises Limited
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _AV_CUTTER_H_
#define _AV_CUTTER_H_

#include <FL/Fl_Widget.H>

/** The AvCutter class provides a widget which is similar to an FLTK value 
    slider, but it controls 3 user changeable values (position, in and out) 
	and has an additional length value. Further to that, the widget has two 
	additional modes - linked and locked. When linked, modifications to in 
	and out attempt to keep the distance between them fixed. When locked, 
	the position can only change between the in and out points.

	When drawn, the widget looks something like:

		            >                      <
		[ in ]       ||||||||||||||||||||||              [ out ]
		                      ^

	During use the object has a 'handling' state - this indicates the currently 
	selected action and can be one of NONE (not being changed), IN, OUT, 
	POSITION or DND. The DND case indicates that the user is currently dragging 
	the middle section of the widget to another widget. 

	The widget also takes the keyboard focus and the cursor keys act on the last 
	selected 'handle'. This is also indicated in the widgets drawing.
*/

namespace shotcut
{
	// Typedef to denote the current state of interation with the widget
	typedef enum 
	{
		NONE = 0,
		POSITION = 1,
		IN = 2,
		OUT = 4,
		DND = 8,
		INSERT = 16
	}
	AvCutterHandle;

	class AvCutter : public Fl_Widget
	{
		private:
			AvCutterHandle _handling;
			AvCutterHandle _focus;
			int _length;
			int _in;
			int _out;
			int _position;
			bool _linked;
			bool _locked;
			bool _pressed;
			int _fixated;
			int _link_gap;
	
		public:
			// The constructor allows you to specify where the widget is placed
			AvCutter( int x, int y, int w, int h );
			// Fix things - this is a bit pattern based on AvCutterHandle - if a 
			// bit is specified, the 'handle' associated to it is disabled
			void fixate( int );
			// Set and get the linked state
			void linked( bool );
			bool linked( );
			// Set and get the locked state
			void locked( bool );
			bool locked( );
			// Used to indicate that the widget is under keyboard control
			bool pressed( );
			// Set the position, in, out and length - no callback generated
			void set_values( int position, int in, int out, int length );
			// Set and get the position
			void position( int );
			int position( );
			// Set and get the in point
			void in( int );
			int in( );
			// Set and get the out point
			void out( int );
			int out( );
			// Set and get the length
			void length( int );
			int length( );
			// Return the current action
			AvCutterHandle action( );
			// Override of draw method
			void draw();
			// Override of handle event method
			int handle( int );
			// Set focus on the specified handle
			void set_focus( int );

		private:
			// Used by handle to deal with the key press event [generates a callback]
			int handle_focus( int );
			// Used by handle to deal with the push event
			void handle_push( AvCutterHandle );
			// Used by handle to deal with the drag event [generates a callback]
			void handle_drag( int value );
			// Used by handle to deal with the release event [generates a callback]
			void handle_release( );
			// Calculates the dimensions of the in/out bar
			bool get_extents( int &, int &, int &, int & );
			// Map an in/out/position value to the x coordinate
			int map( int value );
			// Unmap an x coordinate to an in/out/position value 
			int unmap( int value );
	};
}

#endif
