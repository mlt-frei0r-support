/**
 * AvUtils.h - General Utils
 * Copyright (C) 2004-2005 Charles Yates
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _AV_UTILS_H_
#define _AV_UTILS_H_

namespace shotcut
{
	typedef enum
	{
		TIMECODE = 0,
		FRAMES = 1,
		SECONDS = 2
	}
	AvTimecode;

	class AvUtils
	{
		private:
			static int fps_;
			static int fps_den_;
			static AvTimecode timecode_;
		public:
			static void set_fps( int fps, int fps_den );
			static double get_fps( );
			static void set_timecode( AvTimecode timecode );
			static char *timecode( char *tc, int value );
			// Free the result from this function
			static char *iconv_convert( const char *value, const char *in_encoding, const char *out_encoding );
	};
};

#endif
