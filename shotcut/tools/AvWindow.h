
#ifndef _AV_WINDOW_H_
#define _AV_WINDOW_H_

#include <FL/Fl_Double_Window.H>
#include <FL/x.H>
#include <stdlib.h>

namespace shotcut
{
	class AvWindow : public Fl_Double_Window
	{
		public:
			AvWindow( int x, int y, int w, int h ) :
				Fl_Double_Window( x, y, w, h )
			{
			}

			AvWindow( int width, int height ) :
				Fl_Double_Window( width, height )
			{
			}

			void maximize( bool active )
			{
				XEvent ev;
				Display *display = fl_display;
				static char *names[ ] = { "_NET_WM_STATE",  "_NET_WM_STATE_MAXIMIZED_VERT", "_NET_WM_STATE_MAXIMIZED_HORZ" };
				Atom atoms[ 3 ];
				XInternAtoms( display, names, 3, False, atoms );
				Atom net_wm_state = atoms[ 0 ];
				Atom net_wm_state_vert = atoms[ 1 ];
				Atom net_wm_state_horz = atoms[ 2 ];
				ev.type = ClientMessage;
				ev.xclient.window = fl_xid( this );
				ev.xclient.message_type = net_wm_state;
				ev.xclient.format = 32;
				ev.xclient.data.l[ 0 ] = active ? 1 : 0;
				ev.xclient.data.l[ 1 ] = net_wm_state_vert;
				ev.xclient.data.l[ 2 ] = 0;
				XSendEvent( display, DefaultRootWindow( display ), False, SubstructureNotifyMask|SubstructureRedirectMask, &ev );
				ev.xclient.data.l[ 1 ] = net_wm_state_horz;
				XSendEvent( display, DefaultRootWindow( display ), False, SubstructureNotifyMask|SubstructureRedirectMask, &ev );
			}

			void always_on_top( bool active )
			{
				XEvent ev;
				Display *display = fl_display;
				static char *names[ ] = { "_NET_WM_STATE",  "_NET_WM_STATE_ABOVE" };
				Atom atoms[ 2 ];
				XInternAtoms( display, names, 2, False, atoms );
				Atom net_wm_state = atoms[ 0 ];
				Atom net_wm_state_above = atoms[ 1 ];
				ev.type = ClientMessage;
				ev.xclient.window = fl_xid( this );
				ev.xclient.message_type = net_wm_state;
				ev.xclient.format = 32;
				ev.xclient.data.l[ 0 ] = active ? 1 : 0;
				ev.xclient.data.l[ 1 ] = net_wm_state_above;
				ev.xclient.data.l[ 2 ] = 0;
				XSendEvent( display, DefaultRootWindow( display ), False, SubstructureNotifyMask|SubstructureRedirectMask, &ev );
			}
	};
}

#endif
