#ifndef _FLTK_AV_WIDGET_
#define _FLTK_AV_WIDGET_

namespace shotcut
{
	class AvWidget
	{
		public:
			virtual void on_hide( ) = 0;
			virtual void on_show( ) = 0;
			virtual void on_close( ) = 0;
	};
}

#endif
