/**
 * AvUtils.cpp - General Utils
 * Copyright (C) 2004-2005 Charles Yates
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// TODO: Correct timecodes for NTSC

#include <stdio.h>
#include "AvUtils.h"
using namespace shotcut;

#include <iconv.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

int AvUtils::fps_ = 25;
int AvUtils::fps_den_ = 1;
AvTimecode AvUtils::timecode_ = TIMECODE;

void AvUtils::set_fps( int fps, int fps_den )
{
	fps_ = fps;
	fps_den_ = fps_den;
}

double AvUtils::get_fps( )
{
	return ( double )fps_ / fps_den_;
}

void AvUtils::set_timecode( AvTimecode timecode )
{
	timecode_ = timecode;
}

char *AvUtils::timecode( char *tc, int value )
{
	if ( value < 0 ) value = 0;

	if ( timecode_ == FRAMES )
	{
		sprintf( tc, "%d", value );
	}
	else if ( timecode_ == SECONDS )
	{
		int ms = value % fps_;
		int s = value / fps_;
		int m = s / 60;
		int h = m / 60;
		if ( h > 0 )
			sprintf( tc, "%d:%02d:%02d:%02d", h, m % 60, s % 60, ms * 4 );
		else
			sprintf( tc, "%02d:%02d:%02d", m % 60, s % 60, ms * 4 );
	}
	else if ( timecode_ == TIMECODE )
	{
		int ms = value % fps_;
		int s = value / fps_;
		int m = s / 60;
		int h = m / 60;
		if ( h > 0 )
			sprintf( tc, "%d:%02d:%02d.%02d", h, m % 60, s % 60, ms );
		else
			sprintf( tc, "%02d:%02d.%02d", m % 60, s % 60, ms );
	}

	return tc;
}

char *AvUtils::iconv_convert( const char *value, const char *in_encoding, const char *out_encoding )
{
	// Will hold the result
	char *result = NULL;

	if ( value != NULL )
	{
		// Open the convert
		iconv_t	cd = iconv_open( out_encoding, in_encoding );

		// Ensure that it's valid
		if ( cd != ( iconv_t )-1 )
		{
			// Set up argument for iconv
			char *inbuf_p = ( char * )value;
			size_t inbuf_n = strlen( value );
			size_t outbuf_n = inbuf_n * 6 + 1;
			char *outbuf_p = ( char * )calloc( outbuf_n, 1 );
		
			// Ensure that we have the correct result to return
			result = outbuf_p;

			// Convert the text
			if ( iconv( cd, &inbuf_p, &inbuf_n, &outbuf_p, &outbuf_n ) == ( size_t )-1 )
				fprintf( stderr, "%s\n", strerror( errno ) );

			iconv_close( cd );
		}
	}

	return result;
}

