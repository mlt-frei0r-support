#ifndef _FLTK_MIRACLE_PLAYER_
#define _FLTK_MIRACLE_PLAYER_

#include <mlt++/Mlt.h>
using namespace Mlt;

#include "AvPlayer.h"
using namespace shotcut;

namespace shotcut
{
	class MiraclePlayer : public AvPlayer
	{
		private:
			Miracle *miracle;
		public:
			MiraclePlayer( Properties &properties, int port, int x, int y, int w, int h );
			virtual ~MiraclePlayer( );
	};
}

#endif

