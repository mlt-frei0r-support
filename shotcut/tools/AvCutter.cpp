/**
 * AvCutter.cpp - An FLTK 'Cutting' Widget
 * Copyright (C) 2004-2005 Ushodaya Enterprises Limited
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include "AvUtils.h"
#include "AvCutter.h"
using namespace shotcut;

AvCutter::AvCutter( int x, int y, int w, int h ) :
	Fl_Widget( x, y, w, h ),
	_handling( NONE ),
	_focus( POSITION ),
	_length( 0 ),
	_in( 0 ),
	_out( 0 ),
	_position( -1 ),
	_linked( false ),
	_locked( false ),
	_fixated( NONE ),
	_link_gap( 0 )
{
	set_visible_focus( );
}

void AvCutter::fixate( int flags )
{
	if ( flags != _fixated )
		redraw( );
	_fixated = flags;
}

void AvCutter::linked( bool linked )
{
	_link_gap = _out - _in + 1;
	_linked = linked;
}

bool AvCutter::linked( )
{
	return _linked;
}

void AvCutter::locked( bool value )
{
	_locked = value;
}

bool AvCutter::locked( )
{
	return _locked;
}

bool AvCutter::pressed( )
{
	return _pressed;
}

void AvCutter::set_values( int position, int in, int out, int length )
{
	int o_p = _position;
	int o_i = _in;
	int o_o = _out;
	int o_l = _length;
	if ( position >= 0 ) _position = position; else _position = 0;
	if ( in != -1 ) _in = in;
	if ( out != -1 ) _out = out;
	if ( length != -1 ) _length = length;
	if ( _in < 0 ) _in = 0;
	if ( in != -1 && _in > _out ) _in = _out;
	if ( out != -1 && _out < _in ) _out = _in;
	if ( _in >= _length ) _in = _length - 1;
	if ( _out >= _length ) _out = _length - 1;
	if ( _locked && _position < _in ) _position = _in;
	if ( _locked && _position > _out ) _position = _out;
	if ( _position < 0 ) _position = 0;
	if ( _position >= _length ) _position = _length - 1;
	if ( position == -1 && in != -1 && _focus == IN ) _position = _in;
	if ( position == -1 && out != -1 && _focus == OUT ) _position = _out;
	_link_gap = _out - _in + 1;
	if ( o_p != _position )
		damage( FL_DAMAGE_ALL );
	if ( o_i != _in || o_o != _out || o_l != _length )
		damage( FL_DAMAGE_ALL );
}

void AvCutter::position( int position )
{
	set_values( position, -1, -1, -1 );
}

int AvCutter::position( )
{
	return _position;
}

void AvCutter::in( int in )
{
	if ( !_linked )
		set_values( -1, in, -1, -1 );
	else if ( in + _link_gap < _length )
		set_values( -1, in, in + _link_gap - 1, -1 );
	else
		set_values( -1, _length - _link_gap, _length - 1, -1 );

}

int AvCutter::in( )
{
	return _in;
}

void AvCutter::out( int out )
{
	if ( !_linked )
		set_values( -1, -1, out, -1 );
	else if ( out - _link_gap >= 0 )
		set_values( -1, out - _link_gap + 1, out, -1 );
	else
		set_values( -1, 0, _link_gap - 1, -1 );
}

int AvCutter::out( )
{
	return _out;
}

void AvCutter::length( int length )
{
	set_values( -1, -1, -1, length );
}

int AvCutter::length( )
{
	return _length;
}

AvCutterHandle AvCutter::action( )
{
	return _handling;
}

void AvCutter::draw( )
{
	if ( _length == 0 )
	{
		fl_draw_box( FL_FLAT_BOX, x( ), y( ), w( ), h( ), FL_BACKGROUND_COLOR );
	}
	else if ( damage( ) & FL_DAMAGE_ALL )
	{
		char str[ 100 ];

		fl_draw_box( FL_FLAT_BOX, x( ), y( ), w( ), h( ), FL_BACKGROUND_COLOR );

		// Draw the in and out positions
		fl_draw_box( FL_EMBOSSED_BOX, x( ) + 60, y( ), w( ) - 120, h( ), FL_GRAY );
		fl_draw_box( FL_EMBOSSED_BOX, x( ) + w( ) - 60, y( ), 60, h( ), FL_GRAY );
		fl_draw_box( FL_EMBOSSED_BOX, x( ), y( ), 60, h( ), FL_GRAY );
		fl_font( FL_HELVETICA, 10 ); 
		fl_color( FL_BLACK );
		fl_draw( AvUtils::timecode( str, _in ), x( ) + 3, y( ), 56, h( ), FL_ALIGN_CENTER );
		fl_draw( AvUtils::timecode( str, _out ), x( ) + w( ) - 60 + 3, y( ), 56, h( ), FL_ALIGN_CENTER );

		// Draw the selected region
		int sx, sy, sw, sh;
		if ( get_extents( sx, sy, sw, sh ) )
		{
			fl_draw_box( FL_EMBOSSED_BOX, map( 0 ) - 1, sy + 1, map( _length - 1 ) - x( ) - 60 - h( ) / 3 + 3, sh, FL_GRAY );
			fl_draw_box( FL_FLAT_BOX, sx, sy + 2, sw, sh - 2, FL_BLACK );
		}

		// Draw the position and in/out triangles
		int tx, ty, tx1, ty1, tx2, ty2;
		int d = sh - 2;
		ty = y( ) + d * 2 + 4;
		tx = map( _position );
		if ( tx < x( ) + 60 + d ) tx = x( ) + 60 + d;
		tx1 = tx - d;
		tx2 = tx + d;
		ty1 = ty2 = ty + d;
		fl_color( Fl::focus( ) == this && _focus == POSITION ? FL_BLACK : FL_BLUE );
		if ( !( _fixated & POSITION ) )
			fl_polygon( tx, ty, tx1, ty1, tx2, ty2 );
		ty = y( ) + 2;
		tx = map( _in );
		tx1 = tx - d;
		tx2 = tx;
		ty2 = ty + d;
		ty1 = ty;
		fl_color( Fl::focus( ) == this && _focus == IN ? FL_BLACK : FL_BLUE );
		if ( !( _fixated & IN ) )
			fl_polygon( tx, ty, tx1, ty1, tx2, ty2 );
		ty = y( ) + 2;
		tx = map( _out );
		tx1 = tx + d;
		tx2 = tx;
		ty1 = ty;
		ty2 = ty + d;
		fl_color( Fl::focus( ) == this && _focus == OUT ? FL_BLACK : FL_BLUE );
		if ( !( _fixated & OUT ) )
			fl_polygon( tx, ty, tx1, ty1, tx2, ty2 );
	}
	else
	{
		int sx, sy, sw, sh;
		get_extents( sx, sy, sw, sh );
		int tx, ty, tx1, ty1, tx2, ty2;
		int d = sh - 2;
		ty = y( ) + d * 2 + 4;
		tx = map( _position );
		if ( tx < x( ) + 60 + d ) tx = x( ) + 60 + d;
		tx1 = tx - d;
		tx2 = tx + d;
		ty1 = ty2 = ty + d;
		fl_color( Fl::focus( ) == this && _focus == POSITION ? FL_BLACK : FL_BLUE );
		if ( !( _fixated & POSITION ) )
			fl_polygon( tx, ty, tx1, ty1, tx2, ty2 );
	}
}

int AvCutter::handle( int e )
{
	int sx, sy, sw, sh;
	int ex = Fl::event_x( ) - x( );
	int d = h( ) / 3;
	int x_in = map( _in );
	int x_out = map( _out );

	// If we don't have a valid setting, then return now
	if ( !get_extents( sx, sy, sw, sh ) )
		return 0;

	switch( e )
	{
		case FL_PUSH:
			take_focus( );
			if ( ex < 60 || ex > w( ) - 60 )
			{
				position( ex < 60 ? _in : _out );
				do_callback( );
				return 1;
			}
			else
			{
				if ( !( _fixated & POSITION ) && Fl::event_y( ) > y( ) + 2 * d )
					handle_push( POSITION );
				else if ( !( _fixated & IN ) && Fl::event_inside( x_in - d, y( ), d, d ) )
					handle_push( IN );
				else if ( !( _fixated & OUT ) && Fl::event_inside( x_out, y( ), d, d ) )
					handle_push( OUT );
				else if ( !( _fixated & DND ) )
					handle_push( DND );
				do_callback( );
				return 1;
			}
		case FL_DRAG:
			handle_drag( unmap( ex ) );
			return 1;
		case FL_RELEASE:
			handle_release( );
			return 1;
		case FL_FOCUS:
			damage( FL_DAMAGE_ALL );
			return 1;
		case FL_UNFOCUS:
			damage( FL_DAMAGE_ALL );
			do_callback( );
			return 1;
		case FL_KEYDOWN:
			return handle_focus( Fl::event_key( ) );
		case FL_KEYUP:
			if ( _pressed )
			{
				_pressed = false;
				handle_release( );
				return 1;
			}
		default:
			return Fl_Widget::handle( e );
	}
}

int AvCutter::handle_focus( int key )
{
	int ret = 1;
	int offset = 0;

	if ( Fl::event_state( FL_CTRL ) && key == FL_Left ) offset = -25;
	else if ( key == FL_Left ) offset = -1;
	else if ( Fl::event_state( FL_CTRL ) && key == FL_Right ) offset = 25;
	else if ( key == FL_Right ) offset = 1;
	else ret = 0;

	_handling = _focus;

	if ( ret )
	{
		switch( _focus )
		{
			case POSITION:
				position( _position + offset );
				break;
			case IN:
				in( _in + offset );
				break;
			case OUT:
				out( _out + offset );
				break;
			default:
				// Should never happen
				ret = 0;
				break;
		}
	}
	else
	{
		ret = 1;

		if ( !( _fixated & IN ) && Fl::event_state( FL_CTRL ) && key == 'i' )
			_focus = IN;
		else if ( !( _fixated & OUT ) && Fl::event_state( FL_CTRL ) && key == 'o' )
			_focus = OUT;
		else if ( !( _fixated & POSITION ) && key == 'p' && Fl::event_state( FL_CTRL ) )
			_focus = POSITION;
		else if ( !( _fixated & POSITION ) && key == FL_Home )
			position( _in );
		else if ( !( _fixated & POSITION ) && key == FL_End )
			position( _out );
		else if ( !( _fixated & DND ) && key == FL_Enter )
			handle_push( INSERT );
		else
			ret = 0;
	}

	if ( ret )
	{
		if ( _handling != INSERT )
			_pressed = true;
		do_callback( );
	}

	return ret;
}

void AvCutter::set_focus( int flag )
{
	if ( ( flag & _fixated ) == 0 )
		_focus = ( AvCutterHandle )flag;
	take_focus( );
}

void AvCutter::handle_push( AvCutterHandle pushed )
{
	_handling = pushed;
	if ( pushed == POSITION || pushed == IN || pushed == OUT )
		_focus = pushed;
	if ( pushed == DND )
		do_callback( );
}

void AvCutter::handle_drag( int value )
{
	if ( value >= _length )
		value = _length - 1;
	else if ( value < 0 )
		value = 0;

	switch( _handling )
	{
		case IN:
			position( value );
			in( value );
			break;
		case OUT:
			position( value );
			out( value );
			break;
		case POSITION:
			position( value );
			break;
		default:
			break;
	}

	if ( _handling != DND )
		do_callback( );
}

void AvCutter::handle_release( )
{
	_handling = NONE;
	do_callback( );
}

bool AvCutter::get_extents( int &sx, int &sy, int &sw, int &sh )
{
	if ( _length != 0 )
	{
		int d = h( ) / 3;
		int width = w( ) - 120 - d * 2;
		sx = 60 + d + + ( int )( ( double )width * ( ( double )_in / ( double )_length ) );
		sy = y( ) + d;
		sw = ( int )( ( double )width * ( ( double )_out / ( double )_length ) ) - sx + 60 + d;
		sx += x( );
		sh = d;
	}
	return _length != 0;
}

int AvCutter::map( int value )
{
	if ( _length != 0 )
	{
		int d = h( ) / 3;
		int width = w( ) - 120 - d * 2;
		return 60 + d + x( ) + ( int )( ( double )width * ( ( double )value / ( double )_length ) );
	}
	return 0;
}

int AvCutter::unmap( int value )
{
	if ( _length != 0 )
	{
		int d = h( ) / 3;
		int width = w( ) - 120 - d * 2;
		value -= 60 + d;
		return ( int )( ( double )_length * ( ( double )value / ( double )width ) );
	}
	return 0;
}

