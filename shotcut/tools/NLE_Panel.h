/*
 * NLE_Panel.h -- Abstract class for panels
 * Copyright (C) 2004-2005 Ushodaya Enterprises Limited
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _NLE_PANEL_
#define _NLE_PANEL_

#include <FL/Fl_Group.H>
#include "NLE_Types.h"
#include "NLE.h"

namespace shotcut
{
	/** An NLE Panel has a single constructor (specifying initial geometry of 
	  * the panel and the label that will be used in the tab group to which it 
	  * belongs) and an update method that is invoked when there is change in 
	  * a cut on any track (ie: it's at the panels discretion to determine what
	  * do in the case of an update). They may also optionally listen for
	  * position modifications.
	  *
	  * Additionally, panels may make minor updates to the project without 
	  * triggering autosave and undo buffering using the dirty flag handling
	  * (autosave will automatically happen on the next panel update but can
	  * also be used internally to allow better granularity of autosave on 
	  * certain events).
	  */

	class NLE_Panel : public Fl_Group
	{
		private:
			bool dirty_;
		public:
			// The constructor for the panel
			NLE_Panel( int x, int y, int w, int h, const char *label ) :
				Fl_Group( x, y, w, h, label ), dirty_( false ) { }
			// The virtual update method
			virtual void update( ) = 0;
			// Optional frame changed (absolute within project)
			// Panels don't need to implement this
			virtual void set_position( int value ) { }
			// Specify modes which the panel is active for
			virtual int active_modes( ) { return project_mode; }

			// Set dirty flag
			void dirty( bool flag ) { dirty_ = flag; }
			// Get the dirtry flag
			bool dirty( ) { return dirty_; }
			// Convenience - clear the dirty flag and update if necessary
			void clear_dirty( ) { if ( dirty_ ) { NLE::Ignore_Changes( false ); NLE::Check_Point( ); dirty_ = false; } }
	};
}

#endif
