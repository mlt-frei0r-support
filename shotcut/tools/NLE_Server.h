/**
 * NLE_Server.h - Provides the NLE server component
 * Copyright (C) 2004-2005 Ushodaya Enterprises Limited
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _NLE_SERVER_H_
#define _NLE_SERVER_H_

#include <mlt++/Mlt.h>
using namespace Mlt;

#include "NLE.h"
using namespace shotcut;

namespace shotcut
{
	class NLE_Server : public Miracle
	{
		public:
			// Construct and start the server
			NLE_Server( char *id, int port );
			// Shutdown the old instance to make way for the new
			void shutdown_old_server( int port );
			// We only accept a shutdown command at the moment
			Response *execute( char *command );
			// Push method
			Response *received( char *command, char *doc );
	};
}

#endif

