/**
 * AvMultitrack.cpp - Provides the multitrack widget
 * Copyright (C) 2004-2005 Ushodaya Enterprises Limited
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <math.h>

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/fl_draw.H>
#include "AvUtils.h"
#include "NLE.h"
#include "NLE_Project.h"
#include "AvMultitrack.h"
using namespace shotcut;

#include <mlt++/Mlt.h>
using namespace Mlt;

//
// TRACTOR RENDER
//

AvTractor_Render::AvTractor_Render( AvMultitrack &multitrack_, int x, int y, int w, int h ) :
	Fl_Widget( x, y, w, h ),
	multitrack( multitrack_ ),
	hover( 0 )
{
}

// Draw the current project
void AvTractor_Render::draw( )
{
	try
	{
		// Get the current project
		NLE_Project &project = multitrack.project( );

		// This is the item that is currently selected - depends on mode
		Producer *selected = NLE::Fetch_Selected( );

		// Items which match the parent of the selected item should be warm, 
		// items that match the producer are hot unless they're not on the current
		// track, in which case they're luke warm
		mlt_producer hot = selected != NULL ? selected->get_producer( ) : NULL;
		mlt_producer warm = selected != NULL ? selected->get_parent( ) : NULL;

		// Obtain the number of tracks, current track and position
		int tracks = project.tracks( );
		int active_track_idx = project.current_track( );
		int active_position = project.position( );

		// Calculate the height of each track
		int lh = ( h( ) ) / tracks;

		// Set up the active cuts for each track now to ensure they get updated
		for ( int i = 0; i < tracks; i ++ )
		{
			Playlist &track = project.track( i );
			Producer *cut = track.get_clip_at( active_position );
			track.set( "active_cut", cut != NULL && cut->is_valid( ) ? cut->get_producer( ) : NULL, 0 );
			delete cut;
		}

		// Iterate through each track
		for ( int i = 0; i < tracks; i ++ )
		{
			// Fetch the current track
			Playlist &track = project.track( i );

			// This is the number of frames processed
			int track_processed = 0;

			// Alternate background colours
			Fl_Color bg = i % 2 == 0 ? FL_DARK1 : FL_DARK3;

			// Iterate through each cut on the track
			for ( int j = 0; j < track.count( ); j ++ )
			{
				// Get the current cut
				Producer *cut = project.get_cut( i, j );

				// Get the info related to the cut
				ClipInfo *info = track.clip_info( j );
				int clip_time = info->frame_count;

				// Determine if this the active cut for this track
				int active_cut = active_position >= track_processed && active_position < track_processed + clip_time;

				// Calculate the start and width of this cut when rendering
				int cx = ( int )( ( double )w( ) * ( ( double )track_processed / ( double )multitrack.fixed( ) ) );
				int cw = ( int )( ( double )w( ) * ( ( double )clip_time / ( double )multitrack.fixed( ) ) );

				// If the cut is not blank
				if ( cut != NULL && !cut->is_blank( ) )
				{
					// Set the background colour
					fl_color( bg );

					// Determine if the cut is hot, warm, luke warm or cold
					Fl_Color color = hot == cut->get_producer( ) ? NLE_HOT : NLE_WARM;
					color = warm != cut->get_parent( ) ? NLE_COLD : color;

					// Luke warm is never shown in browser mode
					if ( NLE::Current_Mode( ) != browser_mode )
						if ( i != active_track_idx && active_cut ) color = NLE_LUKE;

					// Draw the box
					Fl_Boxtype box = FL_UP_BOX;
					if ( cut->get_int( "nle_sb" ) ) box = FL_ROUNDED_BOX;
					fl_draw_box( box, x() + cx, y( ) + lh * i + 1, cw, lh, color );

					// Label it
					char *label = cut->parent( ).get( "title" );
					label = label == NULL ? cut->parent( ).get( "nle_title" ) : label;
					if ( label != NULL )
					{
						int j = 0;
						int sizes[ 5 ] = { 14, 12, 10, 8, 6 };
						int tw = 0, th = 0;

						for( j = 0; j < 5; j ++ )
						{
							fl_color( FL_LIGHT3 );
							fl_font( FL_HELVETICA, sizes[ j ] ); 
							fl_measure( label, tw, th );
							if ( tw < cw ) 
							{
								fl_draw( label, x()+cx, y()+lh*i+1, cw, lh, FL_ALIGN_CENTER );
								break;
							}
						}

						if ( cut->get( "meta.volume" ) != NULL && cut->get_double( "meta.volume" ) != 1.0 )
						{
							fl_color( FL_LIGHT3 );
							fl_line( x( ) + cx, y( ) + lh * i, x( ) + cx + cw, y( ) + lh * i + lh );
							fl_line( x( ) + cx, y( ) + lh * i + lh, x( ) + cx + cw, y( ) + lh * i );
						}
					}
				}
				else 
				{
					// Draw a blank, normally as a flat box in the background colour, unless it's active
					fl_draw_box( i == active_track_idx && active_cut ? FL_UP_BOX : FL_FLAT_BOX, x() + cx, y( ) + lh * i + 1, cw, lh, bg );
				}

				// Increment the track processed count
				track_processed += info->frame_count;

				// Delete the cut and info
				delete info;
				delete cut;
			}

			// Fill the remainder of the box in the background colour
			int cx = ( int )( ( double )w( ) * ( ( double )track_processed / ( double )multitrack.fixed( ) ) );
			int cw = w( ) - cx;
			fl_draw_box( FL_FLAT_BOX, x( ) + cx, y( ) + lh * i + 1, cw, lh, bg );
		}

		delete selected;
	}
	catch( const char *exception )
	{
		// Nothing loaded
		fl_font( FL_HELVETICA, 10 ); 
		fl_draw( "No Project Loaded", x( ), y( ), w( ), h( ), FL_ALIGN_CENTER );
	}
}

void AvTractor_Render::static_handle_hover( void *obj )
{
	AvTractor_Render *render = ( AvTractor_Render * )obj;
	if ( render->hover != 0 )
	{
		render->multitrack.scroll_to( render->multitrack.xposition( ) + 25 * render->hover, false, true );
		render->redraw( );
		Fl::check( );
		Fl::add_timeout( 0.05, static_handle_hover, render );
	}
}
		
int AvTractor_Render::handle( int e )
{
	int ret = 0;

	if ( multitrack.has_project( ) && e != 0 )
	{
		ClipInfo info;
		int mouse_track_idx;
		int	mouse_position;

		// Get the track and position under the mouse
		map( Fl::event_x( ), Fl::event_y( ), mouse_track_idx, mouse_position );

		// Correct these so they're in range
		if ( mouse_position < 0 )
			mouse_position = 0;

		// Get the project reference
		NLE_Project &project = multitrack.project( );

		// Get the active cut information
		int active_track_idx = project.current_track( );
		int active_position = project.position( );
		int active_cut_idx = project.cut_index_at( active_track_idx, active_position );

		// Get the drop object
		Producer *drop_object = NLE::Fetch_Drop_Object( );

		// Get the key pressed
		int key = Fl::event_key( );

		switch( e )
		{
			case FL_NO_EVENT:
				ret = 1;
				break;

			case FL_PUSH:
				take_focus( );
				multitrack.damage( FL_DAMAGE_ALL );
				if ( project.current_track( ) != mouse_track_idx )
				{
					project.current_track( mouse_track_idx );
					NLE::Refresh_Multitrack( true );
				}
				if ( mouse_track_idx == 0 && Fl::event_button3( ) && mouse_position < project.length( 0 ) )
				{
					Producer *cut = project.get_cut_at( mouse_position );
					if ( cut != NULL && cut->parent( ).get_int( "nle_type" ) == STORIES )
					{
						if ( cut->get( "meta.volume" ) == NULL )
							cut->set( "meta.volume", 0.0 );
						else if ( cut->get_double( "meta.volume" ) == 1.0 )
							cut->set( "meta.volume", 0.0 );
						else 
							cut->set( "meta.volume", 1.0 );
					}
					delete cut;
					NLE::Change_Mode( project_mode );
				}
				else if ( mouse_track_idx == 0 && Fl::event_clicks( ) == 1 && mouse_position < project.length( 0 ) )
				{
					NLE::Change_Mode( project_mode );
					NLE::Change_Position( mouse_position, multitrack_position, true );
					project.convert_sb( !project.is_sb( ) );
					NLE::Change_Mode( project_mode, true );
				}
				else
				{
					NLE::Change_Mode( project_mode );
					NLE::Change_Position( mouse_position, multitrack_position, true );
				}
				ret = 1;
				break;

			case FL_DRAG:
				ret = project.forward_drop_object_at( active_track_idx, active_position );
				break;

			case FL_FOCUS:
				NLE::Change_Mode( project_mode );
				multitrack.redraw( );
				ret = 1;
				break;

			case FL_UNFOCUS:
				multitrack.redraw( );
				ret = 1;
				break;

			case FL_KEYDOWN:
				ret = 1;
				if ( key == FL_Delete )
				{
					NLE::Change_Mode( project_mode, true );
					int position = project.delete_cut( active_track_idx, active_cut_idx );
					if ( position != -1 ) 
						NLE::Change_Position( position, player_position, true );
					NLE::Change_Mode( project_mode, true );
				}
				else if ( key == 'k' )
				{
					NLE::Change_Mode( project_mode, true );
					int position = project.split_at( active_track_idx, active_position );
					if ( position != -1 ) 
						NLE::Change_Position( position, multitrack_position, true );
					NLE::Change_Mode( project_mode, true );
				}
				else if ( key == 'f' )
				{
					multitrack.fixed( project.length( ) );
				}
				else if ( key == FL_Left )
				{
					NLE::Change_Position( project.position( ) - ( 1 * ( Fl::event_state( FL_CTRL ) ? 25 : 1 ) ), multitrack_position, true );
				}
				else if ( key == FL_Right )
				{
					NLE::Change_Position( project.position( ) + ( 1 * ( Fl::event_state( FL_CTRL ) ? 25 : 1 ) ), multitrack_position, true );
				}
				else if ( key == FL_Up )
				{
					project.current_track( project.current_track( ) - 1 >= 0 ? project.current_track( ) - 1 : 0 );
					NLE::Change_Mode( project_mode, true );
				}
				else if ( key == FL_Down )
				{
					project.current_track( project.current_track( ) + 1 < project.tracks( ) ? project.current_track( ) + 1 : project.tracks( ) - 1 );
					NLE::Change_Mode( project_mode, true );
				}
				else if ( key == FL_Insert )
				{
					NLE::Change_Mode( project_mode, true );
					int position = project.insert_blanks( NLE::Get_Config( ).get_int( "multitrack.insert_length" ) );
					if ( position != -1 ) 
						NLE::Change_Position( position, player_position, true );
					NLE::Change_Mode( project_mode, true );
				}
				else if ( key == 's' )
				{
					project.snap( 0 );
					NLE::Change_Mode( project_mode, true );
				}
				else if ( key == 'm' )
				{
					NLE::Change_Mode( project_mode, true );
					project.match( );
					NLE::Change_Mode( project_mode, true );
				}
				else
					ret = 0;
				break;

			case FL_DND_DRAG:
				if ( project.accepts( mouse_track_idx, drop_object ) )
				{
					Fl::first_window()->cursor( FL_CURSOR_MOVE );
					int ex = Fl::event_x( ) - x( ) - multitrack.xposition( );
					int area = ( multitrack.w( ) - 100 ) / 10;
					if ( multitrack.zoom( ) > 1 && hover == 0 && ex < area )
					{
						hover = -1;
						Fl::add_timeout( 0.1, static_handle_hover, this );
					}
					else if ( multitrack.zoom( ) > 1 && hover == 0 && ex > area * 9 )
					{
						hover = 1;
						Fl::add_timeout( 0.1, static_handle_hover, this );
					}
					else if ( ex > area && ex < area * 9 )
					{
						hover = 0;
					}
					ret = 1;
				}
				else
				{
					Fl::first_window()->cursor( ( Fl_Cursor )1 );
				}
				break;

			case FL_DND_ENTER:
			case FL_DND_LEAVE:
			case FL_DND_RELEASE:
				if ( e == FL_DND_ENTER && project.accepts( mouse_track_idx, drop_object ) )
					Fl::first_window()->cursor( FL_CURSOR_MOVE );
				else if ( e == FL_DND_LEAVE )
					Fl::first_window()->cursor( ( Fl_Cursor )1 );
				else
					Fl::first_window()->cursor( FL_CURSOR_DEFAULT );
				hover = 0;
				ret = 1;
				break;

			case FL_ENTER:
				ret = 1;
				break;

			case FL_PASTE:
				if ( drop_object != NULL )
				{
					int position = -1;
					if ( !strcmp( Fl::event_text( ), "move" ) && active_track_idx == mouse_track_idx )
						position = project.move_to( active_track_idx, active_cut_idx, mouse_position );
					else if ( strcmp( Fl::event_text( ), "move" ) )
						position = project.insert_at( mouse_track_idx, mouse_position, drop_object );
					if ( project.current_track( ) != mouse_track_idx )
						project.current_track( mouse_track_idx );
					NLE::Project( ).track( active_track_idx ).set( "active_cut", NULL, 0 );
					NLE::Change_Mode( project_mode, true );
					if ( position != -1 ) 
						NLE::Change_Position( position, multitrack_position, true );
					ret = 1;
					take_focus( );
				}
				break;

			default:
				break;
		}
	}

	return ret != 0 || e == 0 ? ret : Fl_Widget::handle( e );
}

void AvTractor_Render::map( int ex, int ey, int &track, int &position )
{
	int tracks = multitrack.project( ).tracks( );
	if ( tracks != 0 )
	{
		int lh = h( ) / tracks;
		track = ( ey - y( ) ) / lh;
		position = ( int )( ( ( double ) ( ex - x( ) ) / ( double ) w( ) ) * multitrack.fixed( ) );
	}
}

// 
// TIMELINE WIDGET
//

AvTimeline::AvTimeline( AvMultitrack &multitrack_, int x, int y, int w, int h ) :
	Fl_Widget( x, y, w, h ),
	multitrack( multitrack_ )
{
}

int AvTimeline::handle( int e )
{
	if ( e == FL_RELEASE || e == FL_PUSH )
		multitrack.take_focus( );
	return Fl_Widget::handle( e );
}

void AvTimeline::draw( ) 
{
	fl_draw_box( FL_FLAT_BOX, x( ), y( ), w( ), h( ), FL_GRAY );
	fl_color( FL_BLACK );
	fl_line( x( ), y( ), x( ) + w( ), y( ) );
	int d = h( );
	int max = ( int )multitrack.fixed( );
	if ( max > 0 )
	{
		double seconds = floor( ( double )max / AvUtils::get_fps( ) );
		double size_per_unit = 50;
		int units = ( int )ceil( ( double )( w( ) ) / size_per_unit );
		double unit = 1.0;

		if ( seconds > units )
			unit = ceil( seconds / units );

		if ( unit > 0 && unit < 1 )
			unit = 1.0;
		else if ( unit > 1.0 && unit < 5.0 )
			unit = 5.0;
		else if ( unit > 5.0 && unit < 10.0 )
			unit = 10.0;
		else if ( unit > 10.0 && unit < 15.0 )
			unit = 15.0;
		else if ( unit > 15.0 && unit < 30.0 )
			unit = 30.0;
		else if ( unit > 30.0 && unit < 60.0 )
			unit = 60.0;
		else if ( unit > 60.0 && unit < 120.0 )
			unit = 120.0;
		else if ( unit > 120.0 && unit < 300.0 )
			unit = 300.0;
		else if ( unit > 300 )
			unit = 600.0;

		size_per_unit = ( ( double )w( ) ) / ( seconds / unit );
		units = ( int )ceil( ( double )max / unit );
		double processed = 0;
		fl_font( FL_HELVETICA, 8 );
		char timecode[ 100 ];
		int position = 0;
		while ( processed < w( ) )
		{
			//if ( i % ( int )( unit + 1 ) == 0 )
				fl_draw( AvUtils::timecode( timecode, position ), ( int )( x( ) + processed + 2 ), y( ), 56, d, FL_ALIGN_LEFT );
			fl_line( ( int )( x( ) + processed ), y( ), ( int )( x( ) + processed ), y( ) + d );
			processed += size_per_unit;
			position += ( int )( AvUtils::get_fps( ) * unit );
		}
	}
}

// 
// TIMELINE POSITION WIDGET
//

AvSlider::AvSlider( AvMultitrack &multitrack_, int x, int y, int w, int h ) :
	Fl_Slider( x, y, w, h ),
	multitrack( multitrack_ ),
	hover( 0 )
{
	type( FL_HOR_NICE_SLIDER );
	box( FL_FLAT_BOX );
	slider_size( 0.02 );
	clear_visible_focus( );
	step( 0 );
}

void AvSlider::static_handle_hover( void *obj )
{
	AvSlider *slider = ( AvSlider * )obj;
	if ( slider->hover != 0 )
	{
		NLE::Change_Position( slider->multitrack.project( ).position( ) + slider->hover, multitrack_position, true );
		slider->value( slider->multitrack.project( ).position( ) );
		Fl::add_timeout( 0.02, static_handle_hover, slider );
		slider->redraw( );
		Fl::check( );
	}
}
	
int AvSlider::handle( int e )
{
	if ( e == FL_PUSH || e == FL_DRAG )
	{
		multitrack.take_focus( );
		int ex = Fl::event_x( ) - x( ) - multitrack.xposition( );
		int area = ( multitrack.w( ) - 100 ) / 20;
		if ( multitrack.zoom( ) > 1 && hover == 0 && ex < area )
		{
			hover = -12;
			Fl::add_timeout( 0.2, static_handle_hover, this );
		}
		else if ( multitrack.zoom( ) > 1 && hover == 0 && ex > area * 19 )
		{
			hover = 12;
			Fl::add_timeout( 0.2, static_handle_hover, this );
		}
		else if ( ex > area && ex < area * 19 )
		{
			hover = 0;
		}
	}
	else if ( e == FL_RELEASE )
	{
		hover = 0;
		multitrack.take_focus( );
		return 1;
	}

	return Fl_Slider::handle( e );
}

void AvSlider::draw( ) 
{
	double val = 0.5;
	if ( minimum() != maximum() )
		val = ( value() - minimum() ) / ( maximum( ) - minimum( ) );
	val = val > 1.0 ? 1.0 : val < 0.0 ? 0.0 : val;
	fl_draw_box( FL_FLAT_BOX, x( ), y( ), ( int )( multitrack.zoom( ) * multitrack.w( ) ), h( ), FL_GRAY );
	fl_color( multitrack.has_focus( ) ? FL_BLACK : FL_BLUE );
	fl_line( x( ), y( ) + h( ), x( ) + ( int )( multitrack.zoom( ) * multitrack.w( ) ), y( ) + h( ) );
	int d = h( );
	int tx, ty;
	tx = ( int )( x( ) + val * w( ) );
	ty = y( );
	fl_polygon( tx, ty, tx - d, ty + d, tx + d, ty + d );
}

//
// TIMELINE SCROLL GROUP
//

AvScrollGroup::AvScrollGroup( AvMultitrack &multitrack_, int x, int y, int w, int h ) :
	Fl_Scroll( x, y, w, h ),
	timeline( multitrack_, x, y, w, 9 ),
	slider( multitrack_, x, y + 9, w, 9 ),
	render( multitrack_, x, y + 18, w, h - 32 ),
	multitrack( multitrack_ )
{
	end( );
	type( 0 );
	type( Fl_Scroll::HORIZONTAL_ALWAYS );
}

void AvScrollGroup::resize( int x, int y, int w, int h )
{
	Fl_Scroll::resize( x, y, w, h );
	multitrack.resize_items( );
}

//
// TIMELINE LABELS
//

AvTimeline_Labels::AvTimeline_Labels( AvMultitrack &multitrack_, int x, int y, int w, int h ) :
	Fl_Widget( x, y, w, h ),
	multitrack( multitrack_ )
{
}

void AvTimeline_Labels::draw( )
{
	if ( multitrack.has_project( ) )
	{
		NLE_Project &project = multitrack.project( );

		char tmp[ 100 ];

		fl_font( FL_HELVETICA, 10 ); 
		if ( damage( ) & FL_DAMAGE_ALL )
		{
			int active_track_idx = project.current_track( );
			int tracks = project.tracks( );
			int lh = ( h( ) - 32 ) / tracks;
			for ( int i = 0; i < tracks; i ++ )
			{
				Playlist &track = project.track( i );
				char *label = track.get( ( char * ) "nle_label" );
				int lx = x( );
				int ly = y( ) + 18 + i * lh;
				while( strchr( label, '_' ) )
					*( strchr( label, '_' ) ) = ' ';
				fl_draw_box( FL_DOWN_BOX, lx, ly, w( ), lh,multitrack.has_focus( ) ? FL_GRAY : FL_DARK1 );
				fl_font( FL_HELVETICA, 10 ); 
				fl_color( i == active_track_idx ? FL_RED : FL_BLACK );
				fl_draw( label, lx + 3, ly + 3, w( ) - 6, lh - 6, FL_ALIGN_RIGHT );
			}

			AvUtils::timecode( tmp, project.length( ) );
			fl_draw_box( FL_EMBOSSED_BOX, x( ), y( ) + h( ) - 16, w( ), 16, FL_GRAY );
			fl_color( FL_BLACK );
			fl_draw( tmp, x( ), y( ) + h( ) - 16, w( ), 16, FL_ALIGN_CENTER );
		}

		AvUtils::timecode( tmp, project.position( ) );
		fl_draw_box( FL_FLAT_BOX, x( ), y( ), w( ), 18, FL_GRAY );
		fl_color( FL_BLACK );
		fl_draw( tmp, x( ), y( ), w( ), 18, FL_ALIGN_CENTER );
	}
	else
	{
		// Clear the box
		fl_draw_box( FL_FLAT_BOX, x( ), y( ), w( ), h( ), FL_GRAY );
	}
}

int AvTimeline_Labels::handle( int e )
{
	switch( e )
	{
		case FL_PUSH:
			multitrack.take_focus( );
			if ( Fl::event_inside( x( ), y( ) + 18, w( ), h( ) - 32 ) )
			{
				multitrack.change_active_track( -1 );
				NLE::Change_Mode( project_mode );
			}
			return 1;
	}
	return Fl_Widget::handle( e );
}

// 
// MAIN MULTITRACK GROUP
//

AvMultitrack::AvMultitrack( int x, int y, int w, int h ) :
	Fl_Group( x, y, w, h ),
	labels( *this, x, y, 100, h ),
	timeline( *this, x + 100, y, w - 100, h ),
	fixed_( 7500 ),
	zoom_( 1 ),
	project_( NULL )
{
	end( );
	resizable( timeline );
}

AvMultitrack::~AvMultitrack( )
{
}

void AvMultitrack::project( NLE_Project *project )
{
	project_ = project;
	if ( project_ != NULL )
	{
		if ( project_->get_double( "nle_fixed" ) != 0.0 )
		{
			fixed_ = project_->get_double( "nle_fixed" );
			redraw( );
			resize_items( );
		}
	}
	damage( FL_DAMAGE_ALL );
}

double AvMultitrack::fixed( )
{
	if ( project_ != NULL && project_->length( ) > fixed_ )
	{
		fixed_ = ( project_->length( ) + 250 );
		fixed_ -= ( int )fixed_ % 250;
	}
	else if ( project_ == NULL )
	{
		fixed_ = 1;
	}
	return fixed_;
}

void AvMultitrack::fixed( double value )
{
	fixed_ = value;
	resize_items( );
}

double AvMultitrack::zoom( )
{
	return zoom_;
}

void AvMultitrack::zoom( double value )
{
	if ( value * timeline.w( ) < 32768 )
	{
		zoom_ = value <= 1 ? 1 : value;
		resize_items( );
		NLE::Change_Position( project_->position( ), multitrack_position, true );
	}
}

bool AvMultitrack::refresh( int position )
{
	bool changed = false;
	if ( project_ != NULL )
	{
		// Check if the cut will change on any track
		for ( int i = 0; !changed && i < project_->tracks( ); i ++ )
		{
			int dummy = 0;
			Playlist &track = project_->track( i );
			Producer *producer = track.get_clip_at( position );
			if ( track.get_data( "active_cut", dummy ) != ( producer != NULL ? producer->get_producer( ) : NULL ) )
				changed = true;
			delete producer;
		}
	}
	return changed;
}

bool AvMultitrack::refresh( bool forced )
{
	if ( project_ != NULL && !forced )
	{
		// Check if the active cut has changed on any track...
		for ( int i = 0; !forced && i < project_->tracks( ); i ++ )
		{
			int dummy = 0;
			Playlist &track = project_->track( i );
			Producer *producer = track.get_clip_at( project_->position( ) );
			if ( track.get_data( "active_cut", dummy ) != ( producer != NULL ? producer->get_producer( ) : NULL ) )
				forced = true;
			delete producer;
		}
	}
	if ( project_ == NULL || forced )
		damage( FL_DAMAGE_ALL );
	return forced;
}

void AvMultitrack::resize_items( )
{
	if ( project_ == NULL ) return;
	int width = ( int )( ( double )timeline.w( ) * ( project_->length( ) / fixed( ) ) );
	timeline.render.size( ( int )( timeline.w( ) * zoom_ ), timeline.render.h( ) );
	timeline.timeline.size( ( int )( timeline.w( ) * zoom_ ), timeline.timeline.h( ) );
	timeline.slider.size( ( int )( width * zoom( ) ), timeline.slider.h( ) );
	timeline.init_sizes( );
	redraw( );
	timeline.slider.redraw( );
	//damage( FL_DAMAGE_ALL );
}

void AvMultitrack::change_active_track( int track )
{
	if ( project_ != NULL )
	{
		if ( track == -1 )
		{
			int tracks = project_->tracks( );
			track = ( Fl::event_y( ) - y( ) - 18 ) / ( ( h( ) - 36 ) / tracks );
		}
		project_->current_track( track );
		NLE::Refresh_Multitrack( true );
	}
}

void AvMultitrack::set_position( int value, bool centre )
{
	if ( project_ == NULL ) return;

	// Make sure the value is in range
	if ( value <= 0 ) value = 0;
	else if ( value >= project_->length( ) ) value = project_->length( ) - 1;

	// Resize widgets if necessary
	if ( project_->length( ) != project_->get_int( "_old_playtime" ) )
	{
		timeline.slider.bounds( 0, project_->length( ) - 1 );
		project_->set( "_old_playtime", project_->length( ) );
		resize_items( );
	}

	// Set the position
	if ( Fl::pushed( ) != &timeline.slider )
		timeline.slider.value( value );

	// and scroll if necessary
	int x = ( int )( ( double ) value / fixed_ * ( double )timeline.render.w( ) );
	scroll_to( x, centre );
	labels.damage( FL_DAMAGE_EXPOSE );
}

void AvMultitrack::scroll_to( int x, bool centre, bool force )
{
	// Now map it to an x value according to the width of the render widget and the fixed
	int new_position = !force ? timeline.xposition( ) : x;

	// Centre it if requested
	if ( centre )
		new_position = x - ( int )( timeline.w( ) / 2 );

	// Now change the scroll position if the value isn't in view
	if ( x < new_position )
		new_position = x;
	else if ( x >= new_position + timeline.w( ) )
		new_position = x - timeline.w( );
	if ( new_position + timeline.w( ) > timeline.render.w( ) )
		new_position = timeline.render.w( ) - timeline.w( );

	new_position = new_position < 0 ? 0 : new_position;

	// Set the new position
	timeline.position( new_position, 0 );
	timeline.slider.redraw( );
}

int AvMultitrack::xposition( )
{
	return timeline.xposition( );
}

void AvMultitrack::insert_at( int position, Producer *producer )
{
	if ( producer != NULL && producer->is_valid( ) )
	{
		int len = producer->get_playtime( );
		int active_track_idx = project_->current_track( );
		int placed = project_->insert_at( active_track_idx, position, producer );
		if ( placed >= 0 )
		{
			NLE::Change_Mode( project_mode );
			project_->track( active_track_idx ).set( "active_cut", NULL, 0 );
			NLE::Change_Position( placed + len - 1, multitrack_position, true );
			NLE::Change_Mode( browser_mode );
		}
	}
}

void AvMultitrack::transitions( bool value )
{
	if ( project_ )
		project_->transitions( value );
}

void AvMultitrack::lock_tracks( bool value )
{
	if ( project_ )
		project_->lock_tracks( value );
}

NLE_Project &AvMultitrack::project( )
{
	if ( project_ == NULL )
		throw "No project loaded.";
	return *project_;
}

bool AvMultitrack::has_project( )
{
	return project_ != NULL;
}


