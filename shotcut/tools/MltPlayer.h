
#ifndef _FLTK_MLT_PLAYER_
#define _FLTK_MLT_PLAYER_

#include <mlt++/Mlt.h>
using namespace Mlt;

#include "AvPlayer.h"
using namespace fltk;

#include <string>
using namespace std;

namespace fltk
{
	class MltPlayer : public AvPlayer
	{
		private:
			Event *event;
			Consumer *consumer;
			Producer *current;
			Producer *previous;
			int position;
		public:
			MltPlayer( int x, int y, int w, int h );
			virtual ~MltPlayer( );
			void play( Producer &producer );
			void play( char *clip );
			void set_speed( double speed );
			// Callback from player when user moves the slider
			void position_change( int position, int in, int out );
			// Callback from player when user clicks a button
			void button_press( string id );
			// Callback from the consumer
			void frame_shown( Frame &frame );
			void on_hide( );
			void on_show( );
			void on_close( );
			int handle( int e );
			bool restart( );
			void seek( int offset );
	};
}

#endif

