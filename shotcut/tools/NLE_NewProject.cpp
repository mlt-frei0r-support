/*
 * NLE_NewProject.cpp -- NLE New Project method
 * Copyright (C) 2004-2005 Ushodaya Enterprises Limited
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

// System header files
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

// FLTK header files
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Round_Button.H>
#include <FL/Fl_Return_Button.H>
#include <FL/Fl_Input.H>
#include <FL/fl_ask.H>

// Shotcut header files
#include "NLE.h"
#include "NLE_NewProject.h"
using namespace shotcut;

class NLE_NewProject : public Fl_Window
{
	private:
		Fl_Input *input;
		Fl_Button *button_ok;
		Fl_Button *button_cancel;
		Fl_Round_Button **buttons;
		int count;

	public:
		NLE_NewProject( NLE_Project **templates, int count_ ) :
			Fl_Window( 320, 20 * count_ + 60 ),
			count( count_ )
		{
			input = new Fl_Input( 0, 20, 320, 20, "Title" );
			input->value( "New Project" );
			input->labeltype( FL_NORMAL_LABEL );
			input->align( FL_ALIGN_LEFT | FL_ALIGN_TOP );
			input->labelsize( 12 );
			buttons = new Fl_Round_Button *[ count ];
			for ( int i = 0; i < count; i ++ )
			{
				buttons[ i ] = new Fl_Round_Button( 0, 40 + i * 20, 320, 20, templates[ i ]->get( "nle_template" ) );
				buttons[ i ]->labelsize( 12 );
				buttons[ i ]->value( i == 0 );
			}
			button_ok = new Fl_Return_Button( w( ) - 160, h( ) - 20, 80, 20, "&OK" );
			button_ok->labelsize( 12 );
			button_cancel = new Fl_Button( w( ) - 80, h( ) - 20, 80, 20, "&Cancel" );
			button_cancel->labelsize( 12 );
			end( );
		}

		// Returns -1 if cancelled or 0 to n 
		int exec( )
		{
			int selected = 0;
			set_modal( );
			show( );
  			while( 1 )
			{
				NLE::Refresh_Preview( );
    			Fl_Widget *o = Fl::readqueue();
    			if ( o == NULL ) Fl::wait();
    			else if ( o == this ) { selected = -1; break; }
    			else if ( o == button_cancel ) { selected = -1; break; }
    			else if ( o == button_ok ) { break; }
    			else if ( o == input ) { /* Do nothing */ }
				else if ( count > 0 )
				{
					for ( int i = 0; i < count; i ++ )
					{
						buttons[ i ]->value( buttons[ i ] == o );
						if ( buttons[ i ] == o )
							selected = i;
					}
				}
  			}
			hide( );
			return selected;
		}

		const char *title( )
		{
			return input->value( );
		}
};

static NLE_Project **load_templates( char *directory, int &count )
{
	NLE_Project **templates = NULL;
	count = 0;
	DIR *dir = opendir( directory );
	struct dirent *entry;
	
	// Ugh - scan the directories to get the count
	while( ( entry = readdir( dir ) ) != NULL )
		if ( strstr( entry->d_name, ".westley" ) )
			count ++;

	// Create the array
	templates = new NLE_Project *[ count ];

	// Now load each template
	if ( templates != NULL )
	{
		char temp[ 512 ];

		int i = 0;

		// Seek back to the beginning 
		seekdir( dir, 0 );

		while( ( entry = readdir( dir ) ) != NULL )
		{
			if ( strstr( entry->d_name, ".westley" ) )
			{
				sprintf( temp, "%s/%s", directory, entry->d_name );
				templates[ i ++ ] = new NLE_Project( temp );
			}
		}
	}

	closedir( dir );

	return templates;
}

static void destroy_templates( NLE_Project **templates, int count )
{
	for ( int i = 0; i < count; i ++ )
		delete templates[ i ];
	delete templates;
}

NLE_Project *NLE::New_Project( bool interactive )
{
	// Project to return
	NLE_Project *project = NULL;

	// Determint the directory containing the templates
	char directory[ 1024 ];
	strcpy( directory, getenv( "HOME" ) );
	strcat( directory, "/.shotcut/templates" );

	// Refresh the templates for this user via the script
	char command[ 2048 ];
	sprintf( command, "shotcut-templates %s", directory );
	system( command );

	// Load them
	int count = 0;
	NLE_Project **templates = load_templates( directory, count );

	// Create a window with a radio button for each template, OK and Cancel
	if ( interactive )
	{
		NLE_NewProject *window = new NLE_NewProject( templates, count );
		int selected = window->exec( );
	
		// Obtain the template selected
		if ( selected >= 0 && selected < count )
		{
			project = new NLE_Project( templates[ selected ] );
			project->set( "title", ( char * )window->title( ) );
		}
	}
	else if ( count > 0 )
	{
		// Take the first, default template
		project = new NLE_Project( templates[ 0 ] );
		project->set( "title", "New Project" );
	}
	else
	{
		fl_alert( "Templates unavailable - can't create a project" );
	}

	// Delete the templates
	destroy_templates( templates, count );
	
	return project;
}

