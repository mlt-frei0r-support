/*
 * shotcut-server.cpp -- the shotcut proxy server
 * Copyright (C) 2004-2004 Ushodaya Enterprises Limited
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

#include <FL/Fl.H>
#include <FL/Fl_Button.H>
#include <FL/fl_ask.H>

#include <mlt++/Mlt.h>
using namespace Mlt;

#include "AvWindow.h"
using namespace shotcut;

#include <valerie/valerie_remote.h>

class ShotcutServer : public Miracle
{
	private:
		Properties profile;
		int selection;

	public:
		ShotcutServer( char *id, int port, char *_profile ) :
			Miracle( id, port ),
			selection( 0 )
		{
			set( "push-parser-off", 1 );
			load_profile( _profile );
			//start_tools( );
			shutdown_old_server( port );
			start( );
		}

		void shutdown_old_server( int port )
		{
			valerie_parser parser = valerie_parser_init_remote( "localhost", port );
			valerie_response response = valerie_parser_connect( parser );
			if ( response != NULL )
			{
				valerie_response_close( response );
				response = valerie_parser_execute( parser, "shutdown" );
				if ( response != NULL )
					valerie_response_close( response );
			}
			valerie_parser_close( parser );
		}

		void load_profile( char *file )
		{
			if ( file != NULL )
			{
				Properties temp( file );
				profile.inherit( temp );
			}

			if ( file == NULL || profile.count( ) == 0 )
			{
				profile.parse( "tools=3" );
				profile.parse( "0=Preview" );
				profile.parse( "0.command=shotcut-preview --on-top" );
				profile.parse( "0.port=5251" );
				profile.parse( "1=Text Editor" );
				profile.parse( "1.command=shotcut-editor" );
				profile.parse( "1.port=5252" );
				profile.parse( "2=NLE" );
				profile.parse( "2.port=5260" );
			}
		}

		void start_tools( )
		{
			int tools = profile.get_int( "tools" );
			for( int i = 0; i < tools; i ++ )
				start_tool( i );
		}

		bool start_tool( int index )
		{
			int tools = profile.get_int( "tools" );
			bool started = false;
			if ( index < tools )
			{
				char temp[ 132 ];
				sprintf( temp, "%d.command", index );
				char *cmd = profile.get( temp );

				if ( cmd != NULL )
				{
					sprintf( temp, "%d.port", index );
					int port = profile.get_int( temp );
					if ( port != 0 )
					{
						valerie_parser parser = valerie_parser_init_remote( "localhost", port );
						valerie_response response = valerie_parser_connect( parser );
						if ( response == NULL )
						{
							sprintf( temp, "%s --port=%d &", cmd, port );
							system( temp );
							started = true;
						}
						else
						{
							valerie_response_close( response );
						}
						valerie_parser_close( parser );
					}
					else
					{
						sprintf( temp, "%s &", cmd );
						system( temp );
						started = true;
					}
				}
			}
			return started;
		}

		Response *execute( char *command )
		{
			valerie_response response = valerie_response_init( );
			if ( !strcmp( command, "shutdown" ) )
				exit( 0 );
			return new Response( response );
		}
		
		Response *received( char *command, char *doc )
		{
			valerie_response response = valerie_response_init( );
			if ( doc != NULL )
			{
				valerie_response_set_error( response, 200, "OK" );
				load_file( command, doc );
			}
			return new Response( response );
		}

		static void close_cb( void *arg )
		{
			// Do nothing
		}

		int options( char *command )
		{
			Fl::lock( );
			int dropped = 0;
			int tools = profile.get_int( "tools" );
			Fl_Button *buttons[ tools + 1 ];
			AvWindow window( 160, 25 * ( tools + 1 ) );
			window.callback( (Fl_Callback *)close_cb );
			for ( int i = 0; i < tools; i ++ )
			{
				char temp[ 132 ];
				sprintf( temp, "%d", i );
				if ( strstr( command, profile.get( temp ) ) == NULL )
				{
					buttons[ i ] = new Fl_Button( 0, ( i - dropped ) * 25, 160, 25, profile.get( temp ) );
					window.add( buttons[ i ] );
				}
				else
				{
					dropped = 1;
				}
			}
			buttons[ tools ] = new Fl_Button( 0, ( tools - dropped ) * 25, 160, 25, "Cancel" ); 
			window.add( buttons[ tools ] );
			window.size( 160, 25 * ( tools - dropped + 1 ) );
			window.clear_border( );
			window.hotspot( 80, selection * 25 + 12 );
			window.show( );
			window.always_on_top( true );
			selection = -1;
			while( selection == -1 )
			{
				Fl_Widget *o = Fl::readqueue();
				if ( o == NULL )
				{
					Fl::wait();
				}
				else if ( o == &window )
				{
					selection = tools;
				}
				else 
				{
					for ( int i = 0; i < tools + 1; i ++ )
					{
						if ( o == buttons[ i ] )
							selection = i;
					}
				}
			}
			window.hide( );
			Fl::wait( );
			Fl::unlock( );
			return selection;
		}

		void load_file( char *command, char *doc )
		{
  			int r = options( command );
			int tools = profile.get_int( "tools" );
			
			if ( r < tools )
			{
				char temp[ 132 ];
				sprintf( temp, "%d.port", r );
				int port = profile.get_int( temp );
				sprintf( temp, "%d.command", r );
				char *cmd = profile.get( temp );
				sprintf( temp, "%d.server", r );
				char *server = profile.get( temp );

				Consumer valerie( "valerie" );
				if ( server )
					valerie.set( "server", server );
				valerie.set( "port", port );
				valerie.set( "westley", doc );
				valerie.set( "command", command );
				valerie.start( );
				if ( valerie.get_int( "_error" ) != 0 && cmd != NULL )
				{
					struct timespec tm = { 1, 0 };
					if ( start_tool( r ) )
						nanosleep( &tm, NULL );
					valerie.start( );
				}
			}
		}
};

int main( int argc, char **argv )
{
	int valid = 1;
	char *name = "Shotcut";
	int port = 5250;
	int log_level = 0;
	char *profile = NULL;
	int nofork = 0;

	for ( int i = 1; i < argc && valid; i ++ )
	{
		if ( !strncmp( argv[ i ], "--name=", 7 ) )
			name = argv[ i ] + 7;
		else if ( !strncmp( argv[ i ], "--port=", 7 ) )
			port = atoi( argv[ i ] + 7 );
		else if ( !strncmp( argv[ i ], "--profile=", 10 ) )
			profile = argv[ i ] + 10;
		else if ( !strncmp( argv[ i ], "--log-level=", 12 ) )
			log_level = atoi( argv[ i ] + 12 );
		else if ( !strcmp( argv[ i ], "--no-fork" ) )
			nofork = 1;
		else
			valid = 0;
	}

	if ( !nofork && fork( ) )
		return 0;

	if ( !nofork )
		setsid( );

	if ( valid )
	{
		Factory::init( );
		Miracle::log_level( log_level );
		ShotcutServer server( name, port, profile );
		server.wait_for_shutdown( );
	}
	else
	{
		fprintf( stderr, "Usage: shotcut-server [ --name=Name ] [ --port=port ] [ --profile=file ]\n" );
	}

	return 0;
}

