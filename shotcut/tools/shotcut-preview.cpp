/*
 * shotcut-preview.cpp -- the preview client/server
 * Copyright (C) 2004-2004 Ushodaya Enterprises Limited
 * Author: Charles Yates <charles.yates@pandora.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

// FLTK Header files
#include <FL/Fl.H>

// System header files
#include <sys/types.h>
#include <unistd.h>

// MLT++ header files
#include <mlt++/Mlt.h>
using namespace Mlt;

// Local header files
#include "AvWindow.h"
#include "MiraclePlayer.h"
using namespace shotcut;

class ExtendedAvWindow : public MiraclePlayer
{
	private:
		int out_port;

	public:
		ExtendedAvWindow( Properties &properties, int port, int w, int h ) :
			MiraclePlayer( properties, port, 0, 0, w, h ),
			out_port( 5250 )
		{
			callback( static_close_cb, this );
			end( );
		}

		static void static_close_cb( Fl_Widget *widget, void *obj )
		{
			( ( MiraclePlayer * )obj )->stop( );
			exit( 0 );
		}

		void set_out_port( int value )
		{
			out_port = value;
		}

		int handle( int e )
		{
			int key = Fl::event_key( );

			switch( e )
			{
				case FL_KEYDOWN:
					if ( key == FL_Left || key == FL_Right )
					{
						set_speed( 0 );
						seek_relative( ( Fl::event_state( FL_CTRL ) ? 25 : 1 ) * ( key == FL_Left ? -1 : 1 ) );
						return 1;
					}
					else if ( key == ' ' )
					{
						button_press( "play" );
						return 1;
					}
					else if ( key == 'a' )
					{
						always_on_top( true );
						return 1;
					}
					else if ( key == 't' && Fl::event_state( FL_CTRL ) )
					{
						set_speed( 0 );
						Consumer c( "valerie" );
						c.set( "server", "localhost" );
						c.set( "port", out_port );
						c.connect( *get_current( ) );
						c.start( );
						return 1;
					}
					break;
			}
			return MiraclePlayer::handle( e );
		}
};

class NLE_Properties : public Properties
{
	public:
		NLE_Properties::NLE_Properties( ) : Properties( )
		{
			set( "normalisation", "PAL" );
			set( "preview", "sdl_preview:360x288" );
			set( "preview.resize", 1 );
			set( "preview.progressive", 1 );
			set( "preview.rescale", "hyper" );
			set( "colour.foreground", ( char * )NULL );
			set( "colour.background", ( char * )NULL );
			set( "colour.inactive", ( char * )NULL );
			set( "colour.selection", ( char * )NULL );
			set( "colour.hot", "0xff000000" );
			set( "colour.warm", "0x80000000" );
			set( "colour.luke", "0xff800000" );
			set( "colour.cold", "0x00ff0000" );
			set( "font.application", ( char * )NULL );
			set( "font.data_entry", ( char * )NULL );
			string value( getenv( "HOME" ) );
			value = value + "/.shotcut/shotcut-preview.conf";
			load( value.c_str( ) );
		}
};


char *remove_arg( int &argc, char **argv, int &index, int next )
{
	char *ret = next < 0 ? NULL : argv[ index ] + next;
	for ( int i = index + 1; i < argc; i ++ )
		argv[ i - 1 ] = argv[ i ];
	index --;
	argc --;
	return ret;
}

int main( int argc, char **argv )
{
	char *name = "roberts";
	int port = 5251;
	int out_port = 5250;
	int log_level = 0;
	char *filename = NULL;
	bool always_on_top = false;
	bool maximize = false;
	int i = 1;
	int nofork = 0;

	// Get the file name (must be the first arg :-/)
	if ( argc > 1 && *argv[ 1 ] != '-' )
		filename = remove_arg( argc, argv, i, 0 );

	// Search arguments for port
	for ( i = 1; i < argc; i ++ )
	{
		if ( !strncmp( argv[ i ], "--name=", 7 ) )
			name = remove_arg( argc, argv, i, 7 );
		else if ( !strncmp( argv[ i ], "--port=", 7 ) )
			port = atoi( remove_arg( argc, argv, i, 7 ) );
		else if ( !strncmp( argv[ i ], "--out-port=", 11 ) )
			out_port = atoi( remove_arg( argc, argv, i, 11 ) );
		else if ( !strncmp( argv[ i ], "--log-level=", 12 ) )
			log_level = atoi( remove_arg( argc, argv, i, 12 ) );
		else if ( !strcmp( argv[ i ], "--on-top" ) )
			always_on_top = remove_arg( argc, argv, i, 0 ) != NULL;
		else if ( !strcmp( argv[ i ], "--maximize" ) )
			maximize = remove_arg( argc, argv, i, 0 ) != NULL;
		else if ( !strcmp( argv[ i ], "--no-fork" ) )
			nofork = remove_arg( argc, argv, i, 0 ) != NULL;
	}

	if ( !nofork && fork( ) )
		return 0;

	setenv( "MLT_TEST_CARD", SHOTCUT_SHARE "pixmaps/logo.png", 0 );
	Factory::init( );
	NLE_Properties properties;
	Miracle::log_level( log_level );
	ExtendedAvWindow window( properties, port, 320, 240 );
	window.set_out_port( out_port );
	window.hotspot( 160, 120 );
	window.show( argc, argv );
	window.always_on_top( always_on_top );
	window.maximize( maximize );
	Producer producer( filename ? filename : (char *)( SHOTCUT_SHARE "pixmaps/logo.png" ) );
	if ( filename == NULL )
		producer.set_speed( 0 );
	Fl::lock( );
	window.play( producer );
	window.locked( true );
	return Fl::run();
}

